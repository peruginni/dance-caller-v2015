//
//  PersistenceStack.m
//  DanceCaller
//
//  Created by Ondrej Macoszek on 4/10/14.
//  Copyright (c) 2014 Ondřej Macoszek. All rights reserved.
//

#import "Store.h"
#import "Localization.h"

@interface Store ()

@property (nonatomic, strong) NSManagedObjectContext *context;

@end

@implementation Store

- (id)initWithManagedObjectContext:(NSManagedObjectContext *)context;
{
    self = [super init];
    if (self) {
        self.context = context;
        [self fillDefaultDataIfNeeded];
        [self handleLanguageChangeIfNeeded];
    }
    return self;
}

#pragma mark - Actions

- (NSError *)saveContext
{
    NSError *error = nil;
    [_context save:&error];
    if (error) {
        NSLog(@"%@", error.debugDescription);
        //        @throw [NSException exceptionWithName:CZTException reason:error.debugDescription userInfo:nil];
    }
    return error;
}

- (void)fillDefaultDataIfNeeded
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"filledDefaultData"]) {
        
        [_context performBlockAndWait:^{
            
            CDAppSettings *appSettings = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([CDAppSettings class]) inManagedObjectContext:_context];
            appSettings.lastLanguage = [Localization serverLanguageForCurrentDeviceLocale];
            
            NSMutableArray *mapGridCells = @[].mutableCopy;
            MapGridCell *czechRepublic = [MapGridCell new];
            czechRepublic.identifier = @(1);
            [czechRepublic setupWithBottomLeft:CZ_BOTTOM_LEFT topRight:CZ_TOP_RIGHT];
            [mapGridCells addObject:czechRepublic];
            
            int columns = 7;
            int rows = 4;
            double cell_width = czechRepublic.longitudeSpan / (double)columns;
            double cell_height = czechRepublic.latitudeSpan / (double)rows;
            
            CLLocationCoordinate2D base = czechRepublic.bottomLeft;
            for (int column = 0, i = 2; column < columns; column++) {
                for (int row = 0; row < rows; row++) {
                    if (i != 2 && i != 5 && i != 29 && i != 25) { // skip empty cells (outside republic)
                        MapGridCell *cell = [MapGridCell new];
                        cell.identifier = @(i);
                        [cell setupWithBottomLeft:CLLocationCoordinate2DMake(base.latitude + cell_height * row, base.longitude + cell_width * column)
                                         topRight:CLLocationCoordinate2DMake(base.latitude + cell_height * (row+1), base.longitude + cell_width * (column+1))];
                        [mapGridCells addObject:cell];
                    }
                    i++;
                }
            }
            
            for (MapGridCell *cell in mapGridCells) {
                CDMapGridCell *cdCell = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([CDMapGridCell class]) inManagedObjectContext:_context];
                cdCell.identifier = cell.identifier;
                cdCell.bottomLeftLatitude = cell.bottomLeftLatitude;
                cdCell.bottomLeftLongitude = cell.bottomLeftLongitude;
                cdCell.topRightLatitude = cell.topRightLatitude;
                cdCell.topRightLongitude = cell.topRightLongitude;
                cdCell.lastServerRequest = cell.lastServerRequest;
            }
            
            CDUser *anonymousUser = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([CDUser class]) inManagedObjectContext:_context];
            anonymousUser.anonymous = @(YES);
            
            [self saveContext];
            
        }];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"filledDefaultData"];
    }
}

- (void)handleLanguageChangeIfNeeded
{
    [_context performBlockAndWait:^{
        
        NSString *serverLanguage = [Localization serverLanguageForCurrentDeviceLocale];
        CDAppSettings *appSettings = [self fetchSingleManagedObjectForClass:[CDAppSettings class]];
        
        if (appSettings.lastLanguage) {
            if (![appSettings.lastLanguage isEqualToString:serverLanguage]) {
                
                // reset timestamps
                NSArray *mapGrids = [self fetchAllManagedObjectsForClass:[CDMapGridCell class]];
                for (CDMapGridCell *mapGrid in mapGrids) {
                    mapGrid.lastServerRequest = nil;
                }
                
                NSArray *categories = [self fetchAllManagedObjectsForClass:[CDVenueCategory class]];
                for (CDVenueCategory *category in categories) {
                    category.name = nil;
                }
                
                NSArray *venues = [self fetchAllManagedObjectsForClass:[CDVenue class]];
                for (CDVenue *venue in venues) {
                    venue.dateUpdated = nil;
                }
            }
        }
        appSettings.lastLanguage = serverLanguage;
        
        [self saveContext];
        
    }];
}

#pragma mark - Helpers

- (NSArray *)fetchAllManagedObjectsForClass:(Class)c
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass(c)];
    request.returnsObjectsAsFaults = NO;
    NSArray *result = [_context executeFetchRequest:request error:nil];
    if (result.count == 0) {
        result = nil;
    }
    return result;
}

- (id)fetchSingleManagedObjectForClass:(Class)c
{
    id managedObject = nil;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass(c)];
    request.fetchLimit = 1;
    request.returnsObjectsAsFaults = NO;
    NSArray *fetchedObjects = [_context executeFetchRequest:request error:nil];
    if (fetchedObjects.count > 0) {
        managedObject = [fetchedObjects firstObject];
    }
    return managedObject;
}

- (id)fetchSingleManagedObjectForClass:(Class)c byIdentifier:(NSNumber *)identifier
{
    id object = nil;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass(c)];
    request.predicate = [NSPredicate predicateWithFormat:@"identifier = %@", identifier];
    request.fetchLimit = 1;
    request.returnsObjectsAsFaults = NO;
    NSArray *result = [_context executeFetchRequest:request error:nil];
    if (result.count > 0) {
        object = [result firstObject];
    }
    return object;
}

#pragma mark - Convertors

- (Venue *)venueFromCDVenue:(CDVenue *)cdVenue partial:(BOOL)partial
{
    Venue *venue = nil;
    if (cdVenue) {
        venue = [Venue new];
        
        venue.aliasPath = venue.aliasPath;
        venue.showInDay = cdVenue.showInDay;
        venue.showInNight = cdVenue.showInNight;
        venue.address = cdVenue.address;
        venue.dateCreated = cdVenue.dateCreated;
        venue.dateEnd = cdVenue.dateEnd;
        venue.dateIdentifier = cdVenue.dateIdentifier;
        venue.nodeIdentifier = cdVenue.nodeIdentifier;
        venue.dateStart = cdVenue.dateStart;
        venue.dateEnd = cdVenue.dateEnd;
        venue.headliners = cdVenue.headliners;
        venue.identifier = cdVenue.identifier;
        venue.isPartial = cdVenue.isPartial;
        venue.latitude = cdVenue.latitude;
        venue.longitude = cdVenue.longitude;
        venue.linkOfficial = cdVenue.linkOfficial;
        venue.linkTickets = cdVenue.linkTickets;
        venue.linkOpeningHours = cdVenue.linkOpeningHours;
        venue.name = cdVenue.name;
        venue.perex = cdVenue.perex;
        venue.photos = [cdVenue photosArray];
        venue.rating = cdVenue.rating;
        venue.type = cdVenue.type;
        venue.linkOpeningHours = cdVenue.linkOpeningHours;
        venue.phoneNumber = cdVenue.phoneNumber;
        venue.trending = cdVenue.trending;
        
        NSMutableSet *categories = [NSMutableSet new];
        for (CDVenueCategory *cdCategory in cdVenue.categories) {
            VenueCategory *category = [self categoryFromCDCategory:cdCategory];
            if (category) {
                [categories addObject:category];
            }
        }
        venue.categories = [NSSet setWithSet:categories];
        
        if (!partial) {
            
            NSMutableSet *deals = [NSMutableSet new];
            for (CDDeal *cddeal in cdVenue.deals) {
                Deal *deal = [self dealFromCDDeal:cddeal];
                if (deal) {
                    [deals addObject:deal];
                }
            }
            venue.deals = [NSSet setWithSet:deals];
            
            NSMutableOrderedSet *highlights = [NSMutableOrderedSet new];
            for (CDHighlight *cdHighlight in cdVenue.highlights) {
                Highlight *highlight = [self highlightFromCDHighlight:cdHighlight];
                if (highlight) {
                    [highlights addObject:highlight];
                }
            }
            venue.highlights = [NSOrderedSet orderedSetWithOrderedSet:highlights];
            
            NSMutableSet *transporters = [NSMutableSet new];
            for (CDTransporter *cdtransporter in cdVenue.transporters) {
                Transporter *transporter = [self transporterFromCDTransporter:cdtransporter];
                if (transporter) {
                    [transporters addObject:transporter];
                }
            }
            venue.transporters = [NSSet setWithSet:transporters];
            
            NSMutableOrderedSet *descriptionItems = [NSMutableOrderedSet new];
            for (CDVenueDescriptionItem *cddescriptionItem in cdVenue.descriptionItems) {
                VenueDescriptionItem *descriptionItem = [self venueDescriptionItemFromCDVenueDescriptionItem:cddescriptionItem];
                if (descriptionItem) {
                    [descriptionItems addObject:descriptionItem];
                }
            }
            venue.descriptionItems = [NSOrderedSet orderedSetWithOrderedSet:descriptionItems];
            
        }
    }
    return venue;
}

- (VenueCategory *)categoryFromCDCategory:(CDVenueCategory *)cdCategory
{
    VenueCategory *category = nil;
    if (cdCategory) {
        category = [VenueCategory new];
        category.identifier = cdCategory.identifier;
        category.name = cdCategory.name;
        category.showInNight = cdCategory.showInNight;
    }
    return category;
}

- (Deal *)dealFromCDDeal:(CDDeal *)cdDeal
{
    Deal *deal = nil;
    if (cdDeal) {
        deal = [Deal new];
        deal.companyName = cdDeal.companyName;
        deal.deals = cdDeal.deals;
        deal.identifier = cdDeal.identifier;
        deal.linkOfficial = cdDeal.linkOfficial;
        deal.longDescription = cdDeal.longDescription;
        deal.notes = cdDeal.notes;
    }
    return deal;
}

- (Highlight *)highlightFromCDHighlight:(CDHighlight *)cdHighlight
{
    Highlight *highlight = nil;
    if (cdHighlight) {
        highlight = [Highlight new];
        highlight.text = cdHighlight.text;
        highlight.title = cdHighlight.title;
    }
    return highlight;
}

- (Transporter *)transporterFromCDTransporter:(CDTransporter *)cdTransporter
{
    Transporter *transporter = nil;
    if (cdTransporter) {
        transporter = [Transporter new];
        transporter.discounts = cdTransporter.discounts;
        transporter.departures = cdTransporter.departures;
        transporter.destinations = cdTransporter.destinations;
        transporter.identifier = cdTransporter.identifier;
        transporter.linkOfficial = cdTransporter.linkOfficial;
        transporter.longDescription = cdTransporter.longDescription;
        transporter.name = cdTransporter.name;
        transporter.notes = cdTransporter.notes;
        transporter.prices = cdTransporter.prices;
        transporter.latitude = cdTransporter.latitude;
        transporter.longitude = cdTransporter.longitude;
        transporter.address = cdTransporter.address;
        transporter.openingHours = cdTransporter.openingHours;
        transporter.phone = cdTransporter.phone;
    }
    return transporter;
}

- (VenueDescriptionItem *)venueDescriptionItemFromCDVenueDescriptionItem:(CDVenueDescriptionItem *)cdItem
{
    VenueDescriptionItem *item = nil;
    if (cdItem) {
        item = [VenueDescriptionItem new];
        item.type = cdItem.type;
        item.link = cdItem.link;
        item.thumbnail = cdItem.thumbnail;
        item.content = cdItem.content;
        item.width = cdItem.width;
        item.height = cdItem.height;
        item.identifier = cdItem.identifier;
    }
    return item;
}

- (User *)userFromCDUser:(CDUser *)cdUser
{
    User *user = nil;
    if (cdUser) {
        user = [User new];
        user.anonymous = cdUser.anonymous;
        user.about = cdUser.about;
        user.avatarId = cdUser.avatarId;
        user.birthday = cdUser.birthday;
        user.emailNotifications = cdUser.emailNotifications;
        user.facebookId = cdUser.facebookId;
        user.lastSynchronization = cdUser.lastSynchronization;
        user.identifier = cdUser.identifier;
        user.signedIn = cdUser.signedIn;
        user.language = cdUser.language;
        user.email = cdUser.email;
        user.location = cdUser.location;
        user.name = cdUser.name;
        user.password = cdUser.password;
        user.sessionToken = cdUser.sessionToken;
        user.favoriteVenuesCount = @(cdUser.favoriteVenues.count);
        user.scheduledVenuesCount = @(cdUser.scheduledVenues.count);
    }
    return user;
}

- (MapGridCell *)mapGridCellFromCDMapGridCell:(CDMapGridCell *)cdMapGridCell
{
    MapGridCell *cell = nil;
    if (cdMapGridCell) {
        cell = [MapGridCell new];   
        cell.identifier = cdMapGridCell.identifier;
        cell.lastServerRequest = cdMapGridCell.lastServerRequest;
        cell.bottomLeftLatitude = cdMapGridCell.bottomLeftLatitude;
        cell.bottomLeftLongitude = cdMapGridCell.bottomLeftLongitude;
        cell.topRightLatitude = cdMapGridCell.topRightLatitude;
        cell.topRightLongitude = cdMapGridCell.topRightLongitude;
        [cell computeMissingValues];
    }
    return cell;
}

#pragma mark - AppSettings

- (void)fetchAppSettingsWithCompletion:(void(^)(AppSettings *appSettings))completion
{
    [_context performBlock:^{
        AppSettings *appSettings = [AppSettings new];
        CDAppSettings *cdAppSettings = [self fetchSingleManagedObjectForClass:[CDAppSettings class]];
        if (cdAppSettings) {
            appSettings.deletedLastUpdate = cdAppSettings.deletedLastUpdate;
            appSettings.lastLanguage = cdAppSettings.lastLanguage;
            appSettings.venueCategoriesLastUpdate = cdAppSettings.venueCategoriesLastUpdate;
        }
        if (completion) {
            completion(appSettings);
        }
    }];
}

- (void)persistAppSettings:(AppSettings *)appSettingsToPersist completion:(void(^)(void))completion
{
    [_context performBlock:^{
        AppSettings *appSettings = appSettingsToPersist;
        if (!appSettings) {
            appSettings = [AppSettings new];
        }
        CDAppSettings *cdAppSettings = [self fetchSingleManagedObjectForClass:[CDAppSettings class]];
        if (!cdAppSettings) {
            cdAppSettings = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([CDAppSettings class]) inManagedObjectContext:_context];
        }
        appSettings.deletedLastUpdate = cdAppSettings.deletedLastUpdate;
        appSettings.lastLanguage = cdAppSettings.lastLanguage;
        appSettings.venueCategoriesLastUpdate = cdAppSettings.venueCategoriesLastUpdate;
        
        if (completion) {
            completion();
        }
    }];
}

- (void)fetchStaticPagesForLocale:(NSString *)locale withCompletion:(void(^)(NSArray *pages))completion
{
    [_context performBlock:^{
        
        NSMutableArray *foundPages = @[].mutableCopy;
        
        if (locale) {
            NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([CDStaticPage class])];
            [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"locale = %@", locale]];
            NSArray *fetchedObjects = [_context executeFetchRequest:fetchRequest error:nil];
            if (fetchedObjects.count > 0) {
                for (CDStaticPage *cdPage in fetchedObjects) {
                    StaticPage *page = [StaticPage new];
                    page.title = cdPage.title;
                    page.content = cdPage.content;
                    page.pageId = cdPage.pageId;
                    page.dateUpdated = cdPage.dateUpdated;
                    [foundPages addObject:page];
                }
            }
        }
        
        if (completion) {
            completion(foundPages);
        }
        
    }];
}

- (void)persistStaticPageBundle:(NSDictionary *)bundle forLocale:(NSString *)locale completion:(void(^)(BOOL persisted))completion
{
    [_context performBlock:^{
        
        NSMutableArray *pagesToStore = @[].mutableCopy;
        if (bundle[@"about"]) {
            [pagesToStore addObject:bundle[@"about"]];
        }
        if (bundle[@"practical"]) {
            for (NSDictionary *page in bundle[@"practical"]) {
                [pagesToStore addObject:page];
            }
        }
    
        BOOL persisted = YES;
        for (NSDictionary *dictPage in pagesToStore) {
            if (dictPage[@"id"] && locale) {
                
                CDStaticPage *cdPage = nil;
                NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([CDStaticPage class])];
                [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"locale = %@ && pageId = %@", locale, dictPage[@"id"]]];
                NSArray *fetchedObjects = [_context executeFetchRequest:fetchRequest error:nil];
                if (fetchedObjects.count > 0) {
                    cdPage = [fetchedObjects firstObject];
                } else {
                    cdPage = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([CDStaticPage class]) inManagedObjectContext:_context];
                }
                cdPage.dateUpdated = [NSDate date];
                cdPage.title = dictPage[@"title"];
                cdPage.content = dictPage[@"html"];
                cdPage.locale = locale;
                cdPage.pageId = dictPage[@"id"];

            } else {
                NSLog(@"missing pageid and locale");
            }
        }
        NSError *error = [self saveContext];
        if (error) {
            persisted = NO;
        }

        if (completion) {
            completion(persisted);
        }
    }];
}


#pragma mark - Venues

- (BOOL)shouldDownloadVenue:(Venue *)venue
{
    BOOL download = NO;
    if (venue == nil) {
        download = YES;
    } else if (venue.nodeIdentifier) {
        if (venue.isPartial || !venue.dateUpdated || venue.dateUpdated.timeIntervalSinceNow > VENUE_EXPIRATION) {
            download = YES;
        }
    }
    return download;
}

- (CDVenue *)fetchVenueWithIdentifier:(VenueIdentifier *)identifier
{
    CDVenue *venue = nil;
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([CDVenue class])];
    if (identifier.nodeIdentifier && identifier.dateIdentifier) {
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"nodeIdentifier = %@ && dateIdentifier = %@", identifier.nodeIdentifier, identifier.dateIdentifier]];
    } else if (identifier.nodeIdentifier) {
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"nodeIdentifier = %@", identifier.nodeIdentifier]];
    } else {
        fetchRequest = nil;
    }
    
    if (fetchRequest) {
        NSArray *fetchedObjects = [_context executeFetchRequest:fetchRequest error:nil];
        if (fetchedObjects.count > 0) {
            venue = [fetchedObjects firstObject];
        }
    }
    
    return venue;
}

- (void)fetchVenueWithIdentifier:(VenueIdentifier *)identifier partial:(BOOL)partial completion:(void(^)(Venue *venue))completion
{
    [_context performBlock:^{
        Venue *venue = nil;
        
        CDVenue *cdVenue = [self fetchVenueWithIdentifier:identifier];
        if (cdVenue) {
            venue = [self venueFromCDVenue:cdVenue partial:partial];
        }
        
        if (completion) {
            completion(venue);
        }
    }];
}

- (void)fetchVenuesForKeyword:(NSString *)keyword offset:(NSUInteger)offset maxResults:(NSUInteger)maxResults completion:(void(^)(NSArray *venues))completion
{
    [_context performBlock:^{
        
        NSArray *venues = nil;
        
        NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([CDVenue class])];
        NSMutableArray *predicates = @[].mutableCopy;
        if (keyword) {
            [predicates addObject:[NSPredicate predicateWithFormat:@"(name CONTAINS[cd] %@) OR (perex CONTAINS[cd] %@) OR (address CONTAINS[cd] %@)", keyword, keyword, keyword]];
        }
        [predicates addObject:[NSCompoundPredicate orPredicateWithSubpredicates:@[
            [NSPredicate predicateWithFormat:@"type = %@", @(PlaceVenue)],
            [NSCompoundPredicate andPredicateWithSubpredicates:@[
                [NSPredicate predicateWithFormat:@"type = %@", @(EventVenue)],
                [NSPredicate predicateWithFormat:@"dateStart >= %@", [NSDate date]]
            ]]
        ]]];
        request.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
        request.fetchOffset = offset;
        request.fetchLimit = maxResults;
        
        NSArray *fetchedObjects = [_context executeFetchRequest:request error:nil];
        if (fetchedObjects) {
            NSMutableArray *ma = @[].mutableCopy;
            for (CDVenue *cdVenue in fetchedObjects) {
                Venue *venue = [self venueFromCDVenue:cdVenue partial:YES];
                if (venue) {
                    [ma addObject:venue];
                }
            }
            venues = [NSArray arrayWithArray:ma];
        }
        
        if (completion) {
            completion(venues);
        }
    }];
}

- (void)fetchVenuesForGridCell:(MapGridCell *)gridCell filter:(VenueFilter *)filter userId:(NSNumber *)userId maxResults:(NSUInteger)maxResults sortByRating:(BOOL)sortByRatingArg completion:(void(^)(NSArray *venues))completion
{
    [_context performBlock:^{
        
        NSArray *venues = nil;
        
        NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([CDVenue class])];
        NSMutableArray *predicates = @[].mutableCopy;
        
        if (gridCell) {
            [predicates addObject:[NSPredicate predicateWithFormat:
                                   @"(%@ <= longitude) AND (longitude <= %@)"
                                   @"AND (%@ <= latitude) AND (latitude <= %@)",
                                   @(gridCell.longitudeMin), @(gridCell.longitudeMax), @(gridCell.latitudeMin), @(gridCell.latitudeMax)]];
//            [predicates addObject:[NSPredicate predicateWithFormat:
//                                   @"(%@ <= longitude) AND (longitude <= %@)"
//                                   @"AND (%@ <= latitude) AND (latitude <= %@)",
//                                   @(18.025564), @(18.443045), @(49.733497), @(49.911581)]];
        } else {
            request = nil;
        }
        
        NSArray *filterPredicates = [self createPredicatesForVenueFilter:filter userId:userId];
        if (filterPredicates.count > 0) {
            [predicates addObjectsFromArray:filterPredicates];
        }
        
        [predicates addObject:[NSCompoundPredicate orPredicateWithSubpredicates:@[
            [NSPredicate predicateWithFormat:@"type = %@", @(PlaceVenue)],
            [NSCompoundPredicate andPredicateWithSubpredicates:@[
                [NSPredicate predicateWithFormat:@"type = %@", @(EventVenue)],
                [NSPredicate predicateWithFormat:@"dateStart >= %@", [NSDate date]]
            ]]
        ]]];
        request.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
        
        BOOL sortByRating = sortByRatingArg;
        if (filter.type == BestRatingFilter) {
            sortByRating = YES;
        }
        
        if (sortByRating) {
            request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"rating" ascending:NO]];
        }
        request.fetchLimit = maxResults;
        
        if (request) {
            NSArray *fetchedObjects = [_context executeFetchRequest:request error:nil];
            if (fetchedObjects) {
                NSMutableArray *ma = @[].mutableCopy;
                for (CDVenue *cdVenue in fetchedObjects) {
                    Venue *venue = [self venueFromCDVenue:cdVenue partial:YES];
                    if (venue) {
                        [ma addObject:venue];
                    }
                }
                venues = [NSArray arrayWithArray:ma];
            }
        }
        
        if (completion) {
            completion(venues);
        }
    }];
}

- (void)fetchVenuesForFilter:(VenueFilter *)filter location:(CLLocation *)location completion:(void (^)(NSArray *venues))completion
{
    [_context performBlock:^{
        
        NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([CDVenue class])];
        request.returnsObjectsAsFaults = NO;
        request.fetchLimit = 300;
        
        NSMutableArray *predicates = @[].mutableCopy;
        
        if (location) {
            static double const D = 200.*1000. * 1.1;
            double const R = 6371009.; // Earth readius in meters
            double meanLatitidue = location.coordinate.latitude * M_PI / 180.;
            double deltaLatitude = D / R * 180. / M_PI;
            double deltaLongitude = D / (R * cos(meanLatitidue)) * 180. / M_PI;
            double minLatitude = location.coordinate.latitude - deltaLatitude;
            double maxLatitude = location.coordinate.latitude + deltaLatitude;
            double minLongitude = location.coordinate.longitude - deltaLongitude;
            double maxLongitude = location.coordinate.longitude + deltaLongitude;
            [predicates addObject:[NSPredicate predicateWithFormat:
                                   @"(%@ <= longitude) AND (longitude <= %@)"
                                   @"AND (%@ <= latitude) AND (latitude <= %@)",
                                   @(minLongitude), @(maxLongitude), @(minLatitude), @(maxLatitude)]];
        }
        
        
        CDUser *cdCurrentUser = [self fetchCurrentUser];
        NSNumber *userId = cdCurrentUser.anonymous.boolValue ? nil : cdCurrentUser.identifier.copy;
        
        NSArray *filterPredicates = [self createPredicatesForVenueFilter:filter userId:userId];
        if (filterPredicates.count > 0) {
            [predicates addObjectsFromArray:filterPredicates];
        }
        
        request.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
        
        if (filter.type == BestRatingFilter) {
            request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"rating" ascending:NO]];
        }
        
        NSArray *fetchedObjects = [_context executeFetchRequest:request error:nil];
        
        NSArray *venues = @[];
        if (fetchedObjects) {
            NSMutableArray *tmpv = @[].mutableCopy;
            for (CDVenue *cdVenue in fetchedObjects) {
                Venue *venue = [self venueFromCDVenue:cdVenue partial:YES];
                if (location && filter.type != BestRatingFilter) {
                    CLLocation *venueLocation = [[CLLocation alloc] initWithLatitude:venue.coordinate.latitude longitude:venue.coordinate.longitude];
                    venue.currentDistance = [venueLocation distanceFromLocation:location];
                }
                [tmpv addObject:venue];
            }
            
            if (location && filter.type != BestRatingFilter) {
                venues = [tmpv sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"currentDistance" ascending:YES]]];
            } else {
                venues = [tmpv sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"rating" ascending:YES]]];
            }
        }
        
        if (completion) {
            completion(venues);
        }
        
    }];
}

- (NSArray *)createPredicatesForVenueFilter:(VenueFilter *)filter userId:(NSNumber *)userId
{
    NSMutableArray *predicates = @[].mutableCopy;
    if (filter.night) {
        [predicates addObject:[NSPredicate predicateWithFormat:@"showInNight = %@", @(YES)]];
    } else {
        [predicates addObject:[NSPredicate predicateWithFormat:@"showInDay = %@", @(YES)]];
    }
    switch (filter.type) {
        case DefaultFilter:
            break;
        case FavoritesFilter:
        {
            if (userId) {
                [predicates addObject:[NSPredicate predicateWithFormat:@"ANY usersThatFavorited.identifier = %@", userId]];
            }
        }
            break;
        case ScheduleFilter:
        {
            if (userId) {
                [predicates addObject:[NSPredicate predicateWithFormat:@"ANY usersThatScheduled.identifier = %@", userId]];
            }
        }
            break;
        case PlacesFilter:
            [predicates addObject:[NSPredicate predicateWithFormat:@"type = %@", @(PlaceVenue)]];
            break;
        case EventsFilter:
            [predicates addObject:[NSPredicate predicateWithFormat:@"type = %@", @(EventVenue)]];
            break;
        case TrendingFilter:
            [predicates addObject:[NSPredicate predicateWithFormat:@"trending = %@", @(YES)]];
            break;
        case CategoriesFilter:
            if (filter.categoryIds.count > 0) {
                NSMutableArray *categoryPredicate = @[].mutableCopy;
                for (NSNumber *identifier in filter.categoryIds) {
                    if (identifier) {
                        [categoryPredicate addObject:[NSPredicate predicateWithFormat:@"ANY categories.identifier = %@", identifier]];
                    }
                }
                [predicates addObject:[NSCompoundPredicate orPredicateWithSubpredicates:categoryPredicate]];
            }
            break;
        case DateFilter:
            if (filter.startDate) {
                [predicates addObject:[NSPredicate predicateWithFormat:@"dateStart >= %@", filter.startDate]];
            }
            if (filter.endDate) {
                [predicates addObject:[NSPredicate predicateWithFormat:@"dateStart >= %@", filter.endDate]];
            }
            break;
        default:
            break;
    }
    return predicates;
}

- (void)persistVenues:(NSArray *)venues completion:(void(^)(NSArray *venues))completion
{
    [_context performBlock:^{
        
        NSLog(@"persist venues");
        NSMutableArray *persistedVenues = @[].mutableCopy;
        
        for (Venue *venue in venues) {
            
            CDVenue *cdVenue = nil;
            
            if (!venue.nodeIdentifier) {
                continue;
            }
            
            NSFetchRequest* request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([CDVenue class])];
            NSMutableArray *predicates = @[].mutableCopy;
            [predicates addObject:[NSPredicate predicateWithFormat:@"nodeIdentifier = %@", venue.nodeIdentifier]];
            if (venue.dateIdentifier && [venue.dateIdentifier integerValue] > 0) {
                [predicates addObject:[NSPredicate predicateWithFormat:@"dateIdentifier = %@", venue.dateIdentifier]];
            }
            request.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
            
            NSArray *result = [_context executeFetchRequest:request error:nil];
            if (result.count > 0) {
                cdVenue = [result firstObject];
            } else {
                cdVenue = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([CDVenue class]) inManagedObjectContext:_context];
                cdVenue.dateCreated = [NSDate date];
            }
            
            cdVenue.aliasPath = venue.aliasPath;
            cdVenue.isPartial = venue.isPartial;
            cdVenue.showInNight = venue.showInNight;
            cdVenue.showInDay = venue.showInDay;
            cdVenue.dateUpdated = [NSDate date];
            cdVenue.identifier = venue.identifier;
            cdVenue.dateIdentifier = venue.dateIdentifier;
            cdVenue.nodeIdentifier = venue.nodeIdentifier;
            cdVenue.trending = venue.trending;
            cdVenue.name = venue.name;
            cdVenue.perex = venue.perex;
            cdVenue.address = venue.address;
            cdVenue.latitude = venue.latitude;
            cdVenue.longitude = venue.longitude;
            cdVenue.type = venue.type;
            cdVenue.dateEnd = venue.dateEnd;
            cdVenue.dateStart = venue.dateStart;
            cdVenue.rating = venue.rating;
            cdVenue.headliners = venue.headliners;
            cdVenue.linkOfficial = venue.linkOfficial;
            cdVenue.linkOpeningHours = venue.linkOpeningHours;
            cdVenue.linkTickets = venue.linkTickets;
            
            if (venue.photos.count > 0) {
                [cdVenue setPhotosArray:venue.photos];
            } else if (cdVenue.photosArray.count > 0 ) {
                NSMutableArray *tmpa = @[].mutableCopy;
                for (NSString *ph in cdVenue.photosArray) {
                    [tmpa addObject:ph];
                }
                venue.photos = tmpa.copy;
            }

            
            for (VenueCategory *category in venue.categories) {
                CDVenueCategory *cdCategory = [self fetchSingleManagedObjectForClass:[CDVenueCategory class] byIdentifier:category.identifier];
                if (cdCategory) {
                    [cdVenue.mutableCategories addObject:cdCategory];
                    category.name = cdCategory.name;
                } else {
                    cdCategory = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([CDVenueCategory class]) inManagedObjectContext:_context];
                    cdCategory.identifier = category.identifier;
                    [cdVenue.mutableCategories addObject:cdCategory];
                }
            }
            
            if (venue.deals.count > 0) {
                [cdVenue.deals.copy enumerateObjectsUsingBlock:^(CDDeal *deal, NSUInteger idx, BOOL *stop) {
                    [cdVenue.mutableDeals removeObject:deal];
                }];
                for (Deal *deal in venue.deals) {
                    CDDeal *cdDeal = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([CDDeal class]) inManagedObjectContext:_context];
                    cdDeal.companyName = deal.companyName;
                    cdDeal.deals = deal.deals;
                    cdDeal.linkOfficial = deal.linkOfficial;
                    cdDeal.longDescription = deal.longDescription;
                    cdDeal.notes = deal.notes;
                    [cdVenue.mutableDeals addObject:cdDeal];
                }
            }
            
            if (venue.highlights.count > 0) {
                [cdVenue.highlights.copy enumerateObjectsUsingBlock:^(CDHighlight *highlight, NSUInteger idx, BOOL *stop) {
                    [cdVenue.mutableHighlights removeObject:highlight];
                }];
                for (Highlight *highlight in venue.highlights) {
                    CDHighlight *cdHighlight = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([CDHighlight class]) inManagedObjectContext:_context];
                    cdHighlight.title = highlight.title;
                    cdHighlight.text = highlight.text;
                    [cdVenue.mutableHighlights addObject:cdHighlight];
                }
            }
            
            if (venue.transporters.count > 0) {
                [cdVenue.transporters.copy enumerateObjectsUsingBlock:^(CDTransporter *transporter, NSUInteger idx, BOOL *stop) {
                    [cdVenue.mutableTransporters removeObject:transporter];
                }];
                for (Transporter *transporter in venue.transporters) {
                    CDTransporter *cdTransporter = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([CDTransporter class]) inManagedObjectContext:_context];
                    cdTransporter.departures = transporter.departures;
                    cdTransporter.longDescription = transporter.longDescription;
                    cdTransporter.destinations = transporter.destinations;
                    cdTransporter.discounts = transporter.discounts;
                    cdTransporter.linkOfficial = transporter.linkOfficial;
                    cdTransporter.name = transporter.name;
                    cdTransporter.notes = transporter.notes;
                    cdTransporter.prices = transporter.prices;
                    cdTransporter.latitude = transporter.latitude;
                    cdTransporter.longitude = transporter.longitude;
                    cdTransporter.address = transporter.address;
                    cdTransporter.openingHours = transporter.openingHours;
                    cdTransporter.phone = transporter.phone;
                    [cdVenue.mutableTransporters addObject:cdTransporter];
                }
            }
            
            if (venue.descriptionItems.count > 0) {
                [cdVenue.descriptionItems.copy enumerateObjectsUsingBlock:^(CDVenueDescriptionItem *descriptionItem, NSUInteger idx, BOOL *stop) {
                    [cdVenue.mutableDescriptionItems removeObject:descriptionItem];
                }];
                for (CDVenueDescriptionItem *descriptionItem in venue.descriptionItems) {
                    CDVenueDescriptionItem *cdDescriptionItem = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([CDVenueDescriptionItem class]) inManagedObjectContext:_context];
                    cdDescriptionItem.type = descriptionItem.type;
                    cdDescriptionItem.content = descriptionItem.content;
                    cdDescriptionItem.link = descriptionItem.link;
                    cdDescriptionItem.thumbnail = descriptionItem.thumbnail;
                    [cdVenue.mutableDescriptionItems addObject:cdDescriptionItem];
                }
            }
            
            [persistedVenues addObject:venue];
        }
        [self saveContext];
        
        if (completion) {
            completion([NSArray arrayWithArray:persistedVenues]);
        }
        
    }];
}

- (void)deleteVenuesByArrayOfIdentifiers:(NSArray *)identifiers completion:(void(^)(void))completion
{
    [_context performBlock:^{
        if (identifiers.count > 0) {
            for (VenueIdentifier *vid in identifiers) {
                
                if (!vid.identifier) {
                    continue;
                }
                
                NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([CDVenue class])];
                NSMutableArray *predicates = @[].mutableCopy;
                [predicates addObject:[NSPredicate predicateWithFormat:@"identifier = %@", vid.identifier]];
                if (vid.dateIdentifier != nil && [vid.dateIdentifier integerValue] > 0) {
                    [predicates addObject:[NSPredicate predicateWithFormat:@"dateIdentifier = %@", vid.dateIdentifier]];
                }
                request.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
                NSArray *result = [_context executeFetchRequest:request error:nil];
                if (result.count > 0) {
                    CDVenue *venue = [result firstObject];
                    [_context deleteObject:venue];
                    NSLog(@"deleted id: %@", vid);
                } else {
                    NSLog(@"not deleted, because not found locally %@", vid);
                }
                
            }
            
            CDAppSettings *appSettings = [self fetchSingleManagedObjectForClass:[CDAppSettings class]];
            if (appSettings) {
                appSettings.deletedLastUpdate = [NSDate date];
            }
            
            [self saveContext];
        }
        
        if (completion) {
            completion();
        }
    }];
}

- (void)filterOutExistingVenueIdentifiers:(NSArray *)identifiers completion:(void (^)(NSArray *missingIdentifiers))completion
{
    [_context performBlock:^{
       
        NSMutableArray *queue = identifiers.mutableCopy;
        NSMutableArray *missing = @[].mutableCopy;
        
        while (queue.count > 0) {
            
            VenueIdentifier *vid = [queue pop];
            CDVenue *cdVenue = [self fetchVenueWithIdentifier:vid];
            if (!cdVenue) {
                [missing addObject:vid];
            }
            
        }
        
        if (completion) {
            completion([NSArray arrayWithArray:missing]);
        }
        
    }];
}

#pragma mark - User

- (CDUser *)fetchCurrentUser
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([CDUser class])];
    fetchRequest.returnsObjectsAsFaults = NO;
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"signedIn = %@", @(YES)]];
    CDUser *user = nil;
    NSArray *fetchedObjects = [_context executeFetchRequest:fetchRequest error:nil];
    if (fetchedObjects.count > 0) {
        user = [fetchedObjects firstObject];
    } else {
        NSFetchRequest *annonymousfetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([CDUser class])];
        [annonymousfetchRequest setPredicate:[NSPredicate predicateWithFormat:@"anonymous = %@", @(YES)]];
        fetchedObjects = [_context executeFetchRequest:annonymousfetchRequest error:nil];
        if (fetchedObjects.count > 0) {
            user = [fetchedObjects firstObject];
        }
    }
    return user;
}

- (void)persistLastSearchedKeyword:(NSString *)keyword completion:(void(^)(void))completion
{
    [_context performBlock:^{
        
        if (keyword) {
            CDUser *user = [self fetchCurrentUser];
            if (user) {
                CDSearchTerm *term = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([CDSearchTerm class]) inManagedObjectContext:_context];
                term.text = keyword.copy;
                [user.mutableLastSearchTerms insertObject:term atIndex:0];
                [user.mutableLastSearchTerms.copy enumerateObjectsUsingBlock:^(CDSearchTerm *term, NSUInteger idx, BOOL *stop) {
                    if (idx > 5) {
                        [user.mutableLastSearchTerms removeObject:term];
                    }
                }];
                [self saveContext];
            }
        }

        if (completion) {
            completion();
        }

    }];
}

- (void)fetchLastSearchedKeywordsWithCompletion:(void(^)(NSArray *lastSearchedKeywords))completion
{
    [_context performBlock:^{
       
        NSMutableArray *lastSearchedKeywords = @[].mutableCopy;
        
        CDUser *user = [self fetchCurrentUser];
        if (user) {
            for (CDSearchTerm *term in user.lastSearchTerms) {
                [lastSearchedKeywords addObject:term.text.copy];
            }
        }
        
        if (completion) {
            completion([NSArray arrayWithArray:lastSearchedKeywords]);
        }
        
    }];
}

- (void)fetchCurrentUserWithCompletion:(void(^)(User *user))completion
{
    [_context performBlock:^{
       
        User *user = [self userFromCDUser:[self fetchCurrentUser]];
        
        if (completion) {
            completion(user);
        }
    }];
}

- (void)fetchUserWithIdentifier:(NSNumber *)identifier completion:(void(^)(User *user))completion
{
    [_context performBlock:^{
        User *user = nil;
        CDUser *cdUser = [self fetchSingleManagedObjectForClass:[CDUser class] byIdentifier:identifier];
        if (cdUser) {
            user = [self userFromCDUser:cdUser];
        }
        if (completion) {
            completion(user);
        }
    }];
}

- (void)persistUser:(User *)user completion:(void(^)(User *user))completion
{
    [_context performBlock:^{
        
        if (user) {
            
            CDUser *cdUser = nil;
            if (user.identifier) {
                NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([CDUser class])];
                [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"identifier = %@", user.identifier]];
                NSArray *fetchedObjects = [_context executeFetchRequest:fetchRequest error:nil];
                if (fetchedObjects.count > 0) {
                    cdUser = [fetchedObjects firstObject];
                }
            }
            
            if (!cdUser) {
                cdUser = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([CDUser class]) inManagedObjectContext:_context];
            }
            
            cdUser.anonymous = user.anonymous;
            cdUser.about = user.about;
            cdUser.avatarId = user.avatarId;
            cdUser.birthday = user.birthday;
            cdUser.emailNotifications = user.emailNotifications;
            cdUser.facebookId = user.facebookId;
            cdUser.lastSynchronization = user.lastSynchronization;
            cdUser.identifier = user.identifier;
            cdUser.signedIn = user.signedIn;
            cdUser.language = user.language;
            cdUser.email = user.email;
            cdUser.location = user.location;
            cdUser.name = user.name;
            if (user.sessionToken) {
                cdUser.sessionToken = user.sessionToken;
            }
            
            [self saveContext];
        }
        
        if (completion) {
            completion(user);
        }
    }];
}

- (void)persistCurrentUserChanges:(User *)userChanges completion:(void(^)(User *currentUser))completion
{
    [_context performBlock:^{
        
        User *user = nil;
        
        CDUser *cdUser = [self fetchCurrentUser];

        if (userChanges && cdUser.signedIn.boolValue && !cdUser.anonymous.boolValue) {
            
            cdUser.about = userChanges.about;
            cdUser.avatarId = userChanges.avatarId;
            cdUser.birthday = userChanges.birthday;
            cdUser.emailNotifications = userChanges.emailNotifications;
            cdUser.language = userChanges.language;
            cdUser.location = userChanges.location;
            cdUser.name = userChanges.name;
            
            [self saveContext];
            
            user = [self userFromCDUser:[self fetchCurrentUser]];
        }
        
        if (completion) {
            completion(user);
        }
    }];
}


- (void)signInUser:(User *)downloadedUser completion:(void(^)(User *user))completion
{
    [self fetchUserWithIdentifier:downloadedUser.identifier completion:^(User *storedUser) {
        
        if (storedUser) {
            downloadedUser.lastSynchronization = storedUser.lastSynchronization;
        }
        
        [self persistUser:downloadedUser completion:^(User *user) {
            [_context performBlock:^{
                if (user.identifier) {
                    
                    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([CDUser class])];
                    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"identifier = %@", user.identifier]];
                    NSArray *fetchedObjects = [_context executeFetchRequest:fetchRequest error:nil];
                    if (fetchedObjects.count > 0) {
                        CDUser *cdUser = [fetchedObjects firstObject];
                        if (!cdUser.anonymous.boolValue) {
                            cdUser.signedIn = @(YES);
                            [self saveContext];
                        }
                    }
                    
                }
                
                if (completion) {
                    completion(user);
                }
            }];
        }];
        
    }];
    
}

- (void)signOutCurrentUserWithCompletion:(void(^)(void))completion
{
    [_context performBlock:^{
        CDUser *cdUser = [self fetchCurrentUser];
        if (!cdUser.anonymous.boolValue && cdUser.signedIn.boolValue) {
            cdUser.signedIn = @(NO);
            [self saveContext];
        }
    
        if (completion) {
            completion();
        }
    }];
}

- (void)fetchFavoritesWithCompletion:(void(^)(NSOrderedSet *favorites, BOOL userSigned))completion
{
    [_context performBlock:^{
        NSMutableOrderedSet *ms = [NSMutableOrderedSet new];
        
        CDUser *cdUser = [self fetchCurrentUser];
        BOOL userSigned = !cdUser.anonymous.boolValue && cdUser.signedIn.boolValue;
        if (userSigned) {
            for (CDVenue *cdVenue in cdUser.favoriteVenues) {
                Venue *venue = [self venueFromCDVenue:cdVenue partial:NO];
                if (venue) {
                    [ms addObject:venue];
                }
            }
        }
        
        if (completion) {
            completion([NSOrderedSet orderedSetWithOrderedSet:ms], userSigned);
        }
    }];
}

- (void)fetchScheduledWithCompletion:(void(^)(NSOrderedSet *scheduled, BOOL userSigned))completion
{
    [_context performBlock:^{
        NSMutableOrderedSet *ms = [NSMutableOrderedSet new];
        
        CDUser *cdUser = [self fetchCurrentUser];
        BOOL userSigned = !cdUser.anonymous.boolValue && cdUser.signedIn.boolValue;
        if (userSigned) {
            for (CDVenue *cdVenue in cdUser.scheduledVenues) {
                Venue *venue = [self venueFromCDVenue:cdVenue partial:NO];
                if (venue) {
                    [ms addObject:venue];
                }
            }
        }
        
        if (completion) {
            completion([NSOrderedSet orderedSetWithOrderedSet:ms], userSigned);
        }
    }];
}

- (void)persistFavoriteIdentifiers:(NSOrderedSet *)favorites completion:(void(^)(void))completion
{
    [_context performBlock:^{
        if (favorites) {
            
            CDUser *cdUser = [self fetchCurrentUser];
            if (!cdUser.anonymous.boolValue && cdUser.signedIn.boolValue) {
                
                [cdUser.favoriteVenues.copy enumerateObjectsUsingBlock:^(CDVenue *cdVenue, NSUInteger idx, BOOL *stop) {
                    [cdUser.mutableFavoriteVenues removeObject:cdVenue];
                }];
                for (VenueIdentifier *vid in favorites) {
                    CDVenue *cdVenue = [self fetchVenueWithIdentifier:vid];
                    if (cdVenue) {
                        [cdUser.mutableFavoriteVenues addObject:cdVenue];
                    }
                }
                
                [self saveContext];
            }
            
        }
        
        if (completion) {
            completion();
        }
    }];
}

- (void)persistScheduledIdentifiers:(NSOrderedSet *)scheduled completion:(void(^)(void))completion
{
    [_context performBlock:^{
        if (scheduled) {
            
            CDUser *cdUser = [self fetchCurrentUser];
            if (!cdUser.anonymous.boolValue && cdUser.signedIn.boolValue) {
                
                [cdUser.scheduledVenues.copy enumerateObjectsUsingBlock:^(CDVenue *cdVenue, NSUInteger idx, BOOL *stop) {
                    [cdUser.mutableScheduledVenues removeObject:cdVenue];
                }];
                for (VenueIdentifier *vid in scheduled) {
                    CDVenue *cdVenue = [self fetchVenueWithIdentifier:vid];
                    if (cdVenue) {
                        [cdUser.mutableScheduledVenues addObject:cdVenue];
                    }
                }
                
                [self saveContext];
            }
            
        }
        
        if (completion) {
            completion();
        }
    }];
}

- (void)persistFavoritesIdentifiers:(NSOrderedSet *)favorites scheduledIdentifiers:(NSOrderedSet *)scheduled completion:(void(^)(void))completion
{
    [self persistFavoriteIdentifiers:favorites completion:^{
        [self persistScheduledIdentifiers:scheduled completion:^{
            if (completion) {
                completion();
            }
        }];
    }];
}

- (void)fetchSynchronizationDataForCurrentUserWithCompletion:(void(^)(NSDictionary *synchronizationData))completion
{
    [_context performBlock:^{
        NSDictionary *synchronizationData = nil;
        CDUser *cdUser = [self fetchCurrentUser];
        if (!cdUser.anonymous.boolValue && cdUser.signedIn.boolValue && cdUser.sessionToken) {
            
            NSString *lastChange = [NSString stringWithFormat:@"%lld", ((long long)cdUser.lastSynchronization.timeIntervalSince1970) * 1000];
            NSMutableArray *favorites = @[].mutableCopy;
            for (CDVenue *cdVenue in cdUser.favoriteVenues) {
                [favorites addObject:@{
                                       @"nodeID":cdVenue.nodeIdentifier,
                                       @"dateID":cdVenue.dateIdentifier ? cdVenue.dateIdentifier : @"null"
                                       }];
            }
            NSMutableArray *scheduled = @[].mutableCopy;
            for (CDVenue *cdVenue in cdUser.scheduledVenues) {
                [scheduled addObject:@{
                                       @"nodeID":cdVenue.nodeIdentifier,
                                       @"dateID":cdVenue.dateIdentifier ? cdVenue.dateIdentifier : @"null"
                                       }];
            }
            
            synchronizationData = @{
                @"authToken":cdUser.sessionToken.copy,
                @"favorites": @{
                    @"lastChange":lastChange,
                    @"list": favorites
                },
                @"scheduled": @{
                    @"lastChange":lastChange,
                    @"list": scheduled
                }
            };

        }
        if (completion) {
            completion(synchronizationData);
        }
    }];
}

#pragma mark - Categories

- (void)fetchCategoriesWithCompletion:(void(^)(NSOrderedSet *categories))completion
{
    [_context performBlock:^{
        NSMutableOrderedSet *categories = [NSMutableOrderedSet new];
        
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([CDVenueCategory class])];
        request.predicate = [NSPredicate predicateWithFormat:@"name.length > 0"];
        request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
        NSArray *fetchedObjects = [_context executeFetchRequest:request error:nil];
        if (fetchedObjects.count > 0) {
            for (CDVenueCategory *cdCategory in fetchedObjects) {
                VenueCategory *category = [self categoryFromCDCategory:cdCategory];
                [categories addObject:category];
            }
        }
        
        if (completion) {
            completion([[NSOrderedSet alloc] initWithOrderedSet:categories]);
        }
    }];
}

- (void)persistCategories:(NSArray *)categories completion:(void(^)(void))completion
{
    [_context performBlock:^{
        
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([CDVenueCategory class])];
        request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]];
        NSArray *fetchedObjects = [_context executeFetchRequest:request error:nil];
        NSMutableDictionary *existingCDCategories = @{}.mutableCopy;
        for (CDVenueCategory *category in fetchedObjects) {
            existingCDCategories[category.identifier] = category;
        }
        
        for (VenueCategory *category in categories) {
            CDVenueCategory *cdCategory = existingCDCategories[category.identifier];
            if (!cdCategory) {
                cdCategory = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([CDVenueCategory class]) inManagedObjectContext:_context];
            }
            cdCategory.identifier = category.identifier;
            cdCategory.name = category.name;
            cdCategory.showInNight = category.showInNight;
        }
        
        CDAppSettings *appSettings = [self fetchSingleManagedObjectForClass:[CDAppSettings class]];
        appSettings.venueCategoriesLastUpdate = [NSDate date];
        
        [self saveContext];
        
        if (completion) {
            completion();
        }
    }];
}

#pragma mark - MapGrid

- (void)fetchMapGridForCzechRepublicWithCompletion:(void(^)(MapGridCell *czechRepublic))completion
{
    [_context performBlock:^{
        MapGridCell *czechRepublic = nil;
        
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([CDMapGridCell class])];
        request.predicate = [NSPredicate predicateWithFormat:@"identifier = %@", @(1)];
        request.returnsObjectsAsFaults = NO;
        NSArray *fetchedObjects = [_context executeFetchRequest:request error:nil];
        if (fetchedObjects.count > 0) {
            CDMapGridCell *cdCell = [fetchedObjects firstObject];
            MapGridCell *cell = [self mapGridCellFromCDMapGridCell:cdCell];
            if (cell) {
                czechRepublic = cell;
            }
        }
        
        if (completion) {
            completion(czechRepublic);
        }
    }];
}

- (void)fetchMapGridInsideCzechRepublicWithCompletion:(void(^)(NSArray *mapGridsInsideCzechRepublic))completion
{
    [_context performBlock:^{
        NSMutableArray *mapGridsInsideCzechRepublic = @[].mutableCopy;
        
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([CDMapGridCell class])];
        request.predicate = [NSPredicate predicateWithFormat:@"identifier > %@", @(1)];
        request.returnsObjectsAsFaults = NO;
        NSArray *fetchedObjects = [_context executeFetchRequest:request error:nil];
        if (fetchedObjects.count > 0) {
            for (CDMapGridCell *cdCell in fetchedObjects) {
                MapGridCell *cell = [self mapGridCellFromCDMapGridCell:cdCell];
                if (cell) {
                    [mapGridsInsideCzechRepublic addObject:cell];
                }
            }
        }
        
        if (completion) {
            completion([NSMutableArray arrayWithArray:mapGridsInsideCzechRepublic]);
        }
    }];
}

- (void)persistLastServerRequestForMapGridCell:(MapGridCell *)cell completion:(void(^)(void))completion
{
    [_context performBlock:^{
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([CDMapGridCell class])];
        request.predicate = [NSPredicate predicateWithFormat:@"identifier = %@", cell.identifier];
        request.returnsObjectsAsFaults = NO;
        NSArray *fetchedObjects = [_context executeFetchRequest:request error:nil];
        if (fetchedObjects.count > 0) {
            CDMapGridCell *cdCell = [fetchedObjects firstObject];
            cdCell.lastServerRequest = cell.lastServerRequest;
            [self saveContext];
        }
        if (completion) {
            completion();
        }
    }];
}

@end
