//
//  CDMusic.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/3/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import Foundation
import CoreData

@objc(CDMusic)
class CDMusic: NSManagedObject {

    @NSManaged var band: String
    @NSManaged var assetPersistentID: String
    @NSManaged var assetPath: String
    @NSManaged var localPath: String
    @NSManaged var name: String
    @NSManaged var album: String
    @NSManaged var rhythm: String
    @NSManaged var barsCount: Int64
    @NSManaged var averageBPM: Double
    @NSManaged var bars: NSOrderedSet
    @NSManaged var beats: NSOrderedSet
    @NSManaged var segments: NSOrderedSet
    @NSManaged var usedInFigures: NSSet
    @NSManaged var structureJSON: String
    
}
