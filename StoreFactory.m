//
//  StoreFactory.m
//  cztep
//
//  Created by Ondrej Macoszek on 9/10/14.
//  Copyright (c) 2014 Symbio Digital. All rights reserved.
//

#import "StoreFactory.h"

@implementation StoreFactory

- (instancetype)initWithStoreURL:(NSURL*)storeURL modelURL:(NSURL*)modelURL
{
    self = [super init];
    if (self) {
        NSError *error = nil;
        self.managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
        self.persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
        if (![self.persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                      configuration:nil
                                                                URL:storeURL
                                                            options:@{
                                                                      NSInferMappingModelAutomaticallyOption: @YES,
                                                                      NSMigratePersistentStoresAutomaticallyOption: @YES,
                                                                      NSSQLitePragmasOption: @{@"synchronous": @"OFF"}
                                                                      }
                                                              error:&error]) {
            @throw [NSException exceptionWithName:CZTException reason:[NSString stringWithFormat:@"Error adding persistent store. %@, %@", error, error.userInfo] userInfo:nil];
        }
    }
    return self;
}

- (NSManagedObjectContext *)createNewMainQueueContext
{
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    context.persistentStoreCoordinator = self.persistentStoreCoordinator;
//    context.parentContext = _someParentContext;
    context.mergePolicy = NSMergeByPropertyStoreTrumpMergePolicy;
    return context;
}

- (NSManagedObjectContext *)createNewBackgroundQueueContext
{
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    context.persistentStoreCoordinator = self.persistentStoreCoordinator;
    //    context.parentContext = _someParentContext;
    context.mergePolicy = NSMergeByPropertyStoreTrumpMergePolicy;
    return context;
    
}

@end
