//
//  CDFigure.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/3/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import Foundation
import CoreData

@objc(CDFigure)
class CDFigure: NSManagedObject {

    @NSManaged var bars: Int64
    @NSManaged var musicSpeed: Double
    @NSManaged var name: String
    @NSManaged var order: Int64
    @NSManaged var rhythm: String
    @NSManaged var dance: CDDance
    @NSManaged var flow: NSOrderedSet
    @NSManaged var music: CDMusic?
    @NSManaged var calls: NSOrderedSet

}
