//
//  StoreFactory.h
//  cztep
//
//  Created by Ondrej Macoszek on 9/10/14.
//  Copyright (c) 2014 Symbio Digital. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoreFactory : NSObject

@property (nonatomic, strong) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (instancetype)initWithStoreURL:(NSURL*)storeURL modelURL:(NSURL*)modelURL;
- (NSManagedObjectContext *)createNewMainQueueContext;
- (NSManagedObjectContext *)createNewBackgroundQueueContext;

@end
