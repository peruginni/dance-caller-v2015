//
//  CDDance.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/3/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import Foundation
import CoreData

@objc(CDDance)
class CDDance: NSManagedObject {

    @NSManaged var name: String
    @NSManaged var type: String
    @NSManaged var summary: String
    @NSManaged var figures: NSOrderedSet

}
