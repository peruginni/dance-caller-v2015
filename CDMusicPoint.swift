//
//  CDMusicBar.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/3/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import Foundation
import CoreData

@objc(CDMusicPoint)
class CDMusicPoint: NSManagedObject {

    @NSManaged var time: Double
    @NSManaged var note: String

}
