//
//  CDMove.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/3/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import Foundation
import CoreData

@objc(CDMove)
class CDMove: NSManagedObject {

    @NSManaged var bars: Int64
    @NSManaged var hold: String
    @NSManaged var name: String
    @NSManaged var section: String
    @NSManaged var explanation: String
    @NSManaged var who: String

}
