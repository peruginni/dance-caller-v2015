//
//  CDMusicSegment.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/3/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import Foundation
import CoreData

@objc(CDMusicSegment)
class CDMusicSegment: NSManagedObject {

    @NSManaged var startTime: Double
    @NSManaged var endTime: Double

}
