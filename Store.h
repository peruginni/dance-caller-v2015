//
//  PersistenceStack.h
//  DanceCaller
//
//  Created by Ondrej Macoszek on 4/10/14.
//  Copyright (c) 2014 Ondřej Macoszek. All rights reserved.
//

#import "CDAppSettings.h"
#import "CDDeal.h"
#import "CDHighlight.h"
#import "CDMapGridCell.h"
#import "CDSearchTerm.h"
#import "CDTransporter.h"
#import "CDUser.h"
#import "CDVenue.h"
#import "CDVenueCategory.h"
#import "CDVenueDescriptionItem.h"
#import "CDStaticPage.h"

#import "AppSettings.h"
#import "Deal.h"
#import "Highlight.h"
#import "MapGridCell.h"
#import "Review.h"
#import "SearchTerm.h"
#import "Transporter.h"
#import "User.h"
#import "Venue.h"
#import "VenueCategory.h"
#import "VenueDescriptionItem.h"
#import "StaticPage.h"

@interface Store : NSObject

@property (nonatomic, strong, readonly) NSManagedObjectContext *context;

- (id)initWithManagedObjectContext:(NSManagedObjectContext *)context;
- (NSError *)saveContext;

// AppSettings
- (void)fetchAppSettingsWithCompletion:(void(^)(AppSettings *appSettings))completion;
- (void)persistAppSettings:(AppSettings *)appSettings completion:(void(^)(void))completion;

// Static Page
- (void)fetchStaticPagesForLocale:(NSString *)locale withCompletion:(void(^)(NSArray *pages))completion;
- (void)persistStaticPageBundle:(NSDictionary *)bundle forLocale:(NSString *)locale completion:(void(^)(BOOL persisted))completion;

// Venues
- (BOOL)shouldDownloadVenue:(Venue *)venue;
- (void)fetchVenuesForFilter:(VenueFilter *)filter location:(CLLocation *)location completion:(void (^)(NSArray *venues))completion;
- (void)fetchVenueWithIdentifier:(VenueIdentifier *)identifier partial:(BOOL)partial completion:(void(^)(Venue *venue))completion;
- (void)fetchVenuesForKeyword:(NSString *)keyword offset:(NSUInteger)offset maxResults:(NSUInteger)maxResults completion:(void(^)(NSArray *venues))completion;
- (void)fetchVenuesForGridCell:(MapGridCell *)gridCell filter:(VenueFilter *)filter userId:(NSNumber *)userId maxResults:(NSUInteger)maxResults sortByRating:(BOOL)sortByRatingArg completion:(void(^)(NSArray *venues))completion;
- (void)persistVenues:(NSArray *)venues completion:(void(^)(NSArray *venues))completion;
- (void)deleteVenuesByArrayOfIdentifiers:(NSArray *)identifiers completion:(void(^)(void))completion;
- (void)filterOutExistingVenueIdentifiers:(NSArray *)identifiers completion:(void (^)(NSArray *missingIdentifiers))completion;

// User
- (void)persistLastSearchedKeyword:(NSString *)keyword completion:(void(^)(void))completion;
- (void)fetchLastSearchedKeywordsWithCompletion:(void(^)(NSArray *lastSearchedKeywords))completion;
- (void)fetchCurrentUserWithCompletion:(void(^)(User *user))completion;
- (void)fetchUserWithIdentifier:(NSNumber *)identifier completion:(void(^)(User *user))completion;
- (void)persistUser:(User *)user completion:(void(^)(User *user))completion;
- (void)persistCurrentUserChanges:(User *)user completion:(void(^)(User *currentUser))completion;
- (void)signInUser:(User *)user completion:(void(^)(User *user))completion;
- (void)signOutCurrentUserWithCompletion:(void(^)(void))completion;
- (void)fetchFavoritesWithCompletion:(void(^)(NSOrderedSet *favorites, BOOL userSigned))completion;
- (void)fetchScheduledWithCompletion:(void(^)(NSOrderedSet *scheduled, BOOL userSigned))completion;
- (void)persistFavoriteIdentifiers:(NSOrderedSet *)favorites completion:(void(^)(void))completion;
- (void)persistScheduledIdentifiers:(NSOrderedSet *)scheduled completion:(void(^)(void))completion;
- (void)persistFavoritesIdentifiers:(NSOrderedSet *)favorites scheduledIdentifiers:(NSOrderedSet *)scheduled completion:(void(^)(void))completion;
- (void)fetchSynchronizationDataForCurrentUserWithCompletion:(void(^)(NSDictionary *synchronizationData))completion;

// Categories
- (void)fetchCategoriesWithCompletion:(void(^)(NSOrderedSet *categories))completion;
- (void)persistCategories:(NSArray *)categories completion:(void(^)(void))completion;

// MapGrid
- (void)fetchMapGridForCzechRepublicWithCompletion:(void(^)(MapGridCell *czechRepublic))completion;
- (void)fetchMapGridInsideCzechRepublicWithCompletion:(void(^)(NSArray *mapGridsInsideCzechRepublic))completion;
- (void)persistLastServerRequestForMapGridCell:(MapGridCell *)cell completion:(void(^)(void))completion;

@end