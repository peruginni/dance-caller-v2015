//
//  CDCall.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/3/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import Foundation
import CoreData

@objc(CDCall)
class CDCall: NSManagedObject {

    @NSManaged var identifier: String
    @NSManaged var sentence: String
    @NSManaged var time: Double
    @NSManaged var parentFigure: CDFigure

}
