//
//  BeatDetectionMatch.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 5/6/15.
//  Copyright (c) 2015 Ondrej Macoszek. All rights reserved.
//

import UIKit
import XCTest
import CoreData
import PromiseKit

class BeatDetectionMatch: XCTestCase {
    
    let storeUrl = (NSFileManager.defaultManager().URLForDirectory(.DocumentationDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: true, error: nil)?.URLByAppendingPathComponent("db.sqlite"))!
    let modelUrl = NSBundle.mainBundle().URLForResource("Main", withExtension: "momd")!
    var backgroundContext: NSManagedObjectContext!
    var musicLibrary: MusicLibrary!
    var musicAnalyzer: MusicAnalyzer!
    var beatMatch:[Music:Double] = [:]
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // Development support
        var error:NSError?
        //        NSFileManager.defaultManager().removeItemAtURL(storeUrl, error: &error)
        
        // Store
        var managedObjectModel = NSManagedObjectModel(contentsOfURL: modelUrl)
        var persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel!)
        persistentStoreCoordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: storeUrl, options: [
            NSInferMappingModelAutomaticallyOption : true,
            NSMigratePersistentStoresAutomaticallyOption: true,
            NSSQLitePragmasOption: ["synchronous": "OFF"]
            ], error: nil);
        self.backgroundContext = NSManagedObjectContext(concurrencyType: .PrivateQueueConcurrencyType)
        backgroundContext?.persistentStoreCoordinator = persistentStoreCoordinator
        backgroundContext?.mergePolicy = NSMergeByPropertyStoreTrumpMergePolicy
        
        musicLibrary = MusicLibrary(context: self.backgroundContext!)
        musicAnalyzer = MusicAnalyzer(context: self.backgroundContext!)
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testMatchOfDetectionWithAnnotation()
    {
        let readyExpectation = expectationWithDescription("ready")
        
        var analyzedMusic:[Music] = []
        
    
        self.musicLibrary.fetchAllTestMusicAnnotations().then(body: {musicAnnotations -> Void in
            
            var lastPromise:Promise<Void>! = nil;
            var i = 0;
            for annotation:TestMusicAnnotation in musicAnnotations {
//                if (i++ > 2) {
//                    break;
//                }
                
                var music = Music(name: annotation.musicId)
                var musicPath = NSBundle.mainBundle().pathForResource(annotation.musicId, ofType: "m4a")!
                analyzedMusic.append(music)
                if lastPromise != nil {
                    lastPromise = lastPromise.then(body: { Void in
                        return self.createBeatDetectionPromise(music, musicPath: musicPath, annotation: annotation)
                    })
                } else {
                    lastPromise = self.createBeatDetectionPromise(music, musicPath: musicPath, annotation: annotation);
                }
                
            }
            
            lastPromise.then(body: { Void in
                readyExpectation.fulfill()
            })
            
        })
        
        waitForExpectationsWithTimeout(3600, handler: { error in
            XCTAssertNil(error, "Error")
        })
        
        for (music, match) in self.beatMatch {
            println("\(music.name) ... \(match)")
        }
    }
    
    private func createBeatDetectionPromise(music:Music, musicPath:String, annotation:TestMusicAnnotation) -> Promise<Void>
    {
        return self.musicLibrary.detectBeatsAndBars(music, musicPath: musicPath, progressCallback:{ (processedMusic:Music, progressValue:Float, message:String) -> Void in
            println("Detecting beats in \(processedMusic.name) \(progressValue)% \(message)")
        }).then(body: { music -> Promise<Void> in
            var musicBeats:[Double] = []
            for p in music.beats {
                musicBeats.append(p.time)
            }
            var annotationBeats:[Double] = annotation.beats
            return self.musicAnalyzer.measureMusicalPointsMatch(annotationBeats, y: musicBeats).then(body: { match -> Void in
                println("Measured match \(match)")
                self.beatMatch[music] = match
            })
        }).then(body: { Void in
            println("Finished")
        })
    }
    
    
    
    
    
    
    //    self.progressCallback = { (processedMusic:Music, progressValue:Float, message:String) -> Void in
    //    var outMessage = progressValue > 1 ? String(format:"%.f%% %@", progressValue, message) : "\(message)"
    //    println(outMessage)
    //    self.allMusicStatus[processedMusic.assetPersistentID] = outMessage
    //    dispatch_async(dispatch_get_main_queue(),{
    //    self.songsTableView.reloadData()
    //    });
    //    }
    //
    //    if musicToAnalyze.count > 0 {
    //
    //    var music = musicToAnalyze[nextNotProcessedMusicIndex]
    //    nextNotProcessedMusicIndex++
    //    var musicPath = self.musicLibrary.musicDirectory().stringByAppendingPathComponent(music.localPath)
    //    var promise = self.musicLibrary.detectBeatsAndBars(music, musicPath: musicPath, progressCallback:progressCallback)
    //    chainNextPromiseToCurrentPromise(promise)
    //
    //    }
    //
    //
    //}
    //
    //func chainNextPromiseToCurrentPromise(prevPromise:Promise<Music>) -> Void {
    //    println("nextNotProcessedMusicIndex \(self.nextNotProcessedMusicIndex)")
    //    if self.nextNotProcessedMusicIndex < self.musicToAnalyze.count {
    //        var nextMusic = self.musicToAnalyze[self.nextNotProcessedMusicIndex]
    //        self.nextNotProcessedMusicIndex++
    //        var nextPromise = prevPromise.then(body: { prevMusic -> Promise<Music> in
    //            var musicPath = self.musicLibrary.musicDirectory().stringByAppendingPathComponent(nextMusic.localPath)
    //            return self.musicLibrary.detectBeatsAndBars(nextMusic, musicPath: musicPath, progressCallback:self.progressCallback)
    //        })
    //        self.chainNextPromiseToCurrentPromise(nextPromise)
    //    } else {
    //        prevPromise.then(body: { prevMusic -> Void in
    //            println("promises cleaned")
    //            self.musicToAnalyze = []
    //            self.saveButton.enabled = true
    //        })
    //    }
    //}
    
    

    
    
    
//    
//    func testExample() {
//        // This is an example of a functional test case.
//        XCTAssert(true, "Pass")
//    }
//    
//    func testPerformanceExample() {
//        // This is an example of a performance test case.
//        self.measureBlock() {
//            // Put the code you want to measure the time of here.
//        }
//    }
    
}
