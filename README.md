Dance Caller
============

Idea
----
User selects Irish dance and choose some Irish music. App will automatically detect beats inside selected music and schedule times, when to call upcoming dance movements. 

App was developed as part of my diploma thesis. 

Some highlights:

* Beat detecting algorithm is based on Comb filter method and implemented using **Accelerate** vDSP library. 
* Dance movements are called using dynamically **synthesized voice**. 
* Persisting data is done on **background NSManagedObjectContext** (no passing NSManagedObject between threads). 
* Model elevate **dependency injection via class constructor** (no custom singletons). 
* Nonblocking user interface is done with help of **PromiseKit**.

[Usage demo on YouTube](https://www.youtube.com/watch?v=mJAPur012t0)

[![video demo](http://img.youtube.com/vi/mJAPur012t0/mqdefault.jpg)](https://www.youtube.com/watch?v=mJAPur012t0)


Architecture
-----------------

![architecture](docs/architecture.png)

Project Structure
-----------------

![project structure](docs/project-structure.png)

Persistence model
-----------------

![persistence model](docs/persistence-model.png)

Screenshots
-----------

![Dance figure showing dance movements](docs/screenshots-figure-detail.png)

![Calling dance move](docs/screenshots-calling-dance-move.png)