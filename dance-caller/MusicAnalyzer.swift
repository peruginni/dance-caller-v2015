//
//  MusicAnalyzer.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 1/3/15.
//  Copyright (c) 2015 Ondrej Macoszek. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import PromiseKit
import MediaPlayer
import AVFoundation
import Accelerate

class MusicAnalyzer: NSObject {
    
    var context:NSManagedObjectContext!
    
    init(context:NSManagedObjectContext!)
    {
        self.context = context
    }
    
    func measureMusicalPointsMatch(x:[Double], y:[Double]) -> Promise<Double>
    {
        return Promise<Double> { (fulfill, reject) in
            var match:Double = 0;

            var (x2, y2) = self.alignToNearestSameSize(x, y: y);
            
            match = self.computeOndrasCorrelation(x2, y:y2)
            
//            if (x2.count == y2.count) {
//                match = self.computePearsonCorrelation(x2, y:y2);
//            }
            
            fulfill(match)
        };
    }
    
    func computeOndrasCorrelation(x:[Double], y:[Double], tolerance:Double = 0.2, strict:Bool = true) -> Double
    {
        var match:Double = 0;
        
        var xDistance:[Double] = Array(count: x.count, repeatedValue: 1);
        
        var xi = 0;
        var yi = 0;
        

        
        while (xi < x.count) {
            while (yi < y.count) {
                var diff = x[xi]-y[yi]
                println("\(x[xi]) \(y[yi])")
                if strict {
                    if (abs(diff) < tolerance) {
                        xDistance[xi] = diff;
                    }
                    yi++;
                    break;
                } else {

                    if (abs(diff) < tolerance) {
                        xDistance[xi] = diff;
                        yi++;
                        break;
                    } else if (diff > 0) { // first value is bigger
                        yi++;
                    } else { // second value is bigger
                        break;
                    }
                }
            }
            xi++;
        }
        
        var sumDistances:Double = 0;
        for (idx, val:Double) in enumerate(xDistance) {
            sumDistances = sumDistances + abs(val)
            println("i[\(idx)] = \(val)")
        }
        match = Double(sumDistances) / Double(xDistance.count)
        
        return match;
    }
    
//    func computePearsonCorrelation(x:[Double], y:[Double]) -> Double
//    {
//        var match:Double = 0;
//        
//        // compute pearson correlation
//        //http://www.socscistatistics.com/tests/pearson/Default2.aspx
//        var cX: UnsafeMutablePointer<Double> = UnsafeMutablePointer<Double>.alloc(x.count)
//        cX.initializeFrom(x)
//        var cY: UnsafeMutablePointer<Double> = UnsafeMutablePointer<Double>.alloc(y.count)
//        cY.initializeFrom(y)
//        var cB: UnsafeMutablePointer<Double> = UnsafeMutablePointer<Double>.alloc(1)
//        cB.initializeFrom([0.0])
//        
//        vDSP_sveD(
//            UnsafePointer<Double>(cX), 1 as vDSP_Stride,
//            cB, UInt(x.count) as vDSP_Length
//        )
//        var sumX = cB[0]
//        vDSP_meanvD(
//            UnsafePointer<Double>(cX), 1 as vDSP_Stride,
//            cB, UInt(x.count) as vDSP_Length
//        )
//        var meanX = cB[0]
//        cB[0] = -cB[0]
//        vDSP_vsaddD(
//            UnsafePointer<Double>(cX), 1 as vDSP_Stride,
//            UnsafePointer<Double>(cB),
//            cX, 1 as vDSP_Stride,
//            UInt(x.count) as vDSP_Length
//        )
//        vDSP_svesqD(
//            UnsafePointer<Double>(cX), 1 as vDSP_Stride,
//            cB, UInt(x.count) as vDSP_Length
//        )
//        var ssX = cB[0]
//        println("sumx \(sumX), meanX \(meanX), ssX \(ssX)")
//        
//        
//        vDSP_sveD(
//            UnsafePointer<Double>(cY), 1 as vDSP_Stride,
//            cB, UInt(y.count) as vDSP_Length
//        )
//        var sumY = cB[0]
//        vDSP_meanvD(
//            UnsafePointer<Double>(cY), 1 as vDSP_Stride,
//            cB, UInt(y.count) as vDSP_Length
//        )
//        var meanY = cB[0]
//        cB[0] = -cB[0]
//        vDSP_vsaddD(
//            UnsafePointer<Double>(cY), 1 as vDSP_Stride,
//            UnsafePointer<Double>(cB),
//            cY, 1 as vDSP_Stride,
//            UInt(y.count) as vDSP_Length
//        )
//        vDSP_svesqD(
//            UnsafePointer<Double>(cY), 1 as vDSP_Stride,
//            cB, UInt(y.count) as vDSP_Length
//        )
//        var ssY = cB[0]
//        println("sumY \(sumY), meanY \(meanY), ssY \(ssY)")
//        
//        vDSP_vmulD(
//            UnsafePointer<Double>(cX), 1 as vDSP_Stride,
//            UnsafePointer<Double>(cY), 1 as vDSP_Stride,
//            cX, 1 as vDSP_Stride,
//            UInt(y.count) as vDSP_Length
//        )
//        vDSP_sveD(
//            UnsafePointer<Double>(cX), 1 as vDSP_Stride,
//            cB, UInt(y.count) as vDSP_Length
//        )
//        var sumXY = cB[0]
//        println("sumXY \(sumXY)")
//        
//        var r = sumXY / sqrt(ssX * ssY)
//        println("r \(r)")
//        match = r
//        
//        cX.dealloc(x.count)
//        cY.dealloc(y.count)
//        cB.dealloc(1)
//
//        return match;
//    }
    
    func alignToNearestSameSize(x:[Double], y:[Double], clipEnd:Bool = true) -> ([Double], [Double])
    {
        // align to nearest first value
        //            println("jak to bylo")
        //            for (fi, fd:Double) in enumerate(a) {
        //                if fi < b.count {
        //                    var sd:Double = b[fi]
        //                    println("\(fd) \(sd)")
        //                }
        //            }
        
        var fi = 0;
        var si = 0;
        while (fi < x.count && si < y.count) {
            var diff = x[fi]-y[si]
            if (abs(diff) < 0.2) {
                break;
            } else if (diff > 0) { // first value is bigger
                si++;
            } else { // second value is bigger
                fi++;
            }
        }
        
        //            println("fi \(fi) si \(si)")
        
        var first = x
        var second = y
        if (fi > 0) {
            first.removeRange(0..<fi)
        }
        if (si > 0) {
            second.removeRange(0..<si)
        }
        if clipEnd {
            var minLength = min(first.count, second.count)
            if first.count > minLength {
                first.removeRange(minLength..<first.count)
            }
            if second.count > minLength {
                second.removeRange(minLength..<second.count)
            }
        }
        
        //            println("jak je nyni \(first.count) \(second.count)")
//        for (fi, fd:Double) in enumerate(first) {
//            if fi < second.count {
//                var sd:Double = second[fi]
//                println("\(fd) \(sd)")
//            }
//        }
        
        return (first, second)
    }
    
}
