//
//  BeatDetector.h
//  dance-caller
//
//  Created by Ondrej Macoszek on 9/29/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FloatMatrix.h"
#import "Beat.h"

@class BeatDetector;

/**
 * Methods will be called from background thread
 */
@protocol BeatDetectorWindowDelegate

- (void) beatDetector:(BeatDetector *)beatDetector didReadAudioWave:(float *)wave acceptLength:(NSInteger)acceptLength inWindow:(NSInteger)windowIndex;
- (void) beatDetector:(BeatDetector *)beatDetector didFilterbank:(FloatMatrix *)bankMatrix acceptLength:(NSInteger)acceptLength inWindow:(NSInteger)windowIndex;
- (void) beatDetector:(BeatDetector *)beatDetector didEnvelope:(FloatMatrix *)bankMatrix acceptLength:(NSInteger)acceptLength inWindow:(NSInteger)windowIndex;
- (void) beatDetector:(BeatDetector *)beatDetector didDiffrect:(FloatMatrix *)bankMatrix acceptLength:(NSInteger)acceptLength inWindow:(NSInteger)windowIndex;
- (void) beatDetector:(BeatDetector *)beatDetector debugWave:(float *)wave length:(NSInteger)length;
//- (void) beatDetector:(BeatDetector *)beatDetector didSimplify:(BankMatrix *)bankMatrix inWindow:(NSInteger)windowIndex;
//- (void) beatDetector:(BeatDetector *)beatDetector didEstimateTempo:(float)estimatedTempo inWindow:(NSInteger)windowIndex;
//- (void) beatDetector:(BeatDetector *)beatDetector didFoundBeats:(NSArray *)beats inWindow:(NSInteger)windowIndex;

@end

@interface BeatDetector : NSObject


@property (nonatomic, assign) int debugMaximumReadCount;
@property (nonatomic, assign, readonly) UInt64 totalSamples;
@property (nonatomic, assign, readonly) UInt64 processedSamples;
@property (nonatomic, assign, readonly) double sampleRate;
@property (nonatomic, assign, readonly) NSInteger windowSize;
@property (nonatomic, assign, readonly) double windowSeconds;
@property (nonatomic, assign, readonly) double windowSecondsAcceptRatio;
@property (nonatomic, strong, readonly) NSString *audioFile;
@property (nonatomic, strong, readonly) NSArray *bandLimits;
@property (nonatomic, weak) id<BeatDetectorWindowDelegate> windowDelegate;
@property (nonatomic, copy) void (^progressBlock)(float progressValue, NSString *message);



- (instancetype)initWithAudioFile:(NSString *)audioFile;
- (instancetype)initWithWindowSeconds:(float)windowSeconds audioFile:(NSString *)audioFile;
- (instancetype)initWithWindowSeconds:(float)windowSeconds bandLimits:(NSArray *)bandLimits audioFile:(NSString *)audioFile;

/**
 * Start asynchronously searching for beats
 * All found beats will return in completion.
 */
- (void)findBeatsCompletion:(void (^)(NSArray *allFoundBeats, double averageBPM))completion;

@end
