//
//  AudioVisualizer.h
//  dance-caller
//
//  Created by Ondrej Macoszek on 1/15/15.
//  Copyright (c) 2015 Ondrej Macoszek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class AudioVisualizer;

@protocol AudioVisualizerDelegate <NSObject>

- (void)audioVisualizerDidPrepareImages:(AudioVisualizer *)av;

@end

@interface AudioVisualizer : NSObject

@property (nonatomic, weak) id<AudioVisualizerDelegate> delegate;

- (instancetype)initWithAudioFile:(NSString *)audioFile imageSize:(CGSize)imageSize imageSamplingFrequency:(double)imageSamplingFrequency imageSeconds:(double)imageSeconds;
- (void)startPreparingImages;
- (NSInteger)numberOfImages;
- (UIImage *)imageAtIndex:(NSInteger)index;

@end
