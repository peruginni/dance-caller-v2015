//
//  AnalyzedMusicTableViewCell.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 1/3/15.
//  Copyright (c) 2015 Ondrej Macoszek. All rights reserved.
//

import UIKit

class AnalyzedMusicTableViewCell: UITableViewCell {
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var subnameLabel: UILabel!
    @IBOutlet weak var rhythmLabel: UILabel!
    @IBOutlet weak var actionLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
