//
//  BankMatrix.m
//  dance-caller
//
//  Created by Ondrej Macoszek on 9/29/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

#import "FloatMatrix.h"

@implementation FloatMatrix

- (instancetype)initWithRows:(NSUInteger)rows cells:(NSUInteger)cells
{
    self = [super init];
    if (self) {
        _rows = rows;
        _cells = cells;
        _values = malloc(_rows * sizeof(float *));
        for (int i = 0; i < _rows; i++) {
            _values[i] = calloc(_cells, sizeof(float));
        }
    }
    return self;
}

- (void)clean
{
//    NSLog(@"bankmatrix clean");
    for (int i = 0; i < _rows; i++) {
        free(_values[i]);
    }
    free(_values);
}

- (void)dealloc
{
//    [self clean];
}

@end
