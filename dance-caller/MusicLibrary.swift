//
//  MusicLibrary.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/3/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import UIKit
import CoreData
import PromiseKit
import MediaPlayer
import AVFoundation
import SwiftyJSON

class MusicLibrary: NSObject {
   
    var context:NSManagedObjectContext!
    var rhythms = [
        "reel", "jig", "polka", "hornpipe", "slide"
    ]
    
    init(context:NSManagedObjectContext!)
    {
        self.context = context
    }
    
    func listMusic(byRhythm rhythm: String?) -> Promise<[Music]>
    {
        return Promise<[Music]> { (fulfill, reject) in
            
            var musicList :[Music] = []
            self.context.performBlockAndWait({ () -> Void in
                var request = NSFetchRequest(entityName: "CDMusic")
                var predicates:[NSPredicate] = []
//                if rhythm != nil && rhythm != "all" {
//                    predicates.append(NSPredicate(format: "rhythm == %@", rhythm!)!)
//                }
//                predicates.append(NSPredicate(format: "barsCount > 0")!)
                request.predicate = NSCompoundPredicate.andPredicateWithSubpredicates(predicates)
                var fetchedObjects = self.context.executeFetchRequest(request, error: nil) as! [CDMusic]
                for cdMusic in fetchedObjects {
                    println(cdMusic)
                    musicList.append(Music(cdMusic: cdMusic))
                }
            })
            fulfill(musicList)
            
        }
    }
    
    func dumpAsJson() -> Promise<Void>
    {
        return Promise<Void> { (fulfill, reject) in
            
            var musicList :[Music] = []
            var jsonString = "";
            self.context.performBlockAndWait({ () -> Void in
                var request = NSFetchRequest(entityName: "CDMusic")
                var predicates:[NSPredicate] = []
                request.predicate = NSCompoundPredicate.andPredicateWithSubpredicates(predicates)
                var fetchedObjects:[CDMusic] = self.context.executeFetchRequest(request, error: nil)! as! [CDMusic]
                for cdMusic in fetchedObjects {
                    jsonString += "{\n"
                    jsonString += "\t\"name\":\"\(cdMusic.name)\",\n"
                    jsonString += "\t\"band\":\"\(cdMusic.band)\",\n"
                    jsonString += "\t\"file\":\"\(cdMusic.localPath)\",\n"
                    jsonString += "\t\"rhythm\":\"\(cdMusic.rhythm)\",\n"
                    jsonString += "\t\"beats\":\(cdMusic.structureJSON)\n"
                    jsonString += "},\n"
                    musicList.append(Music(cdMusic: cdMusic, deepCopy: true))
                }
            })
            println(jsonString)
            fulfill()
            
        }
    }
    
    func fetchAllTestMusicAnnotations() -> Promise<[TestMusicAnnotation]>
    {
        return Promise<[TestMusicAnnotation]> { (fulfill, reject) in
            
            // JSON to plain objects
            var annotations:[TestMusicAnnotation] = [];
            
            var jsonFile = NSString(
                contentsOfFile: NSBundle.mainBundle().pathForResource("annotated-music", ofType: "json")!,
                encoding: NSUTF8StringEncoding,
                error: nil
                )!
            let data = jsonFile.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
            let json = JSON(data: data!)
            
            if let jsonTunes = json["tunes"].array {
                
                for jsonTune:JSON in jsonTunes {
                    
                    var annotation = TestMusicAnnotation();
                    annotation.musicId = jsonTune["musicId"].string!
                    annotation.rhythm = jsonTune["rhythm"].string!
                    annotation.name = jsonTune["name"].string!
                    annotation.band = jsonTune["band"].string!
                    annotation.beats = jsonTune["beats"].arrayObject! as! [Double]
                    annotations.append(annotation)
                    
                }
                
            }

            fulfill(annotations)
            
        }
    }
    
    func testAnnotationForMusicId(musicId:String) -> Promise<TestMusicAnnotation>
    {
        return Promise<TestMusicAnnotation> { (fulfill, reject) in
            
            // JSON to plain objects
            var annotation = TestMusicAnnotation();
            
            var jsonFile = NSString(
                contentsOfFile: NSBundle.mainBundle().pathForResource("annotated-music", ofType: "json")!,
                encoding: NSUTF8StringEncoding,
                error: nil
                )!
            let data = jsonFile.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
            let json = JSON(data: data!)
            
            if let jsonTunes = json["tunes"].array {
                
                for jsonTune:JSON in jsonTunes {
                    
                    if let jsonMusicId:String! = jsonTune["musicId"].string {
                        if  jsonMusicId == musicId {
                            annotation.musicId = jsonMusicId!
                            annotation.rhythm = jsonTune["rhythm"].string!
                            annotation.name = jsonTune["name"].string!
                            annotation.band = jsonTune["band"].string!
                            annotation.beats = jsonTune["beats"].arrayObject! as! [Double]
                            break;
                        }
                    }
                    
                }
                
            }

            fulfill(annotation)
            
        }
    }

    func fetchCompleteMusic(music:Music) -> Promise<Music?>
    {
        return Promise<Music?> { (fulfill, reject) in
            
            var foundMusic:Music? = nil
            if music.objectID != nil {
                self.context.performBlockAndWait({ () -> Void in
                    var cdMusic = self.context.objectWithID(music.objectID!) as! CDMusic
                    foundMusic = Music(cdMusic: cdMusic, deepCopy: true);
                })
            }
            fulfill(foundMusic)
            
        }
    }

    
    func computeBars(musicList: [Music]) -> Promise<[Music]>
    {
        return Promise<[Music]> { (fulfill, reject) in
            
            for music in musicList {
                
                var takeEachXBeat:Int = 2
                music.bars = []
                

                for (index, bar) in enumerate(music.beats) {
                    if (index % takeEachXBeat == 0) {
                        music.bars.append(bar)
                    }
                }
                
                // mozna pocitat s rychlosti beatu taky
//                if music.rhythm == "reel" {
//                    
//                } else if music.rhytm == "jig" {
//                    
//                }
                
            }
            
            fulfill(musicList)
            
        }
    }
    
    func saveMusic(musicList: [Music]) -> Promise<[Music]>
    {
        return Promise<[Music]> { (fulfill, reject) in
            
            var saveError:NSError?

            self.context.performBlockAndWait({ () -> Void in
                
                var cdMusicList:[CDMusic] = []
                var musicMap:[String:Music] = [:]
                for music in musicList {
                    
                    musicMap[music.assetPersistentID] = music
                    
                    var cdMusic:CDMusic! = nil
                    if music.objectID == nil {
                        cdMusic = NSEntityDescription.insertNewObjectForEntityForName("CDMusic", inManagedObjectContext: self.context) as! CDMusic
                    } else {
                        cdMusic = self.context.existingObjectWithID(music.objectID!, error: nil) as! CDMusic?
                    }
                    if (cdMusic != nil) {
                        
                        music.copyInto(cdMusic)
                        
                        if music.beats.count > 0 {
                            if cdMusic.beats.count > 0 {
                                for cdMusicPoint in (cdMusic.beats.array as! [CDMusicPoint]) {
                                    self.context.deleteObject(cdMusicPoint)
                                }
                                cdMusic.mutableOrderedSetValueForKey("beats").removeAllObjects()
                            }
                            var cdBeats = cdMusic.mutableOrderedSetValueForKey("beats")
                            for musicPoint in music.beats {
                                var cdMusicPoint:CDMusicPoint = NSEntityDescription.insertNewObjectForEntityForName("CDMusicPoint", inManagedObjectContext: self.context) as! CDMusicPoint
                                cdMusicPoint.time = musicPoint.time
                                cdMusicPoint.note = musicPoint.note
                                cdBeats.addObject(cdMusicPoint)
                            }
                        }
                        
                        if music.bars.count > 0 {
                            if cdMusic.bars.count > 0 {
                                for cdMusicPoint in (cdMusic.bars.array as! [CDMusicPoint]) {
                                    self.context.deleteObject(cdMusicPoint)
                                }
                                cdMusic.mutableOrderedSetValueForKey("bars").removeAllObjects()
                            }
                            var cdBars = cdMusic.mutableOrderedSetValueForKey("bars")
                            for musicPoint in music.bars {
                                var cdMusicPoint:CDMusicPoint = NSEntityDescription.insertNewObjectForEntityForName("CDMusicPoint", inManagedObjectContext: self.context) as! CDMusicPoint
                                cdMusicPoint.time = musicPoint.time
                                cdMusicPoint.note = musicPoint.note
                                cdBars.addObject(cdMusicPoint)
                            }
                            cdMusic.barsCount = Int64(cdBars.count)
                        }
                        
                        cdMusicList.append(cdMusic)
                        
                    }
                    
                }
                
                self.context?.save(&saveError)
                
                for cdMusic in cdMusicList {
                    if musicMap[cdMusic.assetPersistentID] != nil {
                        musicMap[cdMusic.assetPersistentID]?.objectID = cdMusic.objectID
                    }
                }
                

                
            })
             
            
            if (saveError != nil) {
                reject(saveError!)
            } else {
                fulfill(musicList)
            }
            
        }
    }
    
    func detectBeatsAndBars(music:Music, musicPath:String, progressCallback:((Music, Float, String) -> Void)) -> Promise<Music>
    {
        return Promise<Music> { (fulfill, reject) in
            
            println("started promise for \(music.name)")
            
            var beatDetector = BeatDetector(windowSeconds: Float(20.0), audioFile: musicPath)
            beatDetector.progressBlock = {(progress:Float, message:String!) -> Void in
                progressCallback(music, progress, message)
            }
            beatDetector.findBeatsCompletion({ (allBeats:[AnyObject]!, averageBPM:Double) -> Void in
                progressCallback(music, 0, "Found \(allBeats.count) beats")
                music.beats = []
                music.averageBPM = averageBPM
                for beat in (allBeats as! [Beat]!) {
                    var musicPoint = MusicPoint()
                    musicPoint.time = beat.time as Double
                    musicPoint.note = "\(beat.note)"
                    music.beats.append(musicPoint)
                }
                fulfill(music)
            })
            
        }
    }
    
    func importFromMediaItemCollection(mediaItemCollection:MPMediaItemCollection) -> Promise<[Music]>
    {
        return Promise<[Music]> { (fulfill, reject) in
            
            var musicItems:[Music] = []
            for item in (mediaItemCollection.items as! [MPMediaItem]!) {
                var music = Music()
                item.enumerateValuesForProperties(NSSet(array:[
                    MPMediaItemPropertyPersistentID,
                    MPMediaItemPropertyTitle,
                    MPMediaItemPropertyAlbumTitle,
                    MPMediaItemPropertyArtist,
                    MPMediaItemPropertyAssetURL
                ]) as Set<NSObject>, usingBlock: { (propertyName, value, stop) -> Void in
//                    println("\(propertyName) \(value)")
                    switch propertyName {
                        case MPMediaItemPropertyPersistentID:
                            music.assetPersistentID = "\(value)"
                        case MPMediaItemPropertyAssetURL:
                            music.assetPath = "\(value)"
                        case MPMediaItemPropertyTitle:
                            music.name = "\(value)"
                        case MPMediaItemPropertyArtist:
                            music.band = "\(value)"
                        case MPMediaItemPropertyAlbumTitle:
                            music.album = "\(value)"
                        default:
                            break
                    }
                })
                if music.assetPath != "nil" {
                    musicItems.append(music)
                }
            }
            
            var error:NSError?
            self.context.performBlockAndWait({ () -> Void in

                var filteredMusic:[Music] = []
                
                for music in musicItems {
                    
//                    music.localPath = "\(music.assetPersistentID)-\(self.randomStringWithLength(10)).m4a"
                    music.localPath = "\(music.assetPersistentID).m4a"
                    self.guessRhythm(music)
                    
                    var request = NSFetchRequest(entityName: "CDMusic")
                    request.predicate = NSPredicate(format: "assetPersistentID == %@", music.assetPersistentID)
                    var fetchedObjects = self.context.executeFetchRequest(request, error: nil) as! [CDMusic]
                    println("For music \(music.assetPersistentID) exists \(fetchedObjects.count) cdmusics")
                    if fetchedObjects.count > 0 {
                        for cdMusic in fetchedObjects {
//                            println("\(cdMusic.name) \(cdMusic.assetPersistentID)")
//                            if (cdMusic.barsCount == 0) {
                                music.objectID = cdMusic.objectID
                                filteredMusic.append(music)
//                            }
                            break
                        }
                    } else {
                        filteredMusic.append(music)
                    }
                    
                }
                
                musicItems = filteredMusic
                
            })
            
            var baseDirectory = self.musicDirectory()
            if !NSFileManager.defaultManager().fileExistsAtPath(baseDirectory) {
                NSFileManager.defaultManager().createDirectoryAtPath(baseDirectory, withIntermediateDirectories: false, attributes: nil, error: &error)
            }

            if error != nil {
                reject(error!)
            }
            
            var exportPromises:[Promise<Void>] = []
            for music in musicItems {
                
                var assetUrl = NSURL(string: music.assetPath)
//                var staticPath = NSBundle.mainBundle().pathForResource("irish", ofType: "wav")!
//                staticPath = NSBundle.mainBundle().pathForResource("plain1", ofType: "mp3")!
//                staticPath = NSBundle.mainBundle().pathForResource("conn1", ofType: "mp3")!
//                println(staticPath);
//                assetUrl = NSURL(fileURLWithPath: staticPath)
                var songAsset = AVURLAsset(URL: assetUrl!, options: nil)
                var exporter = AVAssetExportSession(asset: songAsset, presetName: AVAssetExportPresetAppleM4A)
                exporter.outputFileType = "com.apple.m4a-audio"
                
                var exportFile = "\(baseDirectory)".stringByAppendingPathComponent(music.localPath)
                NSFileManager.defaultManager().removeItemAtPath(exportFile, error: nil)
                exporter.outputURL = NSURL(fileURLWithPath: exportFile)
                
                var promise = Promise<Void>({ (fulfill, reject) -> Void in
                    exporter.exportAsynchronouslyWithCompletionHandler({ () -> Void in
                        var exportStatus:AVAssetExportSessionStatus = exporter.status;
                        switch exportStatus {
                            case .Completed:
                                fulfill()
                            default:
                                reject(NSError(domain: "Export session error", code: Int(0), userInfo: nil))
                        }
                    })
                })
                
                exportPromises.append(promise)
                
            }

            when(exportPromises).then(body: { Void -> Void in
                fulfill(musicItems)
            })
        }
    }
    
    func musicDirectory() -> String
    {
        var paths = NSSearchPathForDirectoriesInDomains(.DocumentationDirectory, .UserDomainMask, true)
        var documentsDirectory = paths[0] as! String
        return "\(documentsDirectory)/music"
    }
    
    private func randomStringWithLength (len : Int) -> NSString {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyz0123456789"
        
        var randomString : NSMutableString = NSMutableString(capacity: len)
        
        for (var i=0; i < len; i++){
            var length = UInt32 (letters.length)
            var rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.characterAtIndex(Int(rand)))
        }
        
        return randomString
    }
    
    private func guessRhythm(music:Music) {
        
        var guessedRhythm = ""
        
        var band = music.band.lowercaseString
        for rhythm in rhythms {
            if band.rangeOfString(rhythm) != nil {
                guessedRhythm = rhythm
            }
        }
        
        var album = music.album.lowercaseString
        for rhythm in rhythms {
            if album.rangeOfString(rhythm) != nil {
                guessedRhythm = rhythm
            }
        }
        
        var name = music.name.lowercaseString
        for rhythm in rhythms {
            if name.rangeOfString(rhythm) != nil {
                guessedRhythm = rhythm
            }
        }
        
        music.rhythm = guessedRhythm
        
    }
    
    func pathForMusic(music:Music) -> String {
        return self.musicDirectory().stringByAppendingPathComponent(music.localPath)
    }
    
}
