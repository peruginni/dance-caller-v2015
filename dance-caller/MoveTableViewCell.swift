//
//  MoveTableViewCell.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 1/2/15.
//  Copyright (c) 2015 Ondrej Macoszek. All rights reserved.
//

import UIKit

class MoveTableViewCell: UITableViewCell {

    @IBOutlet weak var moveLabel: UILabel!
    @IBOutlet weak var barsLabel: UILabel!
    @IBOutlet weak var sectionView: UIView!
    
}
