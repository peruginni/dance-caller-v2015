//
//  DanceBook.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/3/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import UIKit
import PromiseKit
import CoreData
import SwiftyJSON

class DanceBook: NSObject {
    
    var context:NSManagedObjectContext!
    
    init(context:NSManagedObjectContext!)
    {
        self.context = context
    }

    func countAllDances() -> Promise<Int>
    {
        return Promise<Int> { (fulfill, reject) in

            var numberOfDances :Int = 0
            self.context.performBlockAndWait({ () -> Void in
                numberOfDances = self.context.countForFetchRequest(NSFetchRequest(entityName: "CDDance"), error: nil)
            })
            fulfill(numberOfDances)
        }
    }
    
    func allDances() -> Promise<[Dance]>
    {
        return Promise<[Dance]> { (fulfill, reject) in
    
            var dances :[Dance] = []
            self.context.performBlockAndWait({ () -> Void in
                var fetchedObjects = self.context.executeFetchRequest(NSFetchRequest(entityName: "CDDance"), error: nil) as! [CDDance]
                for cdDance in fetchedObjects {
                    dances.append(Dance(cdDance: cdDance))
                }
                dances.sort({ $0.name < $1.name })
            })
            fulfill(dances)
        }
    }
    
    func importDancesFromJson(jsonFile: String) -> Promise<Void>
    {
        return Promise<Void> { (fulfill, reject) in

            // JSON to plain objects
            let data = jsonFile.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
            let json = JSON(data: data!)
            
            var saveError: NSError?
            self.context.performBlockAndWait({ () -> Void in

                var moves:[String:Move] = [:]
                if let jsonMoves = json["moves"].array {
                    moves = self.readAndMergeMoves(jsonMoves, existingMoves: moves)
                }
                
                if let jsonDances = json["dances"].array {
                    
                    for jsonDance:JSON in jsonDances {
                        
                        let jsonDanceName:String! = jsonDance["name"].string
                        if jsonDanceName == nil {
                           continue
                        }
                        
                        

                        var cdDance:CDDance! = nil
                        
                        var request = NSFetchRequest(entityName: "CDDance")
                        request.predicate = NSPredicate(format: "name == %@", jsonDanceName)
                        var fetchedObjects = self.context.executeFetchRequest(request, error: nil) as! [CDDance]
                        if fetchedObjects.count > 0 {
                            cdDance = fetchedObjects[0]
                            for cdFigure in (cdDance.figures.array as! [CDFigure]) {
                                self.context.deleteObject(cdFigure)
                            }
                            cdDance.mutableOrderedSetValueForKey("figures").removeAllObjects()
                            println("editing existing dance \(jsonDanceName)")
                        } else {
                            cdDance = NSEntityDescription.insertNewObjectForEntityForName("CDDance", inManagedObjectContext: self.context) as! CDDance
                            println("creating new dance \(jsonDanceName)")
                        }
                        
                        cdDance.name = jsonDanceName
                        cdDance.type = jsonDance["type"].stringValue
                        cdDance.summary = jsonDance["summary"].stringValue
                        
                        var danceMoves = moves
                        if let jsonDanceMoves = jsonDance["moves"].array {
                            danceMoves = self.readAndMergeMoves(jsonDanceMoves, existingMoves: danceMoves)
                        }

                        if let jsonDanceFigures = jsonDance["figures"].array {
                            
                            var order:Int64 = 0
                            for jsonFigure:JSON in jsonDanceFigures {
                            
                                order++
                                var cdFigure = NSEntityDescription.insertNewObjectForEntityForName("CDFigure", inManagedObjectContext: self.context) as! CDFigure
                                cdFigure.name = jsonFigure["name"].stringValue
                                cdFigure.rhythm = jsonFigure["rhythm"].stringValue
                                cdFigure.bars = jsonFigure["bars"].int64Value
                                cdFigure.order = order

                                var figureMoves = moves
                                if let jsonFigureMoves = jsonFigure["moves"].array {
                                    figureMoves = self.readAndMergeMoves(jsonFigureMoves, existingMoves: figureMoves)
                                }

                                var lastFlowMove:Move!
                                if let jsonFigureFlow = jsonFigure["flow"].array {
                                    for jsonFigureFlowItem:JSON in jsonFigureFlow {

                                        var move = self.readSingleMove(jsonFigureFlowItem, existingMoves: figureMoves)
                                        if move == nil {
                                            continue
                                        }
                                    
                                        if (lastFlowMove != nil) {
                                            if move!.bars == 0 {
                                                move!.bars = lastFlowMove.bars
                                            }
                                            if move!.who == "" {
                                                move!.who = lastFlowMove.who
                                            }
                                            if move!.section == "" {
                                                move!.section = lastFlowMove.section
                                            }
                                        }
                                        var cdMove = NSEntityDescription.insertNewObjectForEntityForName("CDMove", inManagedObjectContext: self.context) as! CDMove
                                        move!.copyInto(cdMove)
                                        cdFigure.mutableOrderedSetValueForKey("flow").addObject(cdMove)
                                        
                                        lastFlowMove = move
                                        
                                    }
                                }
                                
                                cdDance.mutableOrderedSetValueForKey("figures").addObject(cdFigure)
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
                self.context?.save(&saveError)
                
            })
            
            if (saveError != nil) {
                reject(saveError!)
            } else {
                fulfill()
            }
            
        }
    }
    
    func detailsForDance(dance: Dance) -> Promise<Dance>
    {
        return Promise<Dance> { (fulfill, reject) in
            
            var detailedDance:Dance?
            if dance.objectID != nil {
                self.context.performBlockAndWait({ () -> Void in
                    var cdDance = self.context.existingObjectWithID(dance.objectID!, error: nil) as! CDDance?
                    if cdDance != nil {
                        detailedDance = Dance(cdDance:cdDance!)
                        cdDance?.figures.enumerateObjectsUsingBlock({ (e1, i1, stop1) -> Void in
                            var cdFigure:CDFigure = e1 as! CDFigure
                            var figure = Figure(cdFigure: cdFigure)
                            var lastFlowSection:FlowSection?
                            var lastFlowWho:FlowSection?
                            cdFigure.flow.enumerateObjectsUsingBlock({ (e2, i2, stop2) -> Void in
                                var move = Move(cdMove: e2 as! CDMove)
                                figure.flow.append(move)
                                
                                if lastFlowSection == nil {
                                    lastFlowSection = FlowSection()
                                    lastFlowSection?.name = move.section
                                } else if lastFlowSection!.name != move.section {
                                    figure.flowBySections.append(lastFlowSection!)
                                    lastFlowSection = FlowSection()
                                    lastFlowSection!.name = move.section
                                }
                                lastFlowSection?.flow.append(move)
                                
                                if lastFlowWho == nil {
                                    lastFlowWho = FlowSection()
                                    lastFlowWho?.name = move.who
                                } else if lastFlowWho!.name != move.who {
                                    figure.flowByWho.append(lastFlowWho!)
                                    lastFlowWho = FlowSection()
                                    lastFlowWho!.name = move.who
                                }
                                lastFlowWho?.flow.append(move)
                                
                            })
                            figure.flowBySections.append(lastFlowSection!)
                            figure.flowByWho.append(lastFlowWho!)
                            cdFigure.calls.enumerateObjectsUsingBlock({ (e2, i2, stop2) -> Void in
                                figure.calls.append(Call(cdCall: e2 as! CDCall))
                            })
                            detailedDance?.figures.append(figure)
                        })
                    }
                })
            }
            if detailedDance != nil {
                fulfill(detailedDance!)
            } else {
                reject(NSError())
            }
            
        }
    }
    
    
    func figure(figure: Figure, saveDefaultMusic music: Music) -> Promise<Void>
    {
        return Promise<Void> { (fulfill, reject) in
            
            var saveError: NSError?
            self.context.performBlockAndWait({ () -> Void in
                
                var cdMusic:CDMusic? = self.context.existingObjectWithID(music.objectID!, error: nil) as! CDMusic?
                var cdFigure:CDFigure? = self.context.existingObjectWithID(figure.objectID!, error: nil) as! CDFigure?
                
                if cdFigure != nil && cdFigure != nil {
                    cdFigure?.music = cdMusic
                }
                
                self.context?.save(&saveError)
                
            })
            
            if (saveError != nil) {
                reject(saveError!)
            } else {
                fulfill()
            }
            
        }
    }
    
//    func figure(figure: Figure, saveCalls calls: [Call])
//    {
//        
//    }
    
    private func readAndMergeMoves(jsonMoves:[JSON], existingMoves:[String:Move]) -> [String:Move]
    {
        var moves = existingMoves;
        
        for jsonMove:JSON in jsonMoves {
            let move = self.readSingleMove(jsonMove, existingMoves: moves)
            if move != nil {
                moves[move!.name] = move
            }
        }
    
        return moves
    }
    
    private func readSingleMove(jsonMove:JSON, existingMoves:[String:Move]) -> Move?
    {
        if jsonMove["name"] == nil {
            return nil
        }
        var move = Move()
        move.name = jsonMove["name"].stringValue
        if (existingMoves[move.name] != nil) {
            move = Move(move: existingMoves[move.name]!)
        }
        if (jsonMove["bars"] != nil) {
            move.bars = jsonMove["bars"].int64Value
        }
        if (jsonMove["explanation"] != nil) {
            move.explanation = jsonMove["explanation"].stringValue
        }
        if (jsonMove["hold"] != nil) {
            move.hold = jsonMove["hold"].stringValue
        }
        if (jsonMove["who"] != nil) {
            move.who = jsonMove["who"].stringValue
        }
        if (jsonMove["section"] != nil) {
            move.section = jsonMove["section"].stringValue
        }
        return move
    }
    
    
}
