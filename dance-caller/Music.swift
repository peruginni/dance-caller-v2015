//
//  Music.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/3/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import UIKit
import CoreData

class Music: NSObject {

    var objectID: NSManagedObjectID?
    var band: String = ""
    var assetPersistentID: String = ""
    var assetPath: String = ""
    var localPath: String = ""
    var name: String = ""
    var album: String = ""
    var rhythm: String = ""
    var barsCount: Int64 = 0
    var averageBPM: Double = 0
    var bars: [MusicPoint] = []
    var beats: [MusicPoint] = []
    var structureJSON: String = ""
    var structure: NSDictionary?
    
    override init() {
        
    }
    
    init(name: String)
    {
        self.name = name
    }
    
    init(music: Music)
    {
        objectID = music.objectID
        band = music.band
        album = music.album
        assetPersistentID = music.assetPersistentID
        assetPath = music.assetPath
        localPath = music.localPath
        name = music.name
        rhythm = music.rhythm
        barsCount = music.barsCount
        averageBPM = music.averageBPM
        structureJSON = music.structureJSON
    }
    
    init(cdMusic: CDMusic, deepCopy: Bool = false) {
        objectID = cdMusic.objectID
        band = cdMusic.band
        album = cdMusic.album
        assetPersistentID = cdMusic.assetPersistentID
        assetPath = cdMusic.assetPath
        localPath = cdMusic.localPath
        name = cdMusic.name
        rhythm = cdMusic.rhythm
        barsCount = cdMusic.barsCount
        averageBPM = cdMusic.averageBPM
        
        if (deepCopy) {
            structureJSON = cdMusic.structureJSON
            
            self.bars = []
            for cdPoint in (cdMusic.bars.array as! [CDMusicPoint]) {
                var bar = MusicPoint(cdMusicPoint: cdPoint)
                self.bars.append(bar)
            }
            self.beats = []
            for cdPoint in (cdMusic.beats.array as! [CDMusicPoint]) {
                var beat = MusicPoint(cdMusicPoint: cdPoint)
                self.beats.append(beat)
            }
        }
    }
    
    func copyInto(cdMusic: CDMusic)
    {
        cdMusic.band = band
        cdMusic.album = album
        cdMusic.assetPersistentID = assetPersistentID
        cdMusic.assetPath = assetPath
        cdMusic.localPath = localPath
        cdMusic.name = name
        cdMusic.rhythm = rhythm
        cdMusic.barsCount = barsCount
        cdMusic.averageBPM = averageBPM
        cdMusic.structureJSON = structureJSON
    }
    
}
