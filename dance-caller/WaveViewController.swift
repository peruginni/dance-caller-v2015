////
////  WaveViewController.swift
////  dance-caller
////
////  Created by Ondrej Macoszek on 10/26/14.
////  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
////
//
//import Foundation
//import AVFoundation
//
//enum WaveViewType:Int {
//    case Wave = 0
//    case FilterBank
//    case Envelope
//    case Diffrect
//    case Debug
//}
//
//class WaveViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
//    
//    @IBOutlet weak var testBarButtonItem: UIBarButtonItem!
//    @IBOutlet weak var selectViewBarButtonItem: UIBarButtonItem!
//    @IBOutlet weak var waveView: UICollectionView!
//    @IBOutlet weak var beatsView: UICollectionView!
//    @IBOutlet weak var loadingView: UIView!
//    var audioPath:String?
//    var beatDetector:BeatDetector?
//    var speechSynthesizer:AVSpeechSynthesizer?
//    var audioPlayer:AVPlayer?
//    
//    var currentView:WaveViewType = .Wave
//    var waveImages:[UIImage] = []
//    var waveAbsoluteMax:Float = -1.0
//    var filterBankImages:[UIImage] = []
//    var filterBankAbsoluteMax:Float = -1.0
//    var envelopeImages:[UIImage] = []
//    var envelopeAbsoluteMax:Float = -1.0
//    var diffrectImages:[UIImage] = []
//    var diffrectAbsoluteMax:Float = -1.0
//    var debugImages:[UIImage] = []
//    var debugAbsoluteMax:Float = -1.0
//    var beats:[Double] = []
//    var beatCount = 0
//    var imageHeight = 600;
//    var imageWidth = 1000;
//    
//    override func viewDidAppear(animated: Bool)
//    {
//        self.beatsView.backgroundColor = UIColor.clearColor()
//        (self.waveView.collectionViewLayout as UICollectionViewFlowLayout).itemSize = CGSizeMake(CGFloat(imageWidth), CGFloat(imageHeight));
//        (self.beatsView.collectionViewLayout as UICollectionViewFlowLayout).itemSize = CGSizeMake(CGFloat(imageWidth), CGFloat(imageHeight));
//        
//        self.audioPath = NSBundle.mainBundle().pathForResource("irish", ofType: "wav");
//        self.speechSynthesizer = AVSpeechSynthesizer()
//        self.analyzeMusic()
//        
////        var voices = AVSpeechSynthesisVoice.speechVoices()
////        for voice in voices {
////            println(voice)
////        }
//    }
//    
//    @IBAction func testButtonTapped(sender: AnyObject)
//    {
//        self.say("house around", silent:true)
//        var playerItem = AVPlayerItem(URL: NSBundle.mainBundle().URLForResource("slides", withExtension: "mp3"))
//        self.audioPlayer = AVPlayer(playerItem: playerItem)
////        self.audioPlayer?.addPeriodicTimeObserverForInterval(CMTimeMake(1, 10), queue: dispatch_get_main_queue(), usingBlock: { (time:CMTime) -> Void in
////            var time:Float64 = CMTimeGetSeconds(time)
////            if self.beats.count > 0 {
////                var first = self.beats.first!
////                if abs(first-time) < 0.2 || first < time {
////                    self.say(String(1))
//////                    self.say(String((self.beatCount++ % 8)+1))
////                    self.beats.removeAtIndex(0);
////                }
////            }
////        })
//        
//        var beatsCM:[NSValue] = [];
//        (self.beats as NSArray).enumerateObjectsUsingBlock({ beatTime, index, stop in
//            if (index % 8 == 0) {
//                var time = beatTime as Double;
//                time = time - 0.1;
//                var cm = CMTimeMakeWithSeconds(time, 10);
//                beatsCM.append(NSValue(CMTime: cm));
//            }
//        })
//        
//        self.audioPlayer?.addBoundaryTimeObserverForTimes(beatsCM, queue: dispatch_get_main_queue(), usingBlock: { () -> Void in
//            self.say(String("bar"))
//        });
//        self.audioPlayer?.play()
//    }
//    
//    @IBAction func selectViewButtonTapped(sender: AnyObject)
//    {
//        let alertController = UIAlertController(title: nil, message: "Change View", preferredStyle: .ActionSheet)
//        alertController.addAction(UIAlertAction(title: "Cancel", style: .Cancel){ (action) in
//            // ...
//        })
//        alertController.addAction(UIAlertAction(title: "Debug", style: .Default){ (action) in
//            self.currentView = .Debug
//            self.waveView.reloadData()
//            self.beatsView.reloadData()
//        })
//        alertController.addAction(UIAlertAction(title: "Wave", style: .Default){ (action) in
//            self.currentView = .Wave
//            self.waveView.reloadData()
//            self.beatsView.reloadData()
//        })
//        alertController.addAction(UIAlertAction(title: "Filterbank", style: .Default){ (action) in
//            self.currentView = .FilterBank
//            self.waveView.reloadData()
//            self.beatsView.reloadData()
//        })
//        alertController.addAction(UIAlertAction(title: "Envelope", style: .Default){ (action) in
//            self.currentView = .Envelope
//            self.waveView.reloadData()
//            self.beatsView.reloadData()
//        })
//        alertController.addAction(UIAlertAction(title: "Diffrect", style: .Default){ (action) in
//            self.currentView = .Diffrect
//            self.waveView.reloadData()
//            self.beatsView.reloadData()
//        })
//        let popover = alertController.popoverPresentationController;
//        popover?.sourceView = sender as UIView;
//        popover?.barButtonItem = self.selectViewBarButtonItem;
//        self.presentViewController(alertController, animated: true) { () -> Void in
//            
//        }
//
//    }
//    
//    func say(sentence:String, silent:Bool = false)
//    {
//        var utterance = AVSpeechUtterance(string: sentence)
//        utterance.voice = AVSpeechSynthesisVoice(language: "en-GB")
//        utterance.rate = AVSpeechUtteranceDefaultSpeechRate
//        utterance.pitchMultiplier = 1;
//        if (silent) {
//            utterance.rate = AVSpeechUtteranceMaximumSpeechRate
//            utterance.volume = 0
//        }
//        if speechSynthesizer == nil {
//            self.speechSynthesizer = AVSpeechSynthesizer()
//        }
//        self.speechSynthesizer?.speakUtterance(utterance)
//    }
//    
//    func analyzeMusic()
//    {
//        if (self.beatDetector == nil) {
//            self.beatDetector = BeatDetector(windowSeconds: Float(5.0), audioFile: self.audioPath);
////            self.beatDetector!.windowDelegate = self;
//        }
//        
//        self.loadingView.alpha = 1
//        self.beatDetector?.findBeatsCompletion({ (allBeats:[AnyObject]!, averageBPM:Double) -> Void in
//            self.beats = []
//            var i:Int = 0
//            for num in allBeats {
//                self.beats.append(num.time as Double);
//            }
//            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
//                self.beatsView.reloadData()
//            });
//        })
//    }
//    
//    // MARK: UICollectionViewDelegate
//
//    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
//    {
//        if (collectionView == self.waveView) {
//            switch currentView {
//                case .Wave:
//                    return waveImages.count
//                case .FilterBank:
//                    return filterBankImages.count
//                case .Envelope:
//                    return envelopeImages.count
//                case .Diffrect:
//                    return diffrectImages.count
//                case .Debug:
//                    return debugImages.count
//            }
//        } else if (collectionView == self.beatsView) {
//            return beats.count
//        } else {
//            return 0;
//        }
//    }
//    
//    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
//    {
//        if (collectionView == self.beatsView) {
//            var cell = collectionView.dequeueReusableCellWithReuseIdentifier("beat", forIndexPath: indexPath) as UICollectionViewCell
//            return cell
//        } else {
//            var cell = collectionView.dequeueReusableCellWithReuseIdentifier("image", forIndexPath: indexPath) as UICollectionViewCell
//            var imageView = cell.viewWithTag(1) as UIImageView
//            switch currentView {
//                case .Wave:
//                    imageView.image = waveImages[indexPath.row]
//                case .FilterBank:
//                    imageView.image = filterBankImages[indexPath.row]
//                case .Envelope:
//                    imageView.image = envelopeImages[indexPath.row]
//                case .Diffrect:
//                    imageView.image = diffrectImages[indexPath.row]
//                case .Debug:
//                    imageView.image = debugImages[indexPath.row]
//            }
//            return cell
//        }
//    }
//    
//    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
//    {
//        if (collectionView == self.waveView) {
//            return (collectionViewLayout as UICollectionViewFlowLayout).itemSize;
//        } else if (collectionView == self.beatsView) {
//            var width:Double = 0
//            var winSec:Double = Double(self.beatDetector!.windowSeconds * self.beatDetector!.windowSecondsAcceptRatio);
//            if (self.beatDetector != nil) {
//                if (indexPath.row > 0) {
//                    width = Double(imageWidth) / winSec * (self.beats[indexPath.row]-self.beats[indexPath.row-1])
//                } else {
//                    width = Double(imageWidth) / winSec * self.beats[0]
//                }
//            }
//            return CGSizeMake(CGFloat(width), CGFloat(imageHeight))
//        } else {
//            return CGSizeMake(0, 0)
//        }
//    }
//    
//    func scrollViewDidScroll(scrollView: UIScrollView) {
//        if (scrollView == self.waveView) {
//            self.beatsView.contentOffset = scrollView.contentOffset
//        }
//    }
//        
//    // MARK: BeatDetectorWindowDelegate
//    
////    func beatDetector(beatDetector: BeatDetector!, didReadAudioWave wave: UnsafeMutablePointer<Float>, acceptLength: Int, inWindow windowIndex: Int) {
////        if waveAbsoluteMax < 0 {
////            waveAbsoluteMax = AudioHelper.findAbsoluteMaxFromAudioSamples(wave, length: acceptLength, channels: Int(1))
////        }
////        var image = AudioHelper.createImageFromAudioSamples(wave, length: acceptLength, channels: Int(1), absoluteMax: Float(waveAbsoluteMax), imageWidth: Float(imageWidth), imageHeight: Float(imageHeight))
////        self.waveImages.append(image)
////        NSOperationQueue.mainQueue().addOperationWithBlock { () -> Void in
////            self.loadingView.alpha = 0
////            self.waveView.reloadData()
////            self.beatsView.reloadData()
////        }
////    }
////    
////    func beatDetector(beatDetector: BeatDetector!, didFilterbank bankMatrix: BankMatrix!, acceptLength: Int, inWindow windowIndex: Int) {
//////        if filterBankAbsoluteMax < 0 {
//////            filterBankAbsoluteMax = AudioHelper.findAbsoluteMaxFromAudioSamples(bankMatrix.values[0], length: Int(acceptLength), channels: Int(1))
//////        }
//////        var image = AudioHelper.createImageFromAudioSamples(bankMatrix.values[0], length: Int(acceptLength), channels: Int(1), absoluteMax: Float(filterBankAbsoluteMax), imageWidth: Float(imageWidth), imageHeight: Float(imageHeight))
//////        self.filterBankImages.append(image)
//////        NSOperationQueue.mainQueue().addOperationWithBlock { () -> Void in
//////            self.loadingView.alpha = 0
//////            self.waveView.reloadData()
//////            self.beatsView.reloadData()
//////        }
////    }
////    
////    func beatDetector(beatDetector: BeatDetector!, didEnvelope bankMatrix: BankMatrix!, acceptLength: Int, inWindow windowIndex: Int) {
//////        if envelopeAbsoluteMax < 0 {
//////            envelopeAbsoluteMax = AudioHelper.findAbsoluteMaxFromAudioSamples(bankMatrix.values[0], length: Int(acceptLength), channels: Int(1))
//////        }
//////        var image = AudioHelper.createImageFromAudioSamples(bankMatrix.values[0], length: Int(acceptLength), channels: Int(1), absoluteMax: Float(envelopeAbsoluteMax), imageWidth: Float(imageWidth), imageHeight: Float(imageHeight))
//////        self.envelopeImages.append(image)
//////        NSOperationQueue.mainQueue().addOperationWithBlock { () -> Void in
//////            self.loadingView.alpha = 0
//////            self.waveView.reloadData()
//////            self.beatsView.reloadData()
//////        }
////    }
////    
////    func beatDetector(beatDetector: BeatDetector!, didDiffrect bankMatrix: BankMatrix!, acceptLength: Int, inWindow windowIndex: Int) {
//////        if diffrectAbsoluteMax < 0 {
//////            diffrectAbsoluteMax = AudioHelper.findAbsoluteMaxFromAudioSamples(bankMatrix.values[0], length: Int(acceptLength), channels: Int(1))
//////        }
//////        var image = AudioHelper.createImageFromAudioSamples(bankMatrix.values[0], length: Int(acceptLength), channels: Int(1), absoluteMax: Float(diffrectAbsoluteMax), imageWidth: Float(imageWidth), imageHeight: Float(imageHeight))
//////        self.diffrectImages.append(image)
//////        NSOperationQueue.mainQueue().addOperationWithBlock { () -> Void in
//////            self.loadingView.alpha = 0
//////            self.waveView.reloadData()
//////            self.beatsView.reloadData()
//////        }
////    }
////    
////    func beatDetector(beatDetector: BeatDetector!, debugWave wave: UnsafeMutablePointer<Float>, length: Int) {
//////        if  debugAbsoluteMax < 0 {
//////            debugAbsoluteMax = AudioHelper.findAbsoluteMaxFromAudioSamples(wave, length: length, channels: Int(1))
//////        }
//////        var image = AudioHelper.createImageFromAudioSamples(wave, length: length, channels: Int(1), absoluteMax: Float(debugAbsoluteMax), imageWidth: Float(imageWidth), imageHeight: Float(imageHeight))
////////        self.debugImages = [];
//////        self.debugImages.append(image)
//////        NSOperationQueue.mainQueue().addOperationWithBlock { () -> Void in
//////            self.loadingView.alpha = 0
//////            self.waveView.reloadData()
//////            self.beatsView.reloadData()
//////        }
////    }
//    
//}
