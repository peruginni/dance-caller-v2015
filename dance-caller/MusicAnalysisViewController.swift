//
//  MusicAnalysisViewController.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/1/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import UIKit
import PromiseKit
import AVFoundation

class MusicAnalysisViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var songsTableView: UITableView!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    var audioPlayer:AVPlayer?
    
    var danceBook: DanceBook!
    var musicLibrary: MusicLibrary!
    private var allMusic:[Music] = []
    private var allMusicStatus:[String:String] = [:]
    private var musicToAnalyze:[Music] = []
    private var nextNotProcessedMusicIndex:Int = 0
    private var progressCallback:((processedMusic:Music, progressValue:Float, message:String) -> Void)!
    
    func addMusicForAnalysis(list:[Music])
    {
        if musicToAnalyze.count > 0 {
            println("Cannot add more music until current batch is processed")
            return
        }
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        if danceBook == nil {
            self.danceBook = DanceBook(context: appDelegate.backgroundContext!)
        }
        if musicLibrary == nil {
            self.musicLibrary = MusicLibrary(context: appDelegate.backgroundContext!)
        }
        
        musicToAnalyze = []
        allMusic = []
        allMusicStatus = [:]
        
        for music in list {
            if allMusicStatus[music.assetPersistentID] == nil {
                allMusic.append(music)
                musicToAnalyze.append(music)
                allMusicStatus[music.assetPersistentID] = "Queued for further analysis"
            }
        }

        self.progressCallback = { (processedMusic:Music, progressValue:Float, message:String) -> Void in
            var outMessage = progressValue > 1 ? String(format:"%.f%% %@", progressValue, message) : "\(message)"
            println(outMessage)
            self.allMusicStatus[processedMusic.assetPersistentID] = outMessage
            dispatch_async(dispatch_get_main_queue(),{
                self.songsTableView.reloadData()
            });
        }
        
        if musicToAnalyze.count > 0 {
            
            var music = musicToAnalyze[nextNotProcessedMusicIndex]
            nextNotProcessedMusicIndex++
            var musicPath = self.musicLibrary.musicDirectory().stringByAppendingPathComponent(music.localPath)
            var promise = self.musicLibrary.detectBeatsAndBars(music, musicPath: musicPath, progressCallback:progressCallback)
            chainNextPromiseToCurrentPromise(promise)
            
        }
        
        
    }
    
    func chainNextPromiseToCurrentPromise(prevPromise:Promise<Music>) -> Void {
        println("nextNotProcessedMusicIndex \(self.nextNotProcessedMusicIndex)")
        if self.nextNotProcessedMusicIndex < self.musicToAnalyze.count {
            var nextMusic = self.musicToAnalyze[self.nextNotProcessedMusicIndex]
            self.nextNotProcessedMusicIndex++
            var nextPromise = prevPromise.then(body: { prevMusic -> Promise<Music> in
                var musicPath = self.musicLibrary.musicDirectory().stringByAppendingPathComponent(nextMusic.localPath)
                return self.musicLibrary.detectBeatsAndBars(nextMusic, musicPath: musicPath, progressCallback:self.progressCallback)
            })
            self.chainNextPromiseToCurrentPromise(nextPromise)
        } else {
            prevPromise.then(body: { prevMusic -> Void in
                println("promises cleaned")
                self.musicToAnalyze = []
                self.saveButton.enabled = true
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftItemsSupplementBackButton = false
//        self.navigationItem.leftBarButtonItem = self.cancelButton
        self.navigationItem.leftBarButtonItem = self.saveButton
        self.saveButton.enabled = musicToAnalyze.count == 0

    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allMusic.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("song", forIndexPath: indexPath) as! AnalyzedMusicTableViewCell
        
        var music = allMusic[indexPath.row]
        cell.nameLabel.text = music.name
        cell.subnameLabel.text = "\(music.band) \(music.album)"
        cell.statusLabel.text = allMusicStatus[music.assetPersistentID]
        cell.rhythmLabel.text = music.rhythm
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        var music = allMusic[indexPath.row]

        if self.audioPlayer != nil {
            self.audioPlayer?.pause()
        }
        self.audioPlayer = AVPlayer(playerItem: AVPlayerItem(URL: NSURL(fileURLWithPath: self.musicLibrary.pathForMusic(music))))
        self.audioPlayer?.play()
        
        let alertController = UIAlertController(title: "Listen to playback", message: "and change rhythm", preferredStyle: .ActionSheet)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .Cancel){ (action) in
            self.songsTableView.reloadData()
            self.audioPlayer?.pause()
        })
        alertController.addAction(UIAlertAction(title: "Reel", style: .Default){ (action) in
            music.rhythm = "reel"
            self.songsTableView.reloadData()
            self.audioPlayer?.pause()
        })
        alertController.addAction(UIAlertAction(title: "Jig", style: .Default){ (action) in
            music.rhythm = "jig"
            self.songsTableView.reloadData()
            self.audioPlayer?.pause()
        })
        alertController.addAction(UIAlertAction(title: "Hornpipe", style: .Default){ (action) in
            music.rhythm = "hornpipe"
            self.songsTableView.reloadData()
            self.audioPlayer?.pause()
        })
        alertController.addAction(UIAlertAction(title: "Polka", style: .Default){ (action) in
            music.rhythm = "polka"
            self.songsTableView.reloadData()
            self.audioPlayer?.pause()
        })
        alertController.addAction(UIAlertAction(title: "Slide", style: .Default){ (action) in
            music.rhythm = "slide"
            self.songsTableView.reloadData()
            self.audioPlayer?.pause()
        })
        
        let popover = alertController.popoverPresentationController
        popover?.sourceView = self.view
        self.presentViewController(alertController, animated: true) { () -> Void in
            
        }
   
    }
    
    @IBAction func cancelButtonTapped(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func addButtonTapped(sender: AnyObject) {
        
    }

    @IBAction func saveButtonTapped(sender: AnyObject) {
        self.musicLibrary.computeBars(allMusic).then(body: {musicList -> Promise<[Music]> in
            return self.musicLibrary.saveMusic(musicList)
        }).then(body: { savedMusicList -> Void in
            println("\(self.navigationController)")
            if self.navigationController != nil {
                self.navigationController?.popViewControllerAnimated(true)
            }
        })
    }
        
}
