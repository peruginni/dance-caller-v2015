//
//  SelectSongTableViewCell.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 1/2/15.
//  Copyright (c) 2015 Ondrej Macoszek. All rights reserved.
//

import UIKit

class SelectSongTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var bandLabel: UILabel!
    @IBOutlet weak var barsLabel: UILabel!
    @IBOutlet weak var selectLabel: UILabel!

}
