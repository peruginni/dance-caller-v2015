//
//  Call.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/3/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import UIKit
import CoreData

class Call: NSObject {
    
    var objectID: NSManagedObjectID?
    var sentence: String = ""
    var time: Double = 0
    var moveTime: Double = 0
    var move: Move?
    

    override init() {
        
    }
    
    init(cdCall: CDCall) {
        objectID = cdCall.objectID
        sentence = cdCall.sentence
        time = cdCall.time
    }
    
    init(sentence: String, time: Double, move: Move?) {
        self.sentence = sentence
        self.time = time
        self.move = move
    }

}
