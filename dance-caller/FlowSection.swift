//
//  FlowSection.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/31/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import UIKit
import CoreData

class FlowSection: NSObject {
    
    var name: String = ""
    var flow: [Move] = []
    
}
