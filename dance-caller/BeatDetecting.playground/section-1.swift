import UIKit
import Foundation
import XCPlayground
import Accelerate
import AVFoundation
import CoreAudio
import AudioToolbox
import AVFoundation



//func plotBufferInPlayground<T>(bufferToPlot:UnsafeBufferPointer<T>, title:String) {
//    let count = bufferToPlot.count
//    let maxShow = 44000*2
//    for i in 0..<maxShow {
////        let index = count / maxShow * i
////        println(index)
////        if (index < count) {
//            XCPCaptureValue(title, bufferToPlot[i])
////        }
//    }
//}
//
////XCPSetExecutionShouldContinueIndefinitely(continueIndefinitely: true)
//
//let audioPath:String! = NSBundle.mainBundle().pathForResource("irish", ofType: "wav")
//var audioHelper:AudioHelper = AudioHelper()
//audioHelper.readAudioFile(audioPath, withWindow: 4) { (audioWave:UnsafeMutablePointer<Float> , audioLength:UInt32) -> Void in
//    
//}



//let audioURL:CFURL! = CFURLCreateWithFileSystemPath(
//    kCFAllocatorDefault,
//    audioPath as NSString,
//    CFURLPathStyle.CFURLPOSIXPathStyle,
//    0
//)
//var fileRef:ExtAudioFileRef = nil
//
//var error:OSStatus = ExtAudioFileOpenURL(
//    audioURL,
//    UnsafeMutablePointer<ExtAudioFileRef>(fileRef)
//)
//
//var audioFormat = AudioStreamBasicDescription(
//    mSampleRate: Float64(44100),
//    mFormatID: AudioFormatID(kAudioFormatLinearPCM),
//    mFormatFlags: AudioFormatFlags(kLinearPCMFormatFlagIsFloat),
//    mBytesPerPacket: UInt32(sizeof(Float)),
//    mFramesPerPacket: UInt32(1),
//    mBytesPerFrame: UInt32(sizeof(Float)),
//    mChannelsPerFrame: UInt32(1),
//    mBitsPerChannel: UInt32(sizeof(Float) * 8),
//    mReserved: 0
//)
//
//ExtAudioFileSetProperty(
//    fileRef,
//    ExtAudioFilePropertyID(kExtAudioFileProperty_ClientDataFormat),
//    UInt32(sizeof(AudioStreamBasicDescription)),
//    &audioFormat
//)
//
//let windowSeconds:Double = 4
//let windowSize:UInt32 = UInt32(floor(exp2(log2(audioFormat.mSampleRate * windowSeconds))))
//
//let outputBufferSize:UInt32 = windowSize;
////var outputBuffer = [Float](count: Int(outputBufferSize), repeatedValue: 0)
//var outputBuffer = UnsafeMutablePointer<Float>.alloc(Int(outputBufferSize));
//outputBuffer.initialize(0)
//
//var audioData = AudioBufferList(
//    mNumberBuffers: 1, // mono
//    mBuffers: AudioBuffer(
//        mNumberChannels: audioFormat.mChannelsPerFrame,
//        mDataByteSize: UInt32(outputBufferSize),
//        mData: &outputBuffer
//    )
//)
//
//var readNumberFrames:UInt32 = windowSize
//
//while (readNumberFrames > 0) {
//
//    ExtAudioFileRead(fileRef, &readNumberFrames, &audioData)
//    println(readNumberFrames)
//    if (readNumberFrames > 0) {
//
//        let d = audioData.mBuffers.mData
//
//        let samples = UnsafeBufferPointer<Float>(start: UnsafePointer<Float>(audioData.mBuffers.mData), count: Int(outputBufferSize))
//        
//        let count = samples.count
//
//        println(samples[1])
//        
//////        for i in 0..<count {
//////            println()
//////        }
//////
//////        plotBufferInPlayground(samples, "test")
////
////        
////        
//////        if(readNumberFrames < windowSize) {
//////            
//////        } else {
////
//////        }
////
//    }
// 
//    break
//}





//
//    NSMutableArray *allBeats = @[].mutableCopy;
//    FloatMatrix *waves = nil;
//    UInt32 frameCount = windowSize;
//    Float64 elapsedTime = 0;
//    float *buffer;
//    while (frameCount > 0) {
//        ExtAudioFileRead(fileRef, &frameCount, &audioData);
//        if (frameCount > 0)  {
//            buffer = audioData.mBuffers[0].mData;
//            if (frameCount < windowSize) {
//                buffer = calloc(windowSize, sizeof(float));
//                memcpy(buffer, audioData.mBuffers[0].mData, frameCount);
//                frameCount = windowSize;
//            }
//            waves = [self filterbankSignal:buffer size:frameCount fs:audioFormat.mSampleRate bandLimits:_bandLimits];
//            if (frameCount < windowSize) {
//                free(buffer);
//            }
//
//            [self envelopesForMatrix:waves withWindowSize:0.2];
//            [self diffrectForMatrix:waves];
//
//            NSUInteger pieceSize = 50;
//            Float64 simplifiedFS = floor(sampleRate / pieceSize);
//            waves = [self simplifyMatrix:waves pieceSize:pieceSize];
//
//            NSUInteger tempo = [self determineTempo:waves fs:simplifiedFS bpmMin:60 bpmMax:180 bpmAcc:1];
//            [allBeats addObjectsFromArray:[self determineBeats:waves fs:simplifiedFS tempo:tempo scale:pieceSize offset:elapsedTime]];
//
//            elapsedTime += (Float64)frameCount / sampleRate;
//        }
//    }


//- (FloatMatrix *) filterbankSignal:(float *)signal size:(UInt32)size fs:(UInt32)fs bandLimits:(NSArray *)bandLimits {
//
//    vDSP_Length log2n = log2f(size);
//
//    FFTSetup fftSetup = vDSP_create_fftsetup(log2n, FFT_RADIX2); // todo optimalizovat a vyuzit stejny setup v iteracich
//    NSUInteger halfSize = floor( ((float)size) / 2.0 );
//    NSUInteger bandCount = bandLimits.count - 1;
//
//    // 1) perform FFT
//
//    float Arealp[halfSize];
//    float Aimagp[halfSize];
//    COMPLEX_SPLIT A = { .realp = Arealp, .imagp = Aimagp };
//    vDSP_ctoz((COMPLEX*)signal, 2, &A, 1, halfSize);
//    vDSP_fft_zrip(fftSetup, &A, 1, log2n, FFT_FORWARD);
//
//
//    // 2) cut into bands
//
//    // prepare output array
//    FloatMatrix *output = [[FloatMatrix alloc] initWithRows:bandCount cells:size];
//
//    // determine band bound indexes
//    NSUInteger binsPerOneFrequency = floor(halfSize) / (((float)fs)/2.0);
//    NSUInteger bandIndexes[bandLimits.count];
//    for (int i = 0; i < bandLimits.count; i++) {
//        bandIndexes[i] = [bandLimits[i] longValue] * binsPerOneFrequency;
//    }
//
//    // create waves for each band
//    NSUInteger startIndex, indexesTillEnd;
//    float Brealp[halfSize];
//    float Bimagp[halfSize];
//    COMPLEX_SPLIT B = { .realp = Brealp, .imagp = Bimagp };
//    for (NSUInteger i = 0; i < bandCount; i++) {
//
//        // compute start end indexes
//        startIndex = bandIndexes[i];
//        indexesTillEnd = bandIndexes[i+1]-startIndex;
//        if (startIndex+indexesTillEnd >= halfSize) {
//            indexesTillEnd = halfSize-startIndex;
//        }
//
//        // copy band from fft result
//        vDSP_vclr(B.realp, 1, halfSize);
//        vDSP_vclr(B.imagp, 1, halfSize);
//        memcpy(&(B.realp[startIndex]), &(A.realp[startIndex]), indexesTillEnd*sizeof(float));
//        memcpy(&(B.imagp[startIndex]), &(A.imagp[startIndex]), indexesTillEnd*sizeof(float));
//
//        // compute inverse fft for band into waveform
//        vDSP_fft_zrip(fftSetup, &B, 1, log2n, FFT_INVERSE);
//        float scale = (float)1.0/(2*size);
//        vDSP_vsmul( B.realp, 1, &scale, B.realp, 1, halfSize );
//        vDSP_vsmul( B.imagp, 1, &scale, B.imagp, 1, halfSize );
//        vDSP_ztoc(&B, 1, (COMPLEX *)output.values[i], 2, halfSize);
//
//    }
//    vDSP_destroy_fftsetup(fftSetup);
//
//    return output;
//}
//
//- (void)envelopesForMatrix:(FloatMatrix *)matrix withWindowSize:(float)windowSize {
//
//    // common variables
//    NSUInteger N = matrix.cells;
//    vDSP_Length log2n = log2f(N);
//    FFTSetup fftSetup = vDSP_create_fftsetup(log2n, FFT_RADIX2); // todo optimalizovat a vyuzit stejny setup v iteracich
//    float Arealp[N/2];
//    float Aimagp[N/2];
//    COMPLEX_SPLIT A = { .realp = Arealp, .imagp = Aimagp };
//
//    // create half hann window (steep raise)
//    NSUInteger hannPartN = windowSize*2*4096;
//    float hannPart[hannPartN];
//    vDSP_vclr(hannPart, 1, hannPartN);
//    vDSP_hann_window(hannPart, hannPartN, vDSP_HALF_WINDOW | vDSP_HANN_NORM);
//    vDSP_vrvrs(hannPart, 1, hannPartN);
//    float hannWindow[N];
//    vDSP_vclr(hannWindow, 1, N);
//    memcpy(hannWindow, hannPart, hannPartN * sizeof(float));
//    float Hrealp[N/2];
//    float Himagp[N/2];
//    COMPLEX_SPLIT H = { .realp = Hrealp, .imagp = Himagp };
//    vDSP_ctoz((COMPLEX*)hannWindow, 2, &H, 1, N/2);
//    vDSP_fft_zrip(fftSetup, &H, 1, log2n, FFT_FORWARD);
//
//    for(int r = 0; r < matrix.rows; r++) {
//
//        // rectify waves
//        vDSP_vabs(matrix.values[r], 1, matrix.values[r], 1, matrix.cells);
//
//        // fft
//        vDSP_ctoz((COMPLEX*)matrix.values[r], 2, &A, 1, N/2);
//        vDSP_fft_zrip(fftSetup, &A, 1, log2n, FFT_FORWARD);
//
//        // smooth by multiplying with hann window
//        vDSP_vmul(A.realp, 1, H.realp, 1, A.realp, 1, N/2);
//        vDSP_vmul(A.imagp, 1, H.imagp, 1, A.imagp, 1, N/2);
//
//        // ifft
//        vDSP_fft_zrip(fftSetup, &A, 1, log2n, FFT_INVERSE);
//        float scale = (float)1.0/(2*N);
//        vDSP_vsmul( A.realp, 1, &scale, A.realp, 1, N/2 );
//        vDSP_vsmul( A.imagp, 1, &scale, A.imagp, 1, N/2 );
//        vDSP_ztoc(&A, 1, (COMPLEX *)matrix.values[r], 2, N/2);
//
//    }
//
//    vDSP_destroy_fftsetup(fftSetup);
//
//}
//
//- (void)diffrectForMatrix:(FloatMatrix *)matrix {
//
//    NSUInteger N = matrix.cells;
//    float A[N];
//    vDSP_vclr(A, 1, N);
//    float zero[1] = { 0 };
//
//    for (int r = 0; r < matrix.rows; r++) {
//
//        // diff
//        vDSP_vsub(matrix.values[r], 1, matrix.values[r]+1, 1, A+1, 1, N-1);
//
//        // half-wave rectify
//        vDSP_vthr(A, 1, zero, A, 1, N);
//        memcpy(matrix.values[r], A, N * sizeof(float));
//
//    }
//
//}
//
//- (FloatMatrix *)simplifyMatrix:(FloatMatrix *)matrix pieceSize:(NSUInteger)pieceSize {
//
//    NSUInteger N = floor(matrix.cells / pieceSize);
//    FloatMatrix *simplified = [[FloatMatrix alloc] initWithRows:matrix.rows cells:N];
//
//    for (int c = 0; c < N-1; c++) {
//        for (int r = 0; r < simplified.rows; r++) {
//            vDSP_sve(matrix.values[r]+(c * pieceSize), 1, simplified.values[r]+c, pieceSize);
//            *(simplified.values[r]+c) = *(simplified.values[r]+c) / pieceSize;
//        }
//    }
//
//    return simplified;
//
//}
//
//- (NSUInteger)determineTempo:(FloatMatrix *)matrix fs:(Float64)fs bpmMin:(NSUInteger)bpmMin bpmMax:(NSUInteger)bpmMax bpmAcc:(NSUInteger)bpmAcc {
//
//    // waves to frequencies
//    NSUInteger N = matrix.cells;
//    vDSP_Length log2n = log2f(N);
//    FFTSetup fftSetup = vDSP_create_fftsetup(log2n, FFT_RADIX2);
//    float Arealp[N/2];
//    float Aimagp[N/2];
//    COMPLEX_SPLIT A = { .realp = Arealp, .imagp = Aimagp };
//    FloatMatrix *dft = [[FloatMatrix alloc] initWithRows:matrix.rows cells:matrix.cells];
//    for (int r = 0; r < dft.rows; r++) {
//        vDSP_ctoz((COMPLEX*)matrix.values[r], 2, &A, 1, N/2);
//        vDSP_fft_zrip(fftSetup, &A, 1, log2n, FFT_FORWARD);
//        vDSP_vdist(A.realp, 1, A.imagp, 1, dft.values[r], 1, N/2);
//    }
//
//    // find bpm with highest energy
//    NSUInteger bestBpm = 0;
//    double maxEnergy = 0, energy = 0, secondsPerBeat = 0;
//    float x[1] = { 0 };
//    float comb[N], b[N/2], c[N/2];
//    vDSP_vclr(comb, 1, N);
//    vDSP_vclr(b, 1, N/2);
//    vDSP_vclr(c, 1, N/2);
//    for (NSUInteger bpm = bpmMin; bpm <= bpmMax; bpm = bpm+bpmAcc) {
//
//        secondsPerBeat = 60. / (double)bpm;
//        [self fillComb:comb length:N pulses:3 spacing:floor(fs * secondsPerBeat) peakWidth:10];
//        vDSP_ctoz((COMPLEX*)comb, 2, &A, 1, N/2);
//        vDSP_fft_zrip(fftSetup, &A, 1, log2n, FFT_FORWARD);
//        vDSP_vdist(A.realp, 1, A.imagp, 1, c, 1, N/2);
//
//        energy = 0;
//        for (int r = 0; r < dft.rows; r++) {
//            vDSP_vmul(dft.values[r], 1, c, 1, b, 1, N/2);
//            vDSP_svesq(b, 1, x, N/2);
//            energy = energy + x[0];
//        }
//
//        if (energy > maxEnergy) {
//            maxEnergy = energy;
//            bestBpm = bpm;
//        }
//
//    }
//
//    vDSP_destroy_fftsetup(fftSetup);
//
//    return bestBpm;
//
//}
//
//- (NSArray *)determineBeats:(FloatMatrix *)matrix fs:(NSUInteger)fs tempo:(NSUInteger)tempo scale:(NSUInteger)scale offset:(Float64)offset {
//
//    NSUInteger N = matrix.cells;
//    Float64 originalFS = fs * scale;
//
//    // create comb
//    float comb[N];
//    double secondsPerBeat = 60. / (double)tempo;
//    NSUInteger samplesPerBeat = floor(fs * secondsPerBeat);
//    NSUInteger pulses = floor(N/samplesPerBeat) - 1;
//    [self fillComb:comb length:N pulses:pulses spacing:samplesPerBeat peakWidth:10];
//
//    // move comb along time axis and find best match
//    float shiftedComb[N], c[N], x[1] = { 0 };
//    double energy, maxEnergy = 0;
//    NSUInteger bestShift = 0;
//    for (NSUInteger i = 0; i < samplesPerBeat; i++) {
//        vDSP_vclr(shiftedComb, 1, N);
//        memcpy(shiftedComb+i, comb, N-i);
//        energy = 0;
//        for (int r = 0; r < matrix.rows; r++) {
//            vDSP_vmul(shiftedComb, 1, matrix.values[r], 1, c, 1, N);
//            vDSP_svesq(c, 1, x, N);
//            energy = energy + x[0];
//        }
//        if (energy > maxEnergy) {
//            maxEnergy = energy;
//            bestShift = i;
//        }
//    }
//
//    // locate beat indexes
//    NSMutableArray *beats = @[].mutableCopy;
//
//    long long i = bestShift;
//    while (i > 0) {
//        [beats addObject:@(((Float64)(i*scale))/originalFS + offset)];
//        i = i - samplesPerBeat;
//    }
//    i = bestShift + samplesPerBeat;
//    while (i < N) {
//        [beats addObject:@(((Float64)(i*scale))/originalFS + offset)];
//        i = i + samplesPerBeat;
//    }
//
//    return beats;
//}
//
//- (void)fillComb:(float *)comb length:(NSUInteger)N pulses:(NSUInteger)pulses spacing:(NSUInteger)spacing peakWidth:(NSUInteger)peakWidth {
//
//    vDSP_vclr(comb, 1, N);
//
//    NSUInteger candidateIndex = 0;
//    for (int i = 0; i < pulses; i++) {
//        candidateIndex = i * spacing;
//        if (candidateIndex+peakWidth > N) {
//            break;
//        }
//        vDSP_hamm_window(comb + candidateIndex, peakWidth, 0);
//    }
//
//    // TODO mozna zacit uplne z kraje s polovicnim oknem
//
//}
//



