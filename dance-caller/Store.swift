//
//  Store.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/6/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import UIKit
import CoreData

class Store: NSObject {
    
    var context: NSManagedObjectContext?
    
    init(context: NSManagedObjectContext) {
        
        self.context = context;
    }

    func saveContext() -> Bool {
        var error:NSError?
        self.context?.save(&error)
        if error != nil {
            println("CoreData Error: \(error?.debugDescription)")
        }
        return error != nil
    }
   
}
