//
//  Beat.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/3/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import UIKit
import CoreData

class MusicPoint: NSObject {
    var objectID: NSManagedObjectID?
    var time: Double = 0
    var note: String = ""
    
    override init() {
        
    }
    
    init(cdMusicPoint: CDMusicPoint) {
        objectID = cdMusicPoint.objectID
        time = cdMusicPoint.time
        note = cdMusicPoint.note
    }
}
