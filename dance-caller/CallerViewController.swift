//
//  CallerViewController.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/1/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import UIKit
import AVFoundation

class CallerViewController: UIViewController {

    @IBOutlet weak var playButton: UIBarButtonItem!
    @IBOutlet weak var pauseButton: UIBarButtonItem!
    
    @IBOutlet weak var currentWhoLabel: UILabel!
    @IBOutlet weak var currentStepLabel: UILabel!
    @IBOutlet weak var nextLabel: UILabel!
    @IBOutlet weak var nextStepLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var barsInfoLabel: UILabel!
    
    private var figure:Figure?
    private var dance:Dance?
    var musicLibrary:MusicLibrary?
    var caller:Caller!
    var calls:[Call] = []
    var speechSynthesizer:AVSpeechSynthesizer = AVSpeechSynthesizer()
    var audioPlayer:AVPlayer?
    
    func setupWithFigureAndDance(figure:Figure, dance:Dance)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        if musicLibrary == nil {
            self.musicLibrary = MusicLibrary(context: appDelegate.backgroundContext!)
        }
        if caller == nil {
            self.caller = Caller()
        }
        
        if self.audioPlayer != nil {
            self.audioPlayer?.pause()
        }
        
        self.figure = figure
        self.dance = dance
        
        if self.figure == nil || self.figure?.music == nil {
            return
        }
        
        self.caller.callsForFigureAndDance(figure, dance: dance).then(body: { createdCalls -> Void in
            self.calls = createdCalls.reverse()
            
            for call in self.calls {
                println("\(call.time) call: \(call.sentence)")
            }
          
            self.say("house around", silent:true)
            var musicPath = self.musicLibrary!.pathForMusic(self.figure!.music!)
            var playerItem = AVPlayerItem(URL: NSURL(fileURLWithPath: musicPath))
            self.audioPlayer = AVPlayer(playerItem: playerItem)
            self.audioPlayer?.volume = 0.4
            
            var lastMoveEndTime:Double = 0;
            var prelastMoveEndTime:Double = 0;
            self.barsInfoLabel.text = "";
            
            for (index, bar:MusicPoint) in enumerate(self.figure!.music!.bars) {
                self.audioPlayer?.addBoundaryTimeObserverForTimes([bar.time-0.2], queue: dispatch_get_main_queue(), usingBlock: { () -> Void in
                    self.barsInfoLabel.text = "\(index%8+1)/\(index+1)";
                });
            }
            
            self.audioPlayer?.addPeriodicTimeObserverForInterval(CMTimeMake(1, 10), queue: dispatch_get_main_queue(), usingBlock: { (time:CMTime) -> Void in
                var time:Double = CMTimeGetSeconds(time) as Double
                if self.calls.count > 0 {
                    var call:Call = self.calls.last!
                    
                    // (44-40) / (48-40) * 100 / 100
                    var progress = (time - lastMoveEndTime) / (call.moveTime - lastMoveEndTime) * 100 / 100
                    if time < lastMoveEndTime {
                        progress = ((time - prelastMoveEndTime) / (lastMoveEndTime - prelastMoveEndTime) * 100 / 100)
                    }
                    
                    println("progress \(progress) ... \(call.moveTime), \(lastMoveEndTime), \(time)")
                    self.progressView.setProgress(Float(progress), animated: false)
                    
                    if abs(call.time-time) < 0.2 || call.time < time {
                        self.say(call.sentence)
                        if call.move != nil {
                            self.currentWhoLabel.text = call.move?.who
                            self.currentStepLabel.text = call.move?.name
                        } else {
                            self.currentWhoLabel.text = ""
                            self.currentStepLabel.text = call.sentence
                        }
                        println("cur moveTime \(call.moveTime)")
                        prelastMoveEndTime = lastMoveEndTime
                        lastMoveEndTime = call.moveTime
                        self.calls.removeLast()
                        println("next moveTime \(self.calls.last!.moveTime)")
                        if self.calls.last != nil && self.calls.last!.move != nil {
                            self.nextStepLabel.text = self.calls.last!.move!.name
                        } else {
                            self.nextStepLabel.text = ""
                        }
                    }
                }
            })

            self.say("\(dance.name) - \(figure.orderAsString()) figure.", rate: AVSpeechUtteranceDefaultSpeechRate * 0.2)

            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(3.0 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) { () -> Void in
                println()
                self.audioPlayer?.play()
            }
            

            
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.currentWhoLabel.text = ""
        self.currentStepLabel.text = ""
        self.nextStepLabel.text = ""
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.audioPlayer?.pause()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func pauseButtonTapped(sender: AnyObject) {
        
    }
    
    @IBAction func playButtonTapped(sender: AnyObject) {
        
    }

    
    func say(sentence:String, silent:Bool = false, rate:Float = AVSpeechUtteranceDefaultSpeechRate * 0.3)
    {
        var utterance = AVSpeechUtterance(string: sentence)
//        utterance.voice = AVSpeechSynthesisVoice(language: "en-GB")
        utterance.voice = AVSpeechSynthesisVoice(language: "en-IE")
//        utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
        utterance.rate = rate
        utterance.pitchMultiplier = 1.0
        if (silent) {
            utterance.rate = AVSpeechUtteranceMaximumSpeechRate
            utterance.volume = 0
        }
        self.speechSynthesizer.speakUtterance(utterance)
    }
    

}
