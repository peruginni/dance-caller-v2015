//
//  Move.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/3/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import UIKit
import CoreData

class Move: NSObject {
    
    var objectID: NSManagedObjectID?
    var bars: Int64 = 0
    var hold: String = ""
    var name: String = ""
    var explanation: String = ""
    var who: String = ""
    var section: String = ""
    
    override init() {

    }
    
    init(cdMove: CDMove) {
        objectID = cdMove.objectID
        bars = cdMove.bars
        hold = cdMove.hold
        name = cdMove.name
        explanation = cdMove.explanation
        who = cdMove.who
        section = cdMove.section
    }
    
    init(move: Move) {
        objectID = move.objectID
        bars = move.bars
        hold = move.hold
        name = move.name
        explanation = move.explanation
        who = move.who
        section = move.section
    }
    
    func copyInto(cdMove: CDMove)
    {
        cdMove.bars = bars
        cdMove.hold = hold
        cdMove.name = name
        cdMove.explanation = explanation
        cdMove.who = who
        cdMove.section = section
    }
    
}
