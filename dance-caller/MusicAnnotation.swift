//
//  MusicAnnotation.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 5/6/15.
//  Copyright (c) 2015 Ondrej Macoszek. All rights reserved.
//

import Foundation

class TestMusicAnnotation: NSObject {
    
    var name: String = ""
    var band: String = ""
    var rhythm: String = ""
    var file: String = ""
    var musicId: String = ""
    var beats: [Double] = []
    
    override init() {
    }

}
