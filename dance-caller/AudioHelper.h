//
//  AudioHelper.h
//  dance-caller
//
//  Created by Ondrej Macoszek on 10/20/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FloatMatrix.h"

static void CheckResult(OSStatus result, const char *msg) {
    if (result) {
        printf("%s %i %.4s\n", msg, (int)result, (char *)&result);
        exit(0);
    };
}

@interface AudioHelper : NSObject

+ (void)readAudioFile:(NSString *)audioFile atSampleRate:(double)sampleRate windowSecondsSize:(double)windowSecondsSize
        analysisBlock:(void(^)(float *audioData, NSInteger audioDataSize))analysisBlock;

+ (NSArray *)simplifySamples:(float *)samples length:(NSInteger)length targetSamplingFrequency:(NSUInteger)targetSamplingFrequency;


#pragma mark - todo divne nazvy upravit

+ (UIImage *) createImageFromAudioSamples:(NSArray *) samples
                                 channels:(NSInteger) numberOfChannels
                             maxAmplitude:(float) maxAmplitude
                               imageWidth:(float) imageWidth
                              imageHeight:(float) imageHeight;


+ (float) findAbsoluteMaxFromAudioSamples:(float *) samples
                                   length:(NSInteger) length
                                 channels:(NSInteger) numberOfChannels;

+ (float) findAbsoluteMaxAmplitudeFromAudioSamples:(NSArray *) samples
                                          channels:(NSInteger) numberOfChannels;


@end
