//
//  DanceViewController.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/1/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import UIKit

class DanceViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet weak var walkthroughTableView: UITableView!
    @IBOutlet weak var figureCollectionView: UICollectionView!
    @IBOutlet weak var startButton: UIBarButtonItem!
    var dance :Dance?
    var musicLibrary: MusicLibrary!
    var danceBook: DanceBook!
    var selectedFigure:Figure?
    var sectionColors:[String:UIColor]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.musicLibrary = MusicLibrary(context: appDelegate.backgroundContext!)
        self.danceBook = DanceBook(context: appDelegate.backgroundContext!)
        if (self.dance != nil) {
            self.danceBook.detailsForDance(self.dance!).then(body:{ dance -> Void in
                self.title = dance.name
                self.dance = dance
                if self.dance?.figures.count > 0 {
                    self.selectedFigure = self.dance?.figures[0]
                    self.didSelectFigure(self.selectedFigure!)
                }
            })
        }
    }
    
    override func viewWillAppear(animated: Bool) {

        if self.selectedFigure != nil {
            self.didSelectFigure(self.selectedFigure!)
        }
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if self.selectedFigure != nil {
            return self.selectedFigure!.flowByWho.count + 1
        }
        return 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.selectedFigure != nil {
            if section == 0 {
                return 1
            } else {
                return self.selectedFigure!.flowByWho[section-1].flow.count + 1
            }
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if self.selectedFigure != nil {
            if indexPath.section == 0 {
                var cell = tableView.dequeueReusableCellWithIdentifier("song", forIndexPath: indexPath) as! UITableViewCell
                if  self.selectedFigure?.music == nil {
                    cell.textLabel?.text = "Select music"
                    cell.detailTextLabel?.text = "\(self.selectedFigure!.bars) bars of \(self.selectedFigure!.rhythm)"
                } else {
                    cell.textLabel?.text = "Music selected"
                    cell.detailTextLabel?.text = "\(self.selectedFigure!.bars) bars of \(self.selectedFigure!.rhythm)"
                }
                return cell
            } else {
                var flowSection:FlowSection = self.selectedFigure!.flowByWho[indexPath.section-1]
                if indexPath.row == 0 {
                    var cell = tableView.dequeueReusableCellWithIdentifier("who", forIndexPath: indexPath) as! UITableViewCell
                    cell.textLabel?.text = flowSection.name
                    return cell
                } else {
                    var cell = tableView.dequeueReusableCellWithIdentifier("step", forIndexPath: indexPath) as! MoveTableViewCell
                    var move = flowSection.flow[indexPath.row - 1]
                    cell.moveLabel.text = move.name
                    cell.barsLabel.text = "\(move.bars) bars"
                    if self.sectionColors != nil {
                        cell.sectionView.backgroundColor = self.sectionColors![move.section]
                    }
                    return cell
                }
            }
        }
        var cell = tableView.dequeueReusableCellWithIdentifier("who", forIndexPath: indexPath) as! UITableViewCell
        cell.textLabel?.text = ""
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if indexPath.section == 0 {
            self.performSegueWithIdentifier("pickMusic", sender: self.selectedFigure)
        }
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.dance?.figures.count > 0 {
            return self.dance!.figures.count
        }
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var cell = collectionView.dequeueReusableCellWithReuseIdentifier("figure", forIndexPath: indexPath) as! DanceFigureCollectionViewCell
        if indexPath.row < self.dance?.figures.count {
            var figure = self.dance!.figures[indexPath.row]
            switch figure.order {
                case 1:
                    cell.labelView.text = "\(figure.order)st figure"
                case 2:
                    cell.labelView.text = "\(figure.order)nd figure"
                case 3:
                    cell.labelView.text = "\(figure.order)rd figure"
                default:
                    cell.labelView.text = "\(figure.order)th figure"
            }
            cell.wrapperView.layer.cornerRadius = 20
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            cell.wrapperView.backgroundColor = appDelegate.themeLightGreenColor
            if (self.selectedFigure == figure) {
                cell.wrapperView.backgroundColor = appDelegate.themeGreenColor
            }
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row < self.dance?.figures.count {
            var figure = self.dance!.figures[indexPath.row]
            self.selectedFigure = figure
            self.didSelectFigure(figure)
        }
    }
    
    @IBAction func startButtonTapped(sender: AnyObject) {
        if self.selectedFigure?.music != nil {
            self.musicLibrary.fetchCompleteMusic(self.selectedFigure!.music!).then(body: { fetchedMusic -> Void in
                if (fetchedMusic != nil) {
                    self.selectedFigure!.music = fetchedMusic
                    self.performSegueWithIdentifier("caller", sender: self.selectedFigure)
                }
            })
        } else {
            let alertController = UIAlertController(title: nil, message: "To continue, select some music", preferredStyle: .ActionSheet)
            alertController.addAction(UIAlertAction(title: "Cancel", style: .Cancel){ (action) in
                // ...
            })
            alertController.addAction(UIAlertAction(title: "Select Music", style: .Default){ (action) in
                self.performSegueWithIdentifier("pickMusic", sender: self.selectedFigure)
            })
            let popover = alertController.popoverPresentationController;
            popover?.sourceView = self.view
            self.presentViewController(alertController, animated: true) { () -> Void in
                
            }
        }
    }
    
    private func didSelectFigure(figure: Figure) {

        var availableColors = [
            UIColor.flatMagentaColor(),
            UIColor.flatSkyBlueColor(),
            UIColor.flatBrownColor(),
            UIColor.flatMintColor(),
            UIColor.flatSandColor(),
            UIColor.flatTealColor(),
            UIColor.flatRedColor(),
            UIColor.flatBlueColor(),
            UIColor.flatPinkColor(),
            UIColor.flatGreenColor(),
            UIColor.flatYellowColor(),
        ];
        
        self.sectionColors = [:]
        for flowSection in figure.flowBySections {
            if availableColors.count > 0 {
                if self.sectionColors![flowSection.name] == nil {
                    self.sectionColors![flowSection.name] = availableColors.removeLast()
                }
            } else {
                self.sectionColors![flowSection.name] = UIColor.randomFlatColor()
            }
        }
        
        self.walkthroughTableView.reloadData()
        self.figureCollectionView.reloadData()
        self.figureCollectionView.selectItemAtIndexPath(NSIndexPath(forRow: Int(figure.order-1), inSection: Int(0)), animated: true, scrollPosition: UICollectionViewScrollPosition.CenteredHorizontally)

    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "pickMusic") {
            var vc = segue.destinationViewController as! MusicPickerViewController
            vc.figure = self.selectedFigure
        } else if (segue.identifier == "caller") {
            var vc = segue.destinationViewController as! CallerViewController
            vc.setupWithFigureAndDance(self.selectedFigure!, dance: self.dance!)
        }
    }

}
