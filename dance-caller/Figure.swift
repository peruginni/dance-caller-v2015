//
//  Figure.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/3/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import UIKit
import CoreData

class Figure: NSObject {
    
    var objectID: NSManagedObjectID?
    var name: String = ""
    var order: Int64 = 0
    var bars: Int64 = 0
    var rhythm: String = ""
    var music: Music?
    var musicSpeed: Double = 0
    var flow: [Move] = []
    var flowBySections: [FlowSection] = []
    var flowByWho: [FlowSection] = []
    var calls: [Call] = []
    
    override init() {
    }
    
    init(cdFigure: CDFigure) {
        objectID = cdFigure.objectID
        name = cdFigure.name
        order = cdFigure.order
        bars = cdFigure.bars
        rhythm = cdFigure.rhythm
        musicSpeed = cdFigure.musicSpeed
        if cdFigure.music != nil {
            music = Music(cdMusic: cdFigure.music!)
        }
    }
    
    func orderAsString() -> String {
        switch order {
            case 1:
                return "first"
            case 2:
                return "second"
            case 3:
                return "third"
            default:
                return "\(order)th"
        }
    }
    

}
