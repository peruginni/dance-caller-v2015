//
//  BeatDetector.m
//  dance-caller
//
//  Created by Ondrej Macoszek on 9/29/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

#import "BeatDetector.h"
#import <CoreAudio/CoreAudioTypes.h>
#import <AudioToolbox/AudioToolbox.h>
#import <Accelerate/Accelerate.h>
#import <Foundation/Foundation.h>
#import "FloatMatrix.h"
#import "AudioHelper.h"

@implementation BeatDetector {
    NSOperationQueue *_queue;
}

- (id)initWithAudioFile:(NSString *)audioFile
{
    return [self initWithWindowSeconds:4.0 bandLimits:@[@0, @200, @400] audioFile:audioFile];
}

- (instancetype)initWithWindowSeconds:(float)windowSeconds audioFile:(NSString *)audioFile
{
    return [self initWithWindowSeconds:windowSeconds bandLimits:@[@0, @50, @100, @150, @170, @200] audioFile:audioFile];
}

- (instancetype)initWithWindowSeconds:(float)windowSeconds bandLimits:(NSArray *)bandLimits audioFile:(NSString *)audioFile
{
    self = [super init];
    if (self) {
        _sampleRate = 44100.0;
        _windowSize = exp2(floor(log2f(floor(_sampleRate * windowSeconds)))); // create appropriate size for fft work
        _windowSeconds = (float)_windowSize/(float)(_sampleRate); // update window seconds
        _windowSecondsAcceptRatio = 0.7;
        
        _audioFile = audioFile;
        _bandLimits = bandLimits;
        _queue = [NSOperationQueue new];
        _queue.maxConcurrentOperationCount = 1;
    }
    return self;
}

- (void)findBeatsCompletion:(void (^)(NSArray *allFoundBeats, double averageBPM))completion
{
    // TODO: return, if queue is not empty
    
    [_queue addOperationWithBlock:^{

        __block NSMutableArray *tempos = @[].mutableCopy;
        __block double temposSum = 0;
        __block NSMutableArray *allBeats = @[].mutableCopy;
        __block NSInteger windowIndex = 0;
        __block NSInteger minBpm = 80; // 60
        __block NSInteger maxBpm = 160; // 210
        __block double elapsedTime = 0;
        __block double averageBPM = temposSum / (double)tempos.count;
        __block NSArray *lastBeats = nil;
        __block NSUInteger expectedShift = 0;


        [self readAudioFileWithAcceptRatio:_windowSecondsAcceptRatio analysisBlock:^(float *analysisBuffer, NSInteger analysisBufferSize, NSInteger analysisBufferAcceptSize, NSInteger samplingFrequency) {
            
            if (self.progressBlock != nil && windowIndex % 1 == 0) {
                _progressBlock((double)100.0 / (double)_totalSamples * (double)_processedSamples, @"Detecting beats");
            }
            
//            NSLog(@"window %d", windowIndex);
            
            [_windowDelegate beatDetector:self didReadAudioWave:analysisBuffer acceptLength:analysisBufferAcceptSize inWindow:windowIndex];
        
            // filterbank
            FloatMatrix *bankMatrix = [self filterbankSignal:analysisBuffer length:analysisBufferSize samplingFrequency:samplingFrequency bandLimits:_bandLimits];
            [_windowDelegate beatDetector:self didFilterbank:bankMatrix acceptLength:analysisBufferAcceptSize inWindow:windowIndex];

            // envelope+diffrect
            [self envelopesForMatrix:bankMatrix withHannWindowSize:0.2];
            [_windowDelegate beatDetector:self didEnvelope:bankMatrix acceptLength:analysisBufferAcceptSize inWindow:windowIndex];
            [self diffrectForMatrix:bankMatrix];
            [_windowDelegate beatDetector:self didDiffrect:bankMatrix acceptLength:analysisBufferAcceptSize inWindow:windowIndex];

            // determine tempo
            NSInteger pieceSize = 50;
            Float64 simplifiedFS = floor(samplingFrequency / pieceSize);
            FloatMatrix *simpleBankMatrix = [self simplifyMatrix:bankMatrix pieceSize:pieceSize];
            NSInteger tempo = [self determineTempo:simpleBankMatrix samplingFrequency:simplifiedFS bpmMin:minBpm bpmMax:maxBpm bpmStep:1];
            if (!tempo) {
                return;
            }
            [tempos addObject:@(tempo)];
            temposSum += tempo;
//            NSLog(@"avg tempo %f, min %d, max %d", temposSum / (double)tempos.count, minBpm, maxBpm);
            if (windowIndex > 4) {
                minBpm = floor((temposSum / (double)tempos.count) - 5);
                maxBpm = floor((temposSum / (double)tempos.count) + 5);
            }

            // find beats
            NSUInteger foundShift = [self determineShiftForMatrix:simpleBankMatrix samplingFrequency:simplifiedFS tempo:tempo];
//            NSUInteger foundShift = [self determineShiftForMatrix:simpleBankMatrix samplingFrequency:simplifiedFS tempo:tempo expectedShift:expectedShift];
            NSArray *deducedBeats = [self deduceBeatsFromShift:foundShift samples:simpleBankMatrix.cells samplingFrequency:simplifiedFS tempo:tempo];
            
            deducedBeats = [deducedBeats sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"time" ascending:YES]]];
            lastBeats = deducedBeats;
            double acceptTimeLimit = _windowSeconds * _windowSecondsAcceptRatio;
            [deducedBeats enumerateObjectsUsingBlock:^(Beat *beat, NSUInteger idx, BOOL *stop){
                if (beat.time.doubleValue >= acceptTimeLimit) {
                    *stop = true;
                    expectedShift = floor((beat.time.doubleValue - acceptTimeLimit) * simplifiedFS);
                } else {
                    beat.time = @(beat.time.doubleValue + elapsedTime);
                    beat.note = [NSString stringWithFormat:@"%ld", (long)windowIndex];
                    [allBeats addObject:beat];
                }
            }];
            elapsedTime += acceptTimeLimit;
            
            windowIndex++;
            
            [bankMatrix clean];
            [simpleBankMatrix clean];
            
        }];
        
//        NSLog(@"Beats ");
//        [allBeats enumerateObjectsUsingBlock:^(Beat *s, NSUInteger idx, BOOL *stop) {
//            NSLog(@"time %f ", s.time.doubleValue);
//        }];
        
        if (self.progressBlock != nil) {
            _progressBlock(0, @"Cleaning beats");
        }
        
        allBeats = [self cleanDuplicateBeats:allBeats delta:0.5].mutableCopy;
        allBeats = [allBeats sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"time" ascending:YES]]].mutableCopy;

//        NSLog(@"Beats ");
//        [allBeats enumerateObjectsUsingBlock:^(Beat *s, NSUInteger idx, BOOL *stop) {
//            NSLog(@"time %f ", s.time.doubleValue);
//        }];
//        
        if (allBeats.count > 1) {
            elapsedTime = 0;
            windowIndex = 0;
            __block float *helperBuffer = nil;
            __block NSUInteger helperBufferSize = 0;
            __block NSUInteger helperBufferFilledLength = 0;
            __block NSUInteger currentBeatIndex = 0;
            
            if (self.progressBlock != nil) {
                _progressBlock(0, @"Computing beat loudness...");
            }
            
            __block void(^processHelperBuffer)(void) = ^void(void) {
                
                Beat *beat = allBeats[currentBeatIndex];
                float x[1] = { 0 };
                vDSP_svemg(helperBuffer, 1, x, helperBufferFilledLength);
                beat.sum = @(x[0]);
                
                // clear helperBuffer
                vDSP_vclr(helperBuffer, 1, helperBufferSize);
                helperBufferFilledLength = 0;
                
                // read next beat
                currentBeatIndex++;
                
            };
            
            [self readAudioFileWithAcceptRatio:1 analysisBlock:^(float *analysisBuffer, NSInteger analysisBufferSize, NSInteger analysisBufferAcceptSize, NSInteger samplingFrequency) {
                
                if (!helperBuffer) {
                    helperBufferSize = 3*analysisBufferSize;
                    helperBuffer = calloc(helperBufferSize, sizeof(float));
                    vDSP_vclr(helperBuffer, 1, helperBufferSize);
                }


                while (currentBeatIndex < allBeats.count) {
                    double startBeatTime = ((Beat *)allBeats[currentBeatIndex]).time.doubleValue;
                    if (currentBeatIndex+1 < allBeats.count) {
                        double endBeatTime = ((Beat *)allBeats[currentBeatIndex+1]).time.doubleValue;
                        if (startBeatTime >= elapsedTime + _windowSeconds) {
                            break; // read next data block
                        } else if ((startBeatTime < elapsedTime + _windowSeconds)
                                && (endBeatTime < elapsedTime + _windowSeconds)) {
                            NSUInteger start = 0;
                            NSUInteger end = 0;
                            if (startBeatTime < elapsedTime) {
                                end = abs(ceil((endBeatTime - elapsedTime) * samplingFrequency));
                                cblas_scopy(end, analysisBuffer, 1, &helperBuffer[helperBufferFilledLength], 1);
                                helperBufferFilledLength += end;
                            } else {
                                // TODO start was bigger than end
                                start = abs(ceil((startBeatTime - elapsedTime) * samplingFrequency));
                                end = abs(ceil((endBeatTime - elapsedTime) * samplingFrequency));
                                helperBufferFilledLength = end - start;
                                cblas_scopy(helperBufferFilledLength, &analysisBuffer[start], 1, helperBuffer, 1);
                            }
                            
                            processHelperBuffer();
                            continue;
                        } else if (endBeatTime >= elapsedTime + _windowSeconds) {
                            NSUInteger start = 0;
                            if (helperBufferFilledLength == 0) {
                                start = abs(ceil((startBeatTime - elapsedTime) * samplingFrequency));
                            }
                            NSUInteger length = analysisBufferSize - start;
                            if (helperBufferFilledLength + length + 1 < helperBufferSize) {
                                cblas_scopy(length, &analysisBuffer[start], 1, &helperBuffer[helperBufferFilledLength], 1);
                            } else {
                                processHelperBuffer();
                            }

                            break; // read next data block
                        } else {
                            NSLog(@"CHECK>>> should not happen");
                        }
                    } else if (startBeatTime < elapsedTime + _windowSeconds) {
                        NSUInteger start = abs(ceil((startBeatTime - elapsedTime) * samplingFrequency));
                        helperBufferFilledLength = analysisBufferSize - start;
                        cblas_scopy(helperBufferFilledLength, &analysisBuffer[start], 1, helperBuffer, 1);

                        processHelperBuffer();
                        
                        break;
                    }
                }
                
                elapsedTime += _windowSeconds;

                
            }];
            free(helperBuffer);
            
//            [allBeats enumerateObjectsUsingBlock:^(Beat *s, NSUInteger idx, BOOL *stop) {
//                NSLog(@"time %f sum %f ", s.time.doubleValue, s.sum.doubleValue);
//            }];
            
            if (self.progressBlock != nil) {
                _progressBlock(0, @"Cleaning silent beats...");
            }
            
            allBeats = [self cleanSilentBeats:allBeats withStartDelta:0.80 endDelta:0.1];

            averageBPM = temposSum / (double)tempos.count;
            
            NSLog(@"averageBPM %f", averageBPM);
            
//            [allBeats enumerateObjectsUsingBlock:^(Beat *s, NSUInteger idx, BOOL *stop) {
//                NSLog(@"time %f sum %f ", s.time.doubleValue, s.sum.doubleValue);
//            }];
        }
        
        if (self.progressBlock != nil) {
            _progressBlock(0, @"Detecting finished");
        }
        
        if (completion) {
            completion([NSArray arrayWithArray:allBeats], averageBPM);
        }
    }];
    
}

#pragma mark - Helpers

- (void)readAudioFileWithAcceptRatio:(double)acceptRatio
        analysisBlock:(void(^)(float *analysisBuffer, NSInteger analysisBufferSize, NSInteger analysisBufferAcceptSize, NSInteger samplingFrequency))analysisBlock
{
    ExtAudioFileRef fileRef;
    CheckResult(ExtAudioFileOpenURL(CFURLCreateWithFileSystemPath(kCFAllocatorDefault, (__bridge CFStringRef)self.audioFile, kCFURLPOSIXPathStyle, false), &fileRef), "ExtAudioFileOpenURL failed");
    
    int numberOfChannels = 1; // mono;
    
    AudioStreamBasicDescription audioFormat;
    audioFormat.mSampleRate = _sampleRate; // 44100.0;
    audioFormat.mFormatID = kAudioFormatLinearPCM;
    audioFormat.mFormatFlags = kLinearPCMFormatFlagIsFloat;
    audioFormat.mBitsPerChannel = sizeof(float) * 8;
    audioFormat.mChannelsPerFrame = numberOfChannels;
    audioFormat.mBytesPerFrame = audioFormat.mChannelsPerFrame * sizeof(float);
    audioFormat.mFramesPerPacket = 1;
    audioFormat.mBytesPerPacket = audioFormat.mFramesPerPacket * audioFormat.mBytesPerFrame;
    CheckResult(ExtAudioFileSetProperty(fileRef, kExtAudioFileProperty_ClientDataFormat, sizeof (AudioStreamBasicDescription), &audioFormat),"ExtAudioFileSetProperty failed");
    
    UInt32 propertySize = sizeof(_totalSamples);
    CheckResult(ExtAudioFileGetProperty(fileRef, kExtAudioFileProperty_FileLengthFrames, &propertySize, &_totalSamples), "ExtAudioFileGetProperty failed");
    NSLog(@"ExtAudioFileGetProperty total samples %llu", (unsigned long long)_totalSamples);
    
    UInt32 outputBufferSize = _windowSize * audioFormat.mBytesPerPacket;
    float *outputBuffer = (float *)malloc(sizeof(float) * outputBufferSize);
    
    AudioBufferList audioData;
    audioData.mNumberBuffers = numberOfChannels;
    audioData.mBuffers[0].mNumberChannels = audioFormat.mChannelsPerFrame;
    audioData.mBuffers[0].mDataByteSize = outputBufferSize;
    audioData.mBuffers[0].mData = outputBuffer;
    
    UInt32 audioWaveBufferSize = _windowSize*2; // perhaps, could be even little bit smaller (depends on window accept ratio)
    UInt32 audioWaveBufferFirstAvailableIndex = 0;
    float *audioWaveBuffer = calloc(audioWaveBufferSize, sizeof(float));
    vDSP_vclr(audioWaveBuffer, 1, audioWaveBufferSize);
    UInt32 helperBufferSize = audioWaveBufferSize;
    float *helperBuffer = calloc(helperBufferSize, sizeof(float));
    vDSP_vclr(helperBuffer, 1, helperBufferSize);

    UInt32 analysisBufferSize = _windowSize;
    UInt32 analysisBufferAcceptSize = floor((float)_windowSize * acceptRatio);
    float *analysisBuffer = calloc(analysisBufferSize, sizeof(float));

    UInt32 readLength = _windowSize;
    int readCount = 0;
    while (readCount >= 0) {
        
        if (audioWaveBufferFirstAvailableIndex < analysisBufferSize) {
            // fill audio wave buffer with some new data (for further analysis)
            
            readCount++;
            if (self.debugMaximumReadCount > 0) {
                if (readCount > self.debugMaximumReadCount) {
                    break;
                }
            }
            
            ExtAudioFileRead(fileRef, &readLength, &audioData);
            if (!readLength) {
                // no more data to read
                break;
            } else if (readLength < analysisBufferSize) {
                // if we read last part of file which is smaller thank window, so fill remaining part with zeros
                float *zeros = calloc(analysisBufferSize, sizeof(float));
                cblas_scopy(analysisBufferSize, zeros, 1, &(audioData.mBuffers[0].mData[readLength]), 1);
                free(zeros);
            }
            
            if (audioWaveBufferFirstAvailableIndex + analysisBufferSize > audioWaveBufferSize) {
                NSLog(@"CHECK THIS>>> read more data than is able to store to audioWaveBuffer");
            }
            
            // copy read data to audio wave buffer
            cblas_scopy(analysisBufferSize, audioData.mBuffers[0].mData, 1, &audioWaveBuffer[audioWaveBufferFirstAvailableIndex], 1);
            audioWaveBufferFirstAvailableIndex += analysisBufferSize;
            
        }
        
        // copy data for analysis
        cblas_scopy(analysisBufferSize, audioWaveBuffer, 1, analysisBuffer, 1);
        analysisBlock(analysisBuffer, analysisBufferSize, analysisBufferAcceptSize, audioFormat.mSampleRate);
        
        _processedSamples += analysisBufferAcceptSize;
        
//        [_windowDelegate beatDetector:self debugWave:audioWaveBuffer length:audioWaveBufferSize];
        
        // shorten audio wave of what was analysed up to the analysisBufferAcceptSize
        UInt32 shorterAudioBufferSize = audioWaveBufferFirstAvailableIndex - analysisBufferAcceptSize;
        cblas_scopy(shorterAudioBufferSize, &audioWaveBuffer[analysisBufferAcceptSize], 1, helperBuffer, 1);
        vDSP_vclr(audioWaveBuffer, 1, audioWaveBufferSize);
        cblas_scopy(shorterAudioBufferSize, helperBuffer, 1, audioWaveBuffer, 1);
        audioWaveBufferFirstAvailableIndex -= analysisBufferAcceptSize;
        
//        [_windowDelegate beatDetector:self debugWave:audioWaveBuffer length:audioWaveBufferSize];
        
    }
    
    free(audioWaveBuffer);
    free(helperBuffer);
    free(analysisBuffer);
    free(outputBuffer);
}


- (FloatMatrix *) filterbankSignal:(float *)signal
                           length:(NSInteger)length
                samplingFrequency:(NSInteger)samplingFrequency
                       bandLimits:(NSArray *)bandLimits
{
    
    vDSP_Length log2n = log2f(length);
    
    FFTSetup fftSetup = vDSP_create_fftsetup(log2n, FFT_RADIX2); // todo optimalizovat a vyuzit stejny setup v iteracich
    NSUInteger halfSize = floor( ((float)length) / 2.0 );
    NSUInteger bandCount = bandLimits.count - 1;
    
    // 1) perform FFT
    
    COMPLEX_SPLIT A;
    A.realp = (float *) malloc(halfSize * sizeof(float)); // kdyz tam neni malloc ale staticka alokace tak to hazi badexecexception, asi tam nejsou nuly ale nan cizi adresy....
    A.imagp = (float *) malloc(halfSize * sizeof(float));
    //    vDSP_ctoz(signalComplex, 2, &A, 1, halfSize);
    vDSP_ctoz((COMPLEX *)signal, 2, &A, 1, halfSize);
    vDSP_fft_zrip(fftSetup, &A, 1, log2n, FFT_FORWARD);
    
    // 2) cut into bands
    
    // prepare output array
    FloatMatrix *output = [[FloatMatrix alloc] initWithRows:bandCount cells:length];
    
    // determine band bound indexes
    NSUInteger binsPerOneFrequency = floor(halfSize) / (((float)samplingFrequency)/2.0);
    NSUInteger bandIndexes[bandLimits.count];
    for (int i = 0; i < bandLimits.count; i++) {
        bandIndexes[i] = [bandLimits[i] longValue] * binsPerOneFrequency;
    }
    
    // create waves for each band
    NSUInteger startIndex, indexesTillEnd;
    COMPLEX_SPLIT B;
    B.realp = (float *) malloc(halfSize * sizeof(float));
    B.imagp = (float *) malloc(halfSize * sizeof(float));
    for (NSUInteger bandIndex = 0; bandIndex < bandCount; bandIndex++) {
        
        // compute start end indexes
        startIndex = bandIndexes[bandIndex];
        indexesTillEnd = bandIndexes[bandIndex+1]-startIndex;
        if (startIndex+indexesTillEnd >= halfSize) {
            indexesTillEnd = halfSize-startIndex;
        }
        
        // copy band from fft result
        vDSP_vclr(B.realp, 1, halfSize);
        vDSP_vclr(B.imagp, 1, halfSize);
        memcpy(&(B.realp[startIndex]), &(A.realp[startIndex]), indexesTillEnd*sizeof(float));
        memcpy(&(B.imagp[startIndex]), &(A.imagp[startIndex]), indexesTillEnd*sizeof(float));
        
        // compute inverse fft for band into waveform
        vDSP_fft_zrip(fftSetup, &B, 1, log2n, FFT_INVERSE);
        float scale = (float)1.0/(2*length);
        vDSP_vsmul( B.realp, 1, &scale, B.realp, 1, halfSize );
        vDSP_vsmul( B.imagp, 1, &scale, B.imagp, 1, halfSize );
        vDSP_ztoc(&B, 1, (COMPLEX *)output.values[bandIndex], 2, halfSize);
        
    }
    vDSP_destroy_fftsetup(fftSetup);
    
    free(A.realp);
    free(A.imagp);
    free(B.realp);
    free(B.imagp);
    
    return output;
}

- (void)envelopesForMatrix:(FloatMatrix *)matrix
        withHannWindowSize:(float)hannWindowSize
{
    NSUInteger N = matrix.cells;
    
    // create half hann window (steep raise)
    NSUInteger hannWindowN = hannWindowSize*2*4096;
    float *hannWindow = calloc(hannWindowN, sizeof(float));
    vDSP_vclr(hannWindow, 1, hannWindowN);
    vDSP_hann_window(hannWindow, hannWindowN, vDSP_HANN_NORM | vDSP_HALF_WINDOW);
    vDSP_vrvrs(hannWindow, 1, hannWindowN);
    NSUInteger hannHalfWindowN = floor(hannWindowN/2);
    float *hannHalfWindow = calloc(hannHalfWindowN, sizeof(float));
    cblas_scopy(hannHalfWindowN, &hannWindow[hannHalfWindowN], 1, hannHalfWindow, 1);
//    [_windowDelegate beatDetector:self debugWave:hannHalfWindow length:hannHalfWindowN];
    free(hannWindow);
    
    // smooth each bank
    NSUInteger bufferN = N - hannHalfWindowN + 1;
    float *buffer = calloc(bufferN, sizeof(float));
    vDSP_vclr(buffer, 1, bufferN);
    for(int r = 0; r < matrix.rows; r++) {
        
        // rectify waves
        vDSP_vabs(matrix.values[r], 1, matrix.values[r], 1, matrix.cells);
        
        // smooth
        vDSP_vclr(buffer, 1, bufferN);
        vDSP_conv(matrix.values[r], 1, hannHalfWindow, 1, buffer, 1, bufferN, hannHalfWindowN);
        cblas_scopy(bufferN, buffer, 1, matrix.values[r], 1);
        
    }
    
    free(buffer);
    free(hannHalfWindow);
}

- (void)diffrectForMatrix:(FloatMatrix *)matrix
{
    
    NSUInteger N = matrix.cells;
    float *A = calloc(N, sizeof(float));
    vDSP_vclr(A, 1, N);
    float zero[1] = { 0 };
    
    for (int r = 0; r < matrix.rows; r++) {
        
        // diff
        vDSP_vsub(matrix.values[r], 1, matrix.values[r]+1, 1, A+1, 1, N-1);
        
        // half-wave rectify
        vDSP_vthr(A, 1, zero, A, 1, N);
        memcpy(matrix.values[r], A, N * sizeof(float));
        
    }
    
    free(A);
}

- (FloatMatrix *)simplifyMatrix:(FloatMatrix *)matrix
                     pieceSize:(NSUInteger)pieceSize
{
    
    NSUInteger N = floor(matrix.cells / pieceSize);
    FloatMatrix *simplified = [[FloatMatrix alloc] initWithRows:matrix.rows cells:N];
    
    for (int c = 0; c < N-1; c++) {
        for (int r = 0; r < simplified.rows; r++) {
            vDSP_sve(matrix.values[r]+(c * pieceSize), 1, simplified.values[r]+c, pieceSize);
            *(simplified.values[r]+c) = *(simplified.values[r]+c) / pieceSize;
        }
    }
    
    return simplified;
    
}


- (NSInteger)determineTempo:(FloatMatrix *)matrix
          samplingFrequency:(NSInteger)samplingFrequency
                     bpmMin:(NSInteger)bpmMin
                     bpmMax:(NSInteger)bpmMax
                    bpmStep:(NSInteger)bpmStep
{
    
    // waves to frequencies
    NSUInteger N = matrix.cells;
    vDSP_Length log2n = log2f(N);
    FFTSetup fftSetup = vDSP_create_fftsetup(log2n, FFT_RADIX2);
    NSInteger halfN = N/2;
    COMPLEX_SPLIT A;
    A.realp = (float *) malloc(halfN * sizeof(float)); // kdyz tam neni malloc ale staticka alokace tak to hazi badexecexception, asi tam nejsou nuly ale nan cizi adresy....
    A.imagp = (float *) malloc(halfN * sizeof(float));
    FloatMatrix *dft = [[FloatMatrix alloc] initWithRows:matrix.rows cells:matrix.cells];
    for (int r = 0; r < dft.rows; r++) {
        vDSP_ctoz((COMPLEX*)matrix.values[r], 2, &A, 1, N/2);
        vDSP_fft_zrip(fftSetup, &A, 1, log2n, FFT_FORWARD);
        vDSP_vdist(A.realp, 1, A.imagp, 1, dft.values[r], 1, N/2);
    }
    
    // find bpm with highest energy
    NSUInteger bestBpm = 0;
    double maxEnergy = 0;
    double energy = 0;
    double secondsPerBeat = 0;
    float x[1] = { 0 };
    float *comb = calloc(N, sizeof(float));
    float *b = calloc(halfN, sizeof(float));
    float *c = calloc(halfN, sizeof(float));
    vDSP_vclr(comb, 1, N);
    vDSP_vclr(b, 1, halfN);
    vDSP_vclr(c, 1, halfN);
    for (NSUInteger bpm = bpmMin; bpm <= bpmMax; bpm = bpm+bpmStep) {
        
        secondsPerBeat = 60. / (double)bpm;
        [self fillComb:comb length:N pulses:3 spacing:floor(samplingFrequency * secondsPerBeat) peakWidth:10];
        vDSP_ctoz((COMPLEX*)comb, 2, &A, 1, halfN);
        vDSP_fft_zrip(fftSetup, &A, 1, log2n, FFT_FORWARD);
        vDSP_vdist(A.realp, 1, A.imagp, 1, c, 1, halfN);
        
        energy = 0;
        for (int r = 0; r < dft.rows; r++) {
            vDSP_vmul(dft.values[r], 1, c, 1, b, 1, halfN);
            vDSP_svesq(b, 1, x, halfN);
            energy = energy + x[0];
        }
        
        if (energy > maxEnergy) {
            maxEnergy = energy;
            bestBpm = bpm;
        }
        
    }
    
    vDSP_destroy_fftsetup(fftSetup);
    
    free(A.realp);
    free(A.imagp);
    free(comb);
    free(b);
    free(c);
    
    return bestBpm;
    
}

- (NSUInteger)determineShiftForMatrix:(FloatMatrix *)matrix
                    samplingFrequency:(NSUInteger)samplingFrequency
                                tempo:(NSUInteger)tempo
{
    
    NSUInteger N = matrix.cells;
    
    // create comb
    float *comb = calloc(N, sizeof(float));
    double secondsPerBeat = 60. / (double)tempo;
    NSUInteger samplesPerBeat = floor(samplingFrequency * secondsPerBeat);
    NSUInteger pulses = floor(N/samplesPerBeat) - 1;
    [self fillComb:comb length:N pulses:pulses spacing:samplesPerBeat peakWidth:10];
    
    // move comb along time axis and find best match
    float *shiftedComb = calloc(N, sizeof(float));
    float *c = calloc(N, sizeof(float));
    float x[1] = { 0 };
    double energy, maxEnergy = 0;
    NSUInteger bestShift = 0;
    for (NSUInteger i = 0; i < samplesPerBeat; i++) {
//        NSLog(@"%d/%d", i, samplesPerBeat);
        vDSP_vclr(shiftedComb, 1, N);
        memcpy(shiftedComb+i, comb, N-i);
        energy = 0;
        for (int r = 0; r < matrix.rows; r++) {
            vDSP_vmul(shiftedComb, 1, matrix.values[r], 1, c, 1, N);
            vDSP_svesq(c, 1, x, N);
            energy = energy + x[0];
        }
        if (energy > maxEnergy) {
            maxEnergy = energy;
            bestShift = i;
        }
    }
    
    free(comb);
    free(shiftedComb);
    free(c);
    
    return bestShift;
}



- (NSUInteger)determineShiftForMatrix:(FloatMatrix *)matrix
                    samplingFrequency:(NSUInteger)samplingFrequency
                                tempo:(NSUInteger)tempo
                        expectedShift:(NSUInteger)expectedShift
{
    NSUInteger N = matrix.cells;
    
    // create comb
    float *comb = calloc(N, sizeof(float));
    double secondsPerBeat = 60. / (double)tempo;
    NSUInteger samplesPerBeat = floor(samplingFrequency * secondsPerBeat);
    NSUInteger pulses = floor(N/samplesPerBeat) - 1;
    [self fillComb:comb length:N pulses:pulses spacing:samplesPerBeat peakWidth:10];
    
    // move comb along time axis and find best match
    float *shiftedComb = calloc(N, sizeof(float));
    float *c = calloc(N, sizeof(float));
    float x[1] = { 0 };
    double energy, maxEnergy = 0;
    double fromBoudnary = expectedShift - samplesPerBeat * 0.1;
    double toBoundary = expectedShift + samplesPerBeat * 0.3;
    NSUInteger bestShift = 0;
    for (NSUInteger i = 0; i < samplesPerBeat; i++) {
        if (expectedShift > 0) {
            if (i < fromBoudnary) {
                continue;
            } else if (i > toBoundary) {
                continue;
            }
        }
        // NSLog(@"%d/%d", i, samplesPerBeat);
        vDSP_vclr(shiftedComb, 1, N);
        memcpy(shiftedComb+i, comb, N-i);
        energy = 0;
        for (int r = 0; r < matrix.rows; r++) {
            vDSP_vmul(shiftedComb, 1, matrix.values[r], 1, c, 1, N);
            vDSP_svesq(c, 1, x, N);
            energy = energy + x[0];
        }
        if (energy > maxEnergy) {
            maxEnergy = energy;
            bestShift = i;
        }
    }
    
    free(comb);
    free(shiftedComb);
    free(c);

    NSLog(@"foundShift %d expectedShift %d, (from %f, to %f)", bestShift, expectedShift, fromBoudnary, toBoundary);
    if (expectedShift != 0 && ABS(bestShift-expectedShift) > 20) {
        return expectedShift;
    }
    
    return bestShift;
}

- (NSArray *)deduceBeatsFromShift:(NSUInteger)shift
                          samples:(NSUInteger)samples
                samplingFrequency:(NSUInteger)samplingFrequency
                            tempo:(NSUInteger)tempo
{
    double secondsPerBeat = 60. / (double)tempo;
    NSUInteger samplesPerBeat = floor((double)samplingFrequency * secondsPerBeat);
    
    NSMutableArray *beats = @[].mutableCopy;
    
    long long i = shift;
    while (i > 0) {
        Beat *beat = [Beat new];
        beat.time = @(((double)i)/(double)samplingFrequency);
        [beats addObject:beat];
        i = i - samplesPerBeat;
    }
    i = shift + samplesPerBeat;
    while (i < samples) {
        Beat *beat = [Beat new];
        beat.time = @(((double)i)/(double)samplingFrequency);
        [beats addObject:beat];
        i = i + samplesPerBeat;
    }
    
    return beats;
}

- (NSArray *)computeMagnitudeSumForMatrix:(FloatMatrix *)matrix
                                beatTimes:(NSArray *)beatTimes
                samplingFrequency:(NSUInteger)samplingFrequency;
{
    NSMutableArray *sums = @[].mutableCopy;
    
    if (beatTimes.count > 1) {
        float x[1] = { 0 };
        NSNumber *prev = beatTimes[0];
        for (int i = 1; i < beatTimes.count; i++) {
            NSNumber *current = beatTimes[i];
            NSUInteger start = prev.doubleValue * samplingFrequency;
            NSUInteger end = current.doubleValue * samplingFrequency;
            vDSP_sve(&(matrix.values[0][start]), 1, x, end);
            [sums addObject:@(x[0])];
            prev = current;
        }
    }
    
    return sums;
}



- (void)fillComb:(float *)comb
          length:(NSUInteger)N
          pulses:(NSUInteger)pulses
         spacing:(NSUInteger)spacing
       peakWidth:(NSUInteger)peakWidth
{
    
    vDSP_vclr(comb, 1, N);
    
    NSUInteger candidateIndex = 0;
    for (int i = 0; i < pulses; i++) {
        candidateIndex = i * spacing;
        if (candidateIndex+peakWidth > N) {
            break;
        }
        vDSP_hamm_window(comb + candidateIndex, peakWidth, 0);
    }
    
    // TODO mozna zacit uplne z kraje s polovicnim oknem
    
}

- (NSArray *)cleanDuplicateBeats:(NSArray *)beats delta:(double)delta
{
    double previousBeat = 0;
    
    NSMutableArray *differences = [NSMutableArray arrayWithCapacity:beats.count];
    double totalDiffs = 0;
    for (Beat *beat in beats) {
        double difference = fabs(beat.time.doubleValue-previousBeat);
//        NSLog(@"dif %f", difference);
        [differences addObject:@(difference)];
        
        totalDiffs += difference;
        previousBeat = beat.time.doubleValue;
    }
    double averageDiff = totalDiffs/(double)beats.count;
//    NSLog(@"prumerna diff: %f", averageDiff);
    
    previousBeat = 0;
    NSMutableArray *cleanBeats = [NSMutableArray arrayWithCapacity:beats.count];
    for (Beat *beat in beats) {
        double difference = fabs(beat.time.doubleValue-previousBeat);
        if (difference < averageDiff * delta) {
//            NSLog(@"removing %f", beat.time.doubleValue);
        } else {
            [cleanBeats addObject:beat];
        }
        previousBeat = beat.time.doubleValue;
    }
    
    return cleanBeats;
}

- (NSArray *)cleanSilentBeats:(NSArray *)beats withStartDelta:(double)startDelta endDelta:(double)endDelta
{
    NSMutableArray *cleanBeats = [NSMutableArray arrayWithCapacity:beats.count];

    if (beats.count > 0) {
        
        double percentOfItemsForAveraging = 0.3;
        
        __block double avg = 0;
        __block int maxAvgCount = ceil((double)beats.count * percentOfItemsForAveraging);
        __block int avgCount = 0;
        NSLog(@"loudness summ from beginnign");
        [beats enumerateObjectsUsingBlock:^(Beat *beat, NSUInteger idx, BOOL *stop){
            if (idx > maxAvgCount) {
                *stop = YES;
            }
            avg += beat.sum.doubleValue;
            NSLog(@"%f", beat.sum.doubleValue);
            avgCount++;
        }];
        avg = avg / (double)avgCount;
        NSLog(@"avg sum %f of %d first beats", avg, avgCount);
        
        __block BOOL alwaysAddObject = NO;
        __block int countSkipped = 0;
        [beats enumerateObjectsUsingBlock:^(Beat *beat, NSUInteger idx, BOOL *stop){
            if (alwaysAddObject) {
                [cleanBeats addObject:beat];
            } else if (beat.sum.doubleValue >= avg * startDelta) {
                alwaysAddObject = YES;
                [cleanBeats addObject:beat];
            } else {
                countSkipped++;
            }
        }];
        
        avg = 0;
        maxAvgCount = ceil((double)beats.count * percentOfItemsForAveraging);
        avgCount = 0;
        NSLog(@"loudness summ from end");
        [beats enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(Beat *beat, NSUInteger idx, BOOL *stop){
            if (idx < beats.count-maxAvgCount) {
                *stop = YES;
            }
            avg += beat.sum.doubleValue;
            NSLog(@"%f", beat.sum.doubleValue);
            avgCount++;
        }];
        avg = avg / (double)avgCount;
        NSLog(@"avg sum %f of %d last beats", avg, avgCount);
        
        beats = cleanBeats.copy;
        cleanBeats = [NSMutableArray arrayWithCapacity:beats.count];
        alwaysAddObject = NO;
        [beats enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(Beat *beat, NSUInteger idx, BOOL *stop){
            if (beat.sum.doubleValue >= avg * endDelta || alwaysAddObject) {
                alwaysAddObject = YES;
                [cleanBeats addObject:beat];
            }
        }];
        
        cleanBeats = [cleanBeats sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"time" ascending:YES]]];
        

    }
    
    return cleanBeats;
}



@end
