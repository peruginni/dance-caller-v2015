//
//  MusicViewController.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/1/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import UIKit
import PromiseKit

class MusicViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var songsTableView: UITableView!
    
    var danceBook: DanceBook!
    var musicLibrary: MusicLibrary!
    var musicList:[Music] = []
    var showTapperMode = true;
    
    @IBOutlet weak var modeSwitch: UISwitch!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.danceBook = DanceBook(context: appDelegate.backgroundContext!)
        self.musicLibrary = MusicLibrary(context: appDelegate.backgroundContext!)
        self.musicLibrary.dumpAsJson()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.musicLibrary.listMusic(byRhythm: "all").then(body: { fetchedMusic -> Void in
            self.musicList = fetchedMusic
            self.songsTableView.reloadData()
//            self.musicLibrary.computeBars(self.musicList).then(body: {fetchedMusic2 -> Promise<[Music]> in
//                return self.musicLibrary.saveMusic(fetchedMusic2)
//            }).then(body: {fetchedMusic3 -> Void in
//                self.musicList = fetchedMusic3
//                self.songsTableView.reloadData()
//            })
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.musicList.count
    }
    
    @IBAction func switchModeChanged(sender: AnyObject) {
        self.showTapperMode = !self.showTapperMode
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("song", forIndexPath: indexPath) as! SelectSongTableViewCell
        
        var music = self.musicList[indexPath.row]
        cell.nameLabel.text = music.name
        cell.bandLabel.text = "\(music.band) \(music.album)"
        cell.barsLabel.text = "\(music.barsCount) bars"
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        var music = self.musicList[indexPath.row]
        
        self.musicLibrary.fetchCompleteMusic(music).then(body: { fetchedMusic -> Void in
            if (fetchedMusic != nil) {
                if (self.showTapperMode) {
                    self.performSegueWithIdentifier("preview", sender: fetchedMusic)
                } else {
                    self.performSegueWithIdentifier("tapMusic", sender: music)
                }
            }
        })
        
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "preview" && sender != nil && sender! is Music) {
            var vc = segue.destinationViewController as! MusicPreviewViewController
            if sender != nil {
                var music = sender! as! Music
                vc.setupWithMusic(music)
            }
        } else if (segue.identifier == "tapMusic" && sender != nil && sender! is Music) {
            var vc = segue.destinationViewController as! TapMusicViewController
            if sender != nil {
                var music = sender! as! Music
                vc.setupWithMusic(music)
            }
            
        }
    }


}
