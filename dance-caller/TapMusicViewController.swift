//
//  TapBeatsViewController.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 5/3/15.
//  Copyright (c) 2015 Ondrej Macoszek. All rights reserved.
//

import Foundation

import UIKit
import Foundation
import AVFoundation

class TapMusicViewController : UIViewController {

    @IBOutlet weak var tapMusicButton: UIButton!
    @IBOutlet weak var saveTapsButton: UIButton!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var startTappingButton: UIButton!
    @IBOutlet weak var verifyTapsButton: UIButton!
    
    var audioPath:String?
    var music:Music?
    var audioPlayer:AVPlayer?
    var musicLibrary:MusicLibrary?
    var taps:[Double] = []
    var beats:[Double] = []
    var lastTime:Double = 0
    
    func setupWithMusic(music:Music)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.musicLibrary = MusicLibrary(context: appDelegate.backgroundContext!)
        self.audioPath = self.musicLibrary!.pathForMusic(music)
        self.music = music
    }
    
    override func viewWillAppear(animated: Bool) {
        self.progressLabel.text = ""
        self.progressView.hidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        if self.audioPlayer != nil {
            self.audioPlayer!.pause()
            self.audioPlayer = nil
        }
    }
    
    @IBAction func startTappingAction(sender: AnyObject)
    {
        if self.audioPath != nil {
            if self.audioPlayer != nil {
                self.audioPlayer!.pause();
            }
            self.taps = []
            self.audioPlayer = AVPlayer(playerItem: AVPlayerItem(URL: NSURL(fileURLWithPath: self.audioPath!)))
            var totalTime = CGFloat(CMTimeGetSeconds(self.audioPlayer!.currentItem.duration)) as CGFloat;
            self.audioPlayer!.addPeriodicTimeObserverForInterval(CMTimeMake(1, 10), queue: dispatch_get_main_queue(), usingBlock: { (time:CMTime) -> Void in
                var time:CGFloat = CGFloat(CMTimeGetSeconds(time)) as CGFloat
                self.progressLabel.text = "\(time)"
                self.lastTime = Double(time)
            })
            self.audioPlayer!.play()
            self.progressLabel.text = ""
        }
    }
    
    @IBAction func verifyTapsAction(sender: AnyObject)
    {
        if self.audioPath != nil {
            if self.audioPlayer != nil {
                self.audioPlayer!.pause();
            }

            self.beats = [];
            for (var i = 1; i < self.taps.count; i++) {
                var timeSkip = (self.taps[i]-self.taps[i-1]) / 2.0
                for (var bi = 0; bi < 2; bi++) {
                    var beatTime = self.taps[i-1] + Double(bi) * timeSkip
                    self.beats.append(beatTime)
                }
            }
//            for (var i = 0; i < self.taps.count; i++) {
//                var beatTime = self.taps[i]
//                self.beats.append(beatTime)
//            }
            
            self.progressLabel.text = "Verifying"
            self.audioPlayer = AVPlayer(playerItem: AVPlayerItem(URL: NSURL(fileURLWithPath: self.audioPath!)))
            var counter = 0;
            self.audioPlayer?.addBoundaryTimeObserverForTimes(self.beats, queue: dispatch_get_main_queue(), usingBlock: { () -> Void in
                self.progressLabel.text = "Beat #\(counter++)"
            })
            self.audioPlayer?.play()
        }
    }
    
    @IBAction func tapMusicAction(sender: AnyObject)
    {
        if self.audioPlayer != nil {
            self.taps.append(self.lastTime);
            println(self.lastTime)
        }
    }
    
    @IBAction func saveTapsAction(sender: AnyObject)
    {
        if self.music != nil {
            
            if self.audioPlayer != nil {
                self.audioPlayer!.pause();
            }
            self.progressLabel.text = "Saving"
            
            let data = NSJSONSerialization.dataWithJSONObject(self.beats, options: nil, error: nil)
            let jsonTaps = NSString(data: data!, encoding: NSUTF8StringEncoding)
            
            if jsonTaps != nil {
                self.music?.structureJSON = jsonTaps! as String;
                self.musicLibrary?.saveMusic([self.music!]).then(body: { savedMusicList -> Void in
                    if self.navigationController != nil {
                        self.navigationController?.popViewControllerAnimated(true)
                    }
                })
            }
        }
    }
    
}
