//
//  Caller.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/3/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import UIKit
import PromiseKit

class Caller: NSObject {
    
    func callsForFigureAndDance(figure:Figure, dance:Dance) -> Promise<[Call]>
    {
        return Promise<[Call]> { (fulfill, reject) in
            
            var calls:[Call] = []
            
            var flowOfMoves = figure.flow
            var bars = figure.music?.bars
            
            if bars != nil && bars!.count > 0 && flowOfMoves.count > 0 {
                flowOfMoves = flowOfMoves.reverse()
                var timeOfLastBar:Double = bars!.last!.time;
                var currentMove = flowOfMoves.removeLast()
                var elapsedBars:Int64 = 0
                var lastCall:Call! = nil
                for (index, bar) in enumerate(bars!) {
                    println("\(index) ... \(bar.time)")
                    
                    if index < 8 {
                        if index == 4 {
                            var call = Call()
                            call.sentence = "\(currentMove.who), \(currentMove.name)!"
                            call.time = bar.time
                            call.moveTime = bars?.count >= 8 ? bars![7].time : timeOfLastBar;
                            call.move = currentMove
                            calls.append(call)
                        }
                        continue;
                    }
                    
                    if flowOfMoves.count == 0 {
                        break
                    }
                    
                    var nextMove = flowOfMoves.last!
                    var nextSententce = "\(nextMove.who), \(nextMove.name)"
                    if currentMove.who == nextMove.who {
                        nextSententce = "\(nextMove.name)"
                    }
                    var suggestionStartFromEnd = 3
                    if count(nextSententce) > 7 {
                        suggestionStartFromEnd = 3
                    } else if count(nextSententce) > 10 {
                        suggestionStartFromEnd = 4
                    } else if count(nextSententce) > 12 {
                        suggestionStartFromEnd = 5
                    }

                    if (currentMove.bars-suggestionStartFromEnd <= 0) {
                        // should start call asap
                        var call = Call()
                        call.move = nextMove
                        call.sentence = nextSententce
                        call.time = bar.time
//                            call.moveTime = bars?.count >= 8 ? bars![7].time : timeOfLastBar;
                        calls.append(call)
                        elapsedBars = 0
                        currentMove = flowOfMoves.removeLast()
                    } else if (elapsedBars == currentMove.bars-suggestionStartFromEnd) {
                        // will call when appropriate
                        var call = Call()
                        call.move = nextMove
                        call.sentence = nextSententce
                        call.time = bar.time
                        calls.append(call)
                        lastCall = call
                    } else if elapsedBars == currentMove.bars {
                        // when move started watch upcomming one
                        elapsedBars = 0
                        if lastCall != nil {
                            lastCall!.moveTime = bars![index-1].time
                        }
                        currentMove = flowOfMoves.removeLast()
                    }
                    
                    elapsedBars++

                }
            }
            println("---")

            fulfill(calls)
            
        }
    }
    
}
