//
//  FloatMatrix.h
//  dance-caller
//
//  Created by Ondrej Macoszek on 9/29/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FloatMatrix : NSObject

@property (nonatomic, assign) float **values;
@property (nonatomic, assign, readonly) NSUInteger rows;
@property (nonatomic, assign, readonly) NSUInteger cells;

- (instancetype)initWithRows:(NSUInteger)rows cells:(NSUInteger)cells;
- (void)clean;

@end
