//
//  AudioVisualizer.m
//  dance-caller
//
//  Created by Ondrej Macoszek on 1/15/15.
//  Copyright (c) 2015 Ondrej Macoszek. All rights reserved.
//

#import "AudioVisualizer.h"
#import <CoreAudio/CoreAudioTypes.h>
#import <AudioToolbox/AudioToolbox.h>
#import <Accelerate/Accelerate.h>
#import <Foundation/Foundation.h>
#import "FloatMatrix.h"
#import "AudioHelper.h"

@implementation AudioVisualizer {
    NSString *_audioFile;
    CGSize _imageSize;
    double _imageSamplingFrequency;
    double _imageSeconds;
    double _maxAmplitude;
    NSMutableArray *_images;
    NSMutableArray *_simplifiedBlocks;
    NSOperationQueue *_queue;
}

- (instancetype)initWithAudioFile:(NSString *)audioFile imageSize:(CGSize)imageSize imageSamplingFrequency:(double)imageSamplingFrequency imageSeconds:(double)imageSeconds
{
    self = [super init];
    if (self) {
        _images = @[].mutableCopy;
        _audioFile = audioFile;
        _imageSize = imageSize;
        _imageSamplingFrequency = imageSamplingFrequency;
        _imageSeconds = imageSeconds;
    }
    return self;
}

- (void)startPreparingImages
{
    if (_queue) {
        return;
    }
    _queue = [[NSOperationQueue alloc] init];
    _queue.maxConcurrentOperationCount = 1;
    
    __block NSMutableArray *blocks = @[].mutableCopy;
    __block float maxAmplitude = 0;
    [_queue addOperationWithBlock:^{
        [AudioHelper readAudioFile:_audioFile atSampleRate:44100.0 windowSecondsSize:_imageSeconds analysisBlock:^(float *audioData, NSInteger audioDataSize) {
            
//            NSLog(@"audio read %d", blocks.count);
            NSArray *simplified = [AudioHelper simplifySamples:audioData length:audioDataSize targetSamplingFrequency:_imageSamplingFrequency];
            float simplifiedMax = [AudioHelper findAbsoluteMaxAmplitudeFromAudioSamples:simplified channels:1];
            if (simplifiedMax > maxAmplitude) {
                maxAmplitude = simplifiedMax;
            }
            [blocks addObject:simplified];
            
        }];
        
        _simplifiedBlocks = blocks;
        _maxAmplitude = maxAmplitude;
//        NSLog(@"maxamplitude %f", maxAmplitude);
//        NSLog(@"blocks %d", blocks.count);
        
        _images = @[].mutableCopy;
        [blocks enumerateObjectsUsingBlock:^(NSArray *samples, NSUInteger idx, BOOL *stop) {
            id obj = [NSNull null];
            if (idx < 5) {
                obj = [AudioHelper createImageFromAudioSamples:samples channels:1 maxAmplitude:_maxAmplitude imageWidth:_imageSize.width imageHeight:_imageSize.height];
            }
            _images[idx] = obj;
        }];
        
        if (self.delegate) {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self.delegate audioVisualizerDidPrepareImages:self];
            }];
        }
        
    }];
}

- (NSInteger)numberOfImages
{
    return !_simplifiedBlocks.count ? 0 : _simplifiedBlocks.count;
}

- (UIImage *)imageAtIndex:(NSInteger)index
{
    if (!_simplifiedBlocks || index >= _simplifiedBlocks.count || index < 0) {
        return nil;
    }
    if ([_images[index] isKindOfClass:[NSNull class]]) {
        [_queue addOperationWithBlock:^{
            NSArray *samples = _simplifiedBlocks[index];
            UIImage *image = [AudioHelper createImageFromAudioSamples:samples channels:1 maxAmplitude:_maxAmplitude imageWidth:_imageSize.width imageHeight:_imageSize.height];
            _images[index] = image;
            if (self.delegate) {
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self.delegate audioVisualizerDidPrepareImages:self];
                }];
            }
        }];
        _images[index] = @(NO);
        return nil;
    } else if ([_images[index] isKindOfClass:[UIImage class]]) {
        return _images[index];
    }
    return nil;
}

@end
