//
//  Dance.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/3/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import UIKit
import CoreData

class Dance: NSObject {
   
    var objectID: NSManagedObjectID?
    var name: String = ""
    var summary: String = ""
    var type: String = ""
    var figures: [Figure] = []
    
    override init() {
    }

    init(cdDance: CDDance) {
        objectID = cdDance.objectID
        name = cdDance.name
        type = cdDance.type
        summary = cdDance.summary
    }
    
}
