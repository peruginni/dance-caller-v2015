//
//  Beat.h
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/1/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Beat : NSObject

@property (nonatomic, strong) NSNumber *time;
@property (nonatomic, strong) NSNumber *sum;
@property (nonatomic, strong) NSString *note;

@end
