//
//  MusicPreviewViewController.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/1/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import SwiftyJSON

class MusicPreviewViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, AudioVisualizerDelegate {
    
    @IBOutlet weak var testBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var selectViewBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var waveView: UICollectionView!
    @IBOutlet weak var beatsView: UICollectionView!
    @IBOutlet weak var debugBeatsView: UICollectionView!
    @IBOutlet weak var loadingView: UIView!
    var audioPath:String?
    var audioVisualizer:AudioVisualizer?
    var winSec:Double = 3;
    
    var debugBeats:[Double] = []
    var music:Music?
    var imageSize:CGSize = CGSize(width: 200, height: 300);
    var availableColors = [
        UIColor.flatMagentaColor(),
        UIColor.flatSkyBlueColor(),
        UIColor.flatBrownColor(),
        UIColor.flatMintColor(),
        UIColor.flatSandColor(),
        UIColor.flatTealColor(),
        UIColor.flatRedColor(),
        UIColor.flatBlueColor(),
        UIColor.flatPinkColor(),
        UIColor.flatGreenColor(),
        UIColor.flatYellowColor(),
    ];
    
    func setupWithMusic(music:Music)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        var musicLibrary = MusicLibrary(context: appDelegate.backgroundContext!)
        self.audioPath = musicLibrary.pathForMusic(music)
        self.music = music
        
        musicLibrary.testAnnotationForMusicId(music.assetPersistentID).then(body: {musicAnnotation -> Void in
            self.debugBeats = musicAnnotation.beats;
            if self.debugBeatsView != nil {
                self.debugBeatsView.reloadData()
            }
        })
    }
    
    override func viewDidAppear(animated: Bool)
    {
        self.beatsView.backgroundColor = UIColor.clearColor()
        (self.waveView.collectionViewLayout as! UICollectionViewFlowLayout).itemSize = imageSize;
        (self.debugBeatsView.collectionViewLayout as! UICollectionViewFlowLayout).itemSize = CGSizeMake(imageSize.width, imageSize.height + 30);
        (self.beatsView.collectionViewLayout as! UICollectionViewFlowLayout).itemSize = imageSize;
        self.audioVisualizer = AudioVisualizer(audioFile: self.audioPath, imageSize: imageSize, imageSamplingFrequency: 10, imageSeconds: self.winSec);
        self.audioVisualizer?.startPreparingImages();
        self.audioVisualizer?.delegate = self
    }
    
    // MARK: UICollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if (collectionView == self.waveView) {
            return self.audioVisualizer != nil ? self.audioVisualizer!.numberOfImages() : 0
        } else if (collectionView == self.beatsView) {
            return (music != nil) ? music!.beats.count : 0
        } else if (collectionView == self.debugBeatsView) {
            return self.debugBeats.count
        } else {
            return 0
        }
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        if (collectionView == self.beatsView) {
            var cell = collectionView.dequeueReusableCellWithReuseIdentifier("beat", forIndexPath: indexPath) as! UICollectionViewCell
            var borderView = cell.viewWithTag(1)
            var beat = self.music!.beats[indexPath.row]
            var colorIndex = beat.note.toInt()!
            borderView?.backgroundColor = self.availableColors[colorIndex % self.availableColors.count];
            return cell
        } else if (collectionView == self.debugBeatsView) {
            var cell = collectionView.dequeueReusableCellWithReuseIdentifier("beat", forIndexPath: indexPath) as! UICollectionViewCell
            return cell
        } else {
            var cell = collectionView.dequeueReusableCellWithReuseIdentifier("image", forIndexPath: indexPath) as! UICollectionViewCell
            var imageView = cell.viewWithTag(1) as! UIImageView
            imageView.image = self.audioVisualizer?.imageAtIndex(indexPath.row)
            return cell
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        if (collectionView == self.waveView) {
            return (collectionViewLayout as! UICollectionViewFlowLayout).itemSize;
        } else if (collectionView == self.beatsView) {
            var width:Double = 0
            var beat = self.music!.beats[indexPath.row]
            if (indexPath.row > 0) {
                var beatPrev = self.music!.beats[indexPath.row-1]
                width = Double(imageSize.width) / self.winSec * (beat.time-beatPrev.time)
            } else {
                width = Double(imageSize.width) / self.winSec * beat.time
            }
            
            return CGSizeMake(CGFloat(width), CGFloat(imageSize.height))
        } else if (collectionView == self.debugBeatsView) {
            var width:Double = 0
            var beatTime = self.debugBeats[indexPath.row]
            if (indexPath.row > 0) {
                var beatPrevTime = self.debugBeats[indexPath.row-1]
                width = Double(imageSize.width) / self.winSec * (beatTime-beatPrevTime)
            } else {
                width = Double(imageSize.width) / self.winSec * beatTime
            }
            return CGSizeMake(CGFloat(width), CGFloat(imageSize.height + 30))
        } else {
            return CGSizeMake(0, 0)
        }
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView)
    {
        if (scrollView == self.waveView) {
            self.beatsView.contentOffset = scrollView.contentOffset
            self.debugBeatsView.contentOffset = scrollView.contentOffset
        }
    }
    
    func audioVisualizerDidPrepareImages(av: AudioVisualizer!)
    {
        self.waveView.reloadData()
    }
    
}
