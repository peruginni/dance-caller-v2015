//
//  ViewController.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 9/26/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import UIKit
import PromiseKit

class DancesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var dancesTableView: UITableView!
    var danceBook: DanceBook!
    var dances: [Dance] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.danceBook = DanceBook(context: appDelegate.backgroundContext!)
        self.danceBook.countAllDances().then(body:{ numberOfDances -> Promise<Void>? in
            if (numberOfDances > 0) {
                return nil
            }
            return self.danceBook.importDancesFromJson(NSString(
                contentsOfFile: NSBundle.mainBundle().pathForResource("dances", ofType: "json")!,
                encoding: NSUTF8StringEncoding,
                error: nil
            )! as String)
        }).then(body: {Void -> Promise<[Dance]> in
            return self.danceBook.allDances()
        }).then(body: {dances -> Void in
            self.dances = dances
            self.dancesTableView.reloadData()
        });
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dances.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("dance", forIndexPath: indexPath) as! UITableViewCell
        var dance = self.dances[indexPath.row] as Dance?
        if (dance != nil) {
            cell.textLabel?.text = dance?.name
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        var dance = self.dances[indexPath.row] as Dance?
        if (dance != nil) {
            self.performSegueWithIdentifier("dance", sender: dance)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "dance") {
            var dvc = segue.destinationViewController as! DanceViewController
            dvc.dance = (sender as! Dance)
        }
    }
    
}

