//
//  MusicPickerViewController.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 12/1/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

import UIKit
import MediaPlayer
import PromiseKit

class MusicPickerViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, MPMediaPickerControllerDelegate {
    
    @IBOutlet weak var songsTableView: UITableView!
    @IBOutlet weak var addButton: UIBarButtonItem!
    @IBOutlet weak var filterButton: UIBarButtonItem!
    @IBOutlet weak var loadingView: UIView!
    
    var figure:Figure?
    var danceBook: DanceBook!
    var musicLibrary: MusicLibrary!
    var musicList:[Music] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItems = [
            self.addButton
            //,self.filterButton
        ]
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.danceBook = DanceBook(context: appDelegate.backgroundContext!)
        self.musicLibrary = MusicLibrary(context: appDelegate.backgroundContext!)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.musicLibrary.listMusic(byRhythm: "all").then(body: { fetchedMusic -> Void in
            self.musicList = fetchedMusic
            self.songsTableView.reloadData()
            
            self.musicLibrary.computeBars(self.musicList).then(body: {fetchedMusic2 -> Promise<[Music]> in
                return self.musicLibrary.saveMusic(fetchedMusic2)
            }).then(body: {fetchedMusic3 -> Void in
                self.musicList = fetchedMusic3
                self.songsTableView.reloadData()
            })
        })
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.musicList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("song", forIndexPath: indexPath) as! SelectSongTableViewCell
        var music = self.musicList[indexPath.row]
        cell.nameLabel.text = music.name
        cell.bandLabel.text = "\(music.band) \(music.album)"
        cell.barsLabel.text = "\(music.barsCount) bars"
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        var music = self.musicList[indexPath.row]
        if self.figure != nil {
            self.figure!.music = music
            self.danceBook.figure(self.figure!, saveDefaultMusic: music).then(body: { Void -> Void in
                self.navigationController?.popViewControllerAnimated(true)
            })
        }
        
    }
    
    @IBAction func addButtonTapped(sender: AnyObject) {
        
        var picker = MPMediaPickerController(mediaTypes: MPMediaType.Music)
        picker.delegate = self
        picker.allowsPickingMultipleItems = true
        picker.prompt = "Add new songs"
        picker.showsCloudItems = false
        self.navigationController?.presentViewController(picker, animated: true, completion: { () -> Void in
            
        })
        
    }
    
    @IBAction func filterButtonTapped(sender: AnyObject) {
        
    }
    
    // MARK: - MPMediaPickerControllerDelegate
    
    func mediaPicker(mediaPicker: MPMediaPickerController!, didPickMediaItems mediaItemCollection: MPMediaItemCollection!)
    {
        UIView.animateWithDuration(1, animations: { () -> Void in
            self.loadingView.alpha = 1
        })
        self.musicLibrary.importFromMediaItemCollection(mediaItemCollection).then(body: { musicList -> Promise<[Music]> in
            return self.musicLibrary.saveMusic(musicList)
        }).then(body: { musicList -> Void in
            var list:[Music] = []
            for music in musicList {
                list.append(music)
            }
            UIView.animateWithDuration(1, animations: { () -> Void in
                self.loadingView.alpha = 0
            })
            self.performSegueWithIdentifier("analyze", sender: list)
        })
        self.navigationController?.dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
    }
    
    func mediaPickerDidCancel(mediaPicker: MPMediaPickerController!)
    {
        self.navigationController?.dismissViewControllerAnimated(true, completion: { () -> Void in
            
        })
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "analyze" && sender != nil && sender! is [Music]) {
            var vc = segue.destinationViewController as! MusicAnalysisViewController
            vc.addMusicForAnalysis(sender as! [Music])
        }
    }

}
