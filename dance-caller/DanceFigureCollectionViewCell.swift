//
//  DanceFigureCellCollectionViewCell.swift
//  dance-caller
//
//  Created by Ondrej Macoszek on 1/1/15.
//  Copyright (c) 2015 Ondrej Macoszek. All rights reserved.
//

import UIKit

class DanceFigureCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var wrapperView: UIView!
    @IBOutlet weak var labelView: UILabel!
    
}
