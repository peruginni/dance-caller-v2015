//
//  AudioHelper.m
//  dance-caller
//
//  Created by Ondrej Macoszek on 10/20/14.
//  Copyright (c) 2014 Ondrej Macoszek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreAudio/CoreAudioTypes.h>
#import <AudioToolbox/AudioToolbox.h>
#import <Accelerate/Accelerate.h>
#import <UIKit/UIKit.h>
#import "AudioHelper.h"

@implementation AudioHelper

+ (void)readAudioFile:(NSString *)audioFile atSampleRate:(double)sampleRate windowSecondsSize:(double)windowSecondsSize
        analysisBlock:(void(^)(float *audioData, NSInteger audioDataSize))analysisBlock
{
    ExtAudioFileRef fileRef;
    CheckResult(ExtAudioFileOpenURL(CFURLCreateWithFileSystemPath(kCFAllocatorDefault, (__bridge CFStringRef)audioFile, kCFURLPOSIXPathStyle, false), &fileRef), "ExtAudioFileOpenURL failed");
    
    int numberOfChannels = 1; // mono;
    
    AudioStreamBasicDescription audioFormat;
    audioFormat.mSampleRate = sampleRate; // 44100.0;
    audioFormat.mFormatID = kAudioFormatLinearPCM;
    audioFormat.mFormatFlags = kLinearPCMFormatFlagIsFloat;
    audioFormat.mBitsPerChannel = sizeof(float) * 8;
    audioFormat.mChannelsPerFrame = numberOfChannels;
    audioFormat.mBytesPerFrame = audioFormat.mChannelsPerFrame * sizeof(float);
    audioFormat.mFramesPerPacket = 1;
    audioFormat.mBytesPerPacket = audioFormat.mFramesPerPacket * audioFormat.mBytesPerFrame;
    CheckResult(ExtAudioFileSetProperty(fileRef, kExtAudioFileProperty_ClientDataFormat, sizeof (AudioStreamBasicDescription), &audioFormat),"ExtAudioFileSetProperty failed");
    
    UInt64 totalSamples = 0;
    UInt32 totalSamplesPropertySize = sizeof(totalSamples);
    CheckResult(ExtAudioFileGetProperty(fileRef, kExtAudioFileProperty_FileLengthFrames, &totalSamplesPropertySize, &totalSamples), "ExtAudioFileGetProperty failed");
    
    NSInteger windowSampleSize = floor(sampleRate * windowSecondsSize);
    UInt32 outputBufferSize = windowSampleSize * audioFormat.mBytesPerPacket;
    float *outputBuffer = (float *)malloc(sizeof(float) * outputBufferSize);
    
    AudioBufferList audioData;
    audioData.mNumberBuffers = numberOfChannels;
    audioData.mBuffers[0].mNumberChannels = audioFormat.mChannelsPerFrame;
    audioData.mBuffers[0].mDataByteSize = outputBufferSize;
    audioData.mBuffers[0].mData = outputBuffer;
    
    UInt32 readLength = windowSampleSize;
    while (true) {

        CheckResult(ExtAudioFileRead(fileRef, &readLength, &audioData), "ExtAudioFileRead failed");
        if (!readLength) {
            // no more data to read
            break;
        } else if (readLength < windowSampleSize) {
            // if we read last part of file which is smaller thank window, so fill remaining part with zeros
            float *zeros = calloc(windowSampleSize, sizeof(float));
            cblas_scopy(windowSampleSize, zeros, 1, &(audioData.mBuffers[0].mData[readLength]), 1);
            free(zeros);
        }
    
        // copy read data to audio wave buffer
        analysisBlock(audioData.mBuffers[0].mData, windowSampleSize);
    
    }
    
    free(outputBuffer);
}

+ (NSArray *)simplifySamples:(float *)samples length:(NSInteger)length targetSamplingFrequency:(NSUInteger)targetSamplingFrequency;
{
    NSMutableArray *simplified = @[].mutableCopy;
    
    float c;
    NSUInteger pieceSize = targetSamplingFrequency;
    for (NSUInteger i = 0; i < length; ) {
        if (i + pieceSize > length) {
            pieceSize = length - i;
        }
        vDSP_sve(&samples[i], 1, &c, pieceSize); // C[0] = sum(A[n], 0 <= n < N);
        c = c/(float)targetSamplingFrequency;
        [simplified addObject:@(c)];
        i = i + pieceSize;
    }
    
    return simplified;
    
}

+ (UIImage *) createImageFromAudioSamples:(NSArray *) samples
                                 channels:(NSInteger) numberOfChannels
                              maxAmplitude:(float) maxAmplitude
                               imageWidth:(float) imageWidth
                              imageHeight:(float) imageHeight
{
    if (maxAmplitude == 0.0) {
        maxAmplitude = 1;
    }
    
    NSInteger sampleWidth = samples.count;
    
    // source: http://stackoverflow.com/questions/11239215/how-to-create-a-song-visualization-in-wavefrom-in-mpmusicplayer-controller
    
    UIGraphicsBeginImageContext(CGSizeMake(sampleWidth, imageHeight));
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextSetAlpha(context, 1.0);
    CGRect rect = {sampleWidth, imageHeight, 0, 0};
    CGColorRef leftcolor = [UIColor blackColor].CGColor;
    CGColorRef rightcolor = [UIColor redColor].CGColor;
    
    CGContextFillRect(context, rect);
    
    CGContextSetLineWidth(context, 1.0);
    
    float halfGraphHeight = (imageHeight / 2) / (float) numberOfChannels ;
    float centerLeft = halfGraphHeight;
    float centerRight = (halfGraphHeight * 3);
    float sampleAdjustmentFactor = (halfGraphHeight/ (float) numberOfChannels) / (float) maxAmplitude;
    
    float axisX = 0.0;
    for (NSNumber *number in samples) {
        float left = number.floatValue;
        float pixels = (float) left;
        pixels *= sampleAdjustmentFactor;
//        NSLog(@"left %f , pixels %f, factor %f", left, pixels, sampleAdjustmentFactor);
        CGContextMoveToPoint(context, axisX, centerLeft); // -pixels
        CGContextAddLineToPoint(context, axisX, centerLeft-pixels); //+pixels
        CGContextSetStrokeColorWithColor(context, leftcolor);
        CGContextStrokePath(context);
        axisX += sampleWidth/(float)samples.count;
//        NSLog(@"axisX %f , left %f , pixels %f, factor %f", axisX, left, pixels, sampleAdjustmentFactor);
    }
    
    // Create new image
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContext(CGSizeMake(imageWidth, imageHeight));
    [newImage drawInRect:CGRectMake(0, 0, imageWidth, imageHeight)];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (float) findAbsoluteMaxFromAudioSamples:(float *) samples
                                   length:(NSInteger) length
                                 channels:(NSInteger) numberOfChannels
{
    float c;
    vDSP_maxmgv(samples, 1, &c, length);
    return c;
}

+ (float) findAbsoluteMaxAmplitudeFromAudioSamples:(NSArray *) samples
                                  channels:(NSInteger) numberOfChannels
{
    float c = 0;
    for (NSNumber *number in samples) {
        if (ABS(number.floatValue) > c) {
            c = number.floatValue;
        }
    }
    return c;
}


@end

